% $Id: bg-limited.tex,v 1.33 2009/12/21 11:00:18 dvd Exp $

\section{Limited Rationality}
\label{sec:bg-limited}

While ideally programs, or agents, should {\em act rationally}
\cite{Russell.aima}, absolute rationality is not feasible. The
computation power of the agent approaching the problem must be taken
into account.

\subsection{Types of Rationality}

The simplest to formulate---and the hardest to implement---is {\em
  perfect rationality}.  A perfectly rational agent acts in a way that
maximizes its expected utility given the information the agent
acquires from the environment. Since making a decision takes time,
perfect rationality is not realizable.

An agent exhibiting {\em calculative rationality} acts in a way that
was optimal at the beginning of deliberation. Calculative rationality
was viewed as an approximation of perfect rationality. However,
intuitively it is clear that the right decision at the wrong time has
little value. Morever, a decision that depends on the past state may
lead to chaotic, unpredictable behavior: such dependency can be
modeled by a delay differential equation, and delay differential
equations are a model for chaos \cite{Sprott.dde}.

Herbert Simon \cite{Simon.mms} introduced the notion of {\em bounded
  rationality} and suggested that bounded rationality works by {\bf
  satisficing}---deliberating only long enough to come with an answer
that is ``good enough''. A formal mathematical model for bounded
rationality is {\em bounded optimality}.

\subsection{Bounded Optimality}

A bounded optimal agent finds a solution that is optimal given its
computational resources \cite{Horvitz.reasoningabout}. That is, there
is at least one program that exhibits bounded optimality in the given
conditions: the one which ensures that the agent achieves the highest
expected utility among all of the programs running on the same
machine.

Bounded optimality is the most useful model for limited rationality:
this model can be both defined formally and achieved or approached by real
agents. The formal specification should be a framework for choosing
a theoretically sound compromise between deliberating and acting, and
for selecting the most worthwhile ways of deliberation.

\subsection{Rational Metareasoning}
\label{sec:bg-limited-meta}

Rational metareasoning \cite{Russell.right} is an approach to building
bounded optimal agents. The approach describes a method of choosing {\em
meta-level actions} and is based on notions of {\em value of information}
and {\em time cost}.

\subsubsection{Meta-level Actions}

The agent performs a sequence of base-level actions drawn from a known
set $\{A_i\}$. Before committing to each action, the agent
deliberates, and the deliberation can be represented as a sequence of
meta-level actions from a set $\{S_j\}$. At any given time there is a
base-level action, $A_\alpha$, that appears best to the agent. The
goal of subsequent meta-level actions is to refine the choice of
$A_\alpha$ before performing the base-level action.

The agent selects $\alpha$ by numericaly estimating the {\em
  utilities} of action outcomes; according to decision theory, an
optimal action $A_\alpha$ is one which maximizes the agent's expected
utility:

\begin{eqnarray}
\label{eq:bg-limited-eu}
\IE[U(A_i)]&=&\sum_k P(W_k) U(A_i,W_k)\\
\alpha&=&\arg \max_i \IE[U(A_i)]
\end{eqnarray}
where $\{W_k\}$ is the set of possible world states, and $P(W_k)$ is
the probability that the agent is currently in state $W_k$.

\subsubsection{Value of Information}

A meta-level action affects the choice of the base-level action
$A_\alpha$. The {\em value} of a meta-level action is measured by the
resulting increase in the utility of $A_\alpha$. Since neither the
outcomes of meta-level actions nor the true utility of $A_\alpha$ are
known in advance, a meta-level action is selected according to its
expected influence on the expected utility of $A_\alpha$.

The {\em value of information} of a meta-level action $S_j$ is the
expected difference between the expected utility of $S_j$ and the
expected utility of the current $A_\alpha$.

\begin{equation}
\label{eq:bg-limited-nv}
V(S_j)=\IE(\IE(U(S_j))-\IE(U(A_\alpha)))
\end{equation}

$S_j$ affects the internal state of the agent and the effect of
possible further meta-level actions $\mathbf{T}$. Thus, the utility of
$S_j$ is:

\begin{equation}
\label{eq:bg-limited-su}
U(S_j)=\sum_{\mathbf{T}}P(\mathbf{T})U(A_{\alpha}^\mathbf{T},S_j\cdot\mathbf{T})
\end{equation}

While a perfectly rational agent would always choose the most valuable
computation sequence, an agent with only limited rationality makes
decisions based on an approximation of the utility.

\subsubsection{Benefit and Time Cost}

The general dependence on the overall state complicates the analysis. Under
certain assumptions, it is possible to capture the dependence of utility on time in a
separate notion of {\em time cost} $C$. Then, the utility of an action $A_i$ taken
 after a meta-level action $S_j$ is the utility of $A_i$ taken now less the
cost of time for performing $S_j$:

\begin{equation}
\label{eq:bg-limited-iu}
U(A_i, S_j) = U(A_i) - C(A_i, S_j)
\end{equation}

It is customary to call the current utility of a future base-level
action its {\em intrinsic utility}. The separation into intrinsic
utility and time cost allows to estimate the utility of a base-level
action in a time-independent manner, and then refine the true utility
estimate under varying the time pressure represented by $C$.

In many cases, the time cost of an internal action is independent of
the subsequently taken base-level action. When $C$ depends only on
$S_j$, [\ref{eq:bg-limited-nv}] can be rewritten with the cost and the
intrinsic value of information of a computation as separate terms.

\begin{eqnarray}
\label{eq:bg-limited-v=bc}
V(S_j)&=&\IE\left(\IE(U(A_\alpha^j, S_J))-\IE(U(A_\alpha))\right)\nonumber\\
     &=&\IE\left(\IE(U(A_\alpha^j))-\IE(U(A_\alpha))\right)-C(S_j)\\
     &=& \Lambda(S_j)-C(S_j)
\end{eqnarray}
where
\begin{equation}
\label{eq:bg-limited-benefit}
\Lambda(S_j)=\IE\left(\IE(U(A_\alpha^j))-\IE(U(A_\alpha))\right)
\end{equation}
denotes the intrinsic value of information, that is, the expected
difference between the intrinsic expected utilities of the new
and the old selected base-level action, computed after the meta-level
action was taken.

For any particular outcome of the meta-level action, one of the
following cases takes place:

\begin{enumerate}
\item the selected base-level action $A_\alpha$ stays the same;
  consequently, $\IE(U(A_\alpha^j))-\IE(U(A_\alpha))$ is always zero;
\item a different base-level action $A_\alpha^j$ is selected, with
  {\bf higher} expected utility than the expected utility of
  $A_\alpha$ before the meta-level action was taken;
\item a different $A_\alpha^j$ is selected, and its expected
  utility is {\bf the same} as or {\bf lower} than the expected
  utility of $A_\alpha$ before the meta-level action was taken.
\end{enumerate}

In the last two cases, the difference is positive---though the
expected utility of the final choice can decrease, the decrease is due
to the updated knowledge about all actions, and the latter
choice appears to be better than the earlier one. Thus, while the
value of information can be either positive or negative depending on
the cost of the action, the intrinsic value of information is always
non-negative.

\subsection{Metareasoning under Simplifying Assumptions}
\label{sec:bg-limited-simplifying}

As base-level perfect rationality is not possible, meta-level
rationality is not achievable as well. The utility of a meta-level
action has to be approximated. The approximation should allow both to
bound the set of considered sequences of meta-level actions and to
restrict the ways the meta-level actions affect the choice of a
base-level action.

The simplest such approximation is based on {\em meta-greediness} and
{\em single-step} assumptions. \cite{Russell.right}.

\begin{description}
\item[Meta-greedy algorithms:] Only sequences consisting of a single
  meta-level action are considered; the meta-level policy commits to
  the best single action and therefore is greedy. This approach can be
  generalized and still kept tractable by, for example, considering a
  small enough set of certain sequences of meta-level actions.

\item[Single-step assumption:] The utility of a meta-level action is
  determined only by its immediate effect on the choice of a
  base-level action. This assumption is tantamount to acting as if
  time is left only for a single meta-level action; hence the
  name.
\end{description}

The value of information estimate and the greedy algorithm which
follow from these assumptions are customarily called {\em myopic}
\cite{Russell.right}. The myopic value of information estimate $MVI$
need not be restricted to single atomic actions; multiple atomic actions
performed in a batch, without intermediate decisions, can be
considered a single meta-level action. For a batch of $n$ meta-level
actions ${S_{j_1}\ldots S_{j_n}}$, the myopic estimate is:
\begin{equation}
MVI_{j_1\ldots j_n}=\IE^{j_1\ldots j_n}\left(\IE^{j_1\ldots j_n}(U(A_\alpha^{j_1\ldots j_n}))
                 -\IE^{j_1\ldots j_n}(U(A_\alpha))\right)-\sum_{k=1}^nC(S_{j_k})
\label{eq:bg-limited-mvi}
\end{equation}
where expectation $\IE^{j_1\ldots j_n}$ is computed according to the
immediate posterior belief distributions. When all of the actions
in the batch are identical, notation $MVI_j^n$ will be used, denoting
the myopic estimate of a batch of $n$ actions $S_j$.

Theoretical bounds of the myopic estimate are proved for some
restricted cases. In many applications, experiments show that the
assumptions work sufficiently well. When the assumptions turn out to
be unjustified, more sophisticated approximations can often be
designed by extending or replacing either the meta-greediness or
the single-step assumption.
