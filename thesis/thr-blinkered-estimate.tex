% $Id: thr-blinkered-estimate.tex,v 1.51 2009/12/22 13:22:36 dvd Exp $

\section{Semi-Myopic Value of Information Estimate}

\subsection{Shortcomings of the Myopic Estimate}
\label{sec:thr-mvi-shortcomings}

The decisions an optimization algorithm makes at each step are:
\begin{enumerate}
\item[1)] which measurement is the most valuable, and
\item[2)] whether the most valuable measurement has a positive value.
\end{enumerate}
The simplifying assumptions are justified when they lead to decisions
which are sufficiently close to optimal in terms of the performance
measure, the net utility.

The first decision controls the search `direction'.  When the
direction is wrong, a non-optimal item is measured, and thus more
measurements are done before arriving at a final decision. The net
utility decreases due to the higher measurement cost.  The second
decision determines when the algorithm terminates, and can be
erroneously made too late or too early. Made too late, the decision
leads to the same deficiency as above: the measurement cost is too
high. Made too early, it causes early and potentially incorrect item
selection, due to wrong beliefs. The net utility decreases because the
utility of the selected item is low. In terms of value of information,
the assumptions lead to correct decisions when
\begin{itemize}
\item if there exists a step according to the meta-greedy approach,
  that is, one with the greatest positive expected value, the step
  begins an optimal observation plan;
\item otherwise, if the greatest value of information of a single step (`the
  first step' would be a better term) is non-positive, the value is
  non-positive for every sequence of steps.
\end{itemize}
These criteria are related to the notion of {\it non-increasing returns};
the assumptions are based on an implicit hypothesis that the intrinsic
value grows more slowly than the cost. When the hypothesis is correct,
the assumptions should work well; otherwise, the myopic algorithm either
gets stuck or goes the wrong way.

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.75,trim=5pt 15pt 0pt 45pt,clip]{s-curve.pdf}
\end{center}
\caption{The s-curve} 
\label{fig:thr-s-curve}
\end{figure} 

It is commonly believed that many processes exhibit diminishing
returns; the {\it law of diminishing returns} is considered a
universal law in economics \cite{Johns.economics}. However, the law
only holds asymptotically: while it is often true that starting with
some point in time the returns never grow, until that point they can
alternate between increases and decreases. Figure~\ref{fig:thr-s-curve} presents the curve of diminishing returns (the
dashed curve), the s-curve (the solid curve), and the areas with
negative and positive values for the s-curved returns for a linear
cost (the dotted straight line). Investments for the first two units
of time do not pay off, and the maximum return is achieved at
approximately $2.8$.

A concave \cite{Wikipedia.Concave_function} cost function,
corresponding to the case of a non-free movement cost
(\ref{eq:greedy-statement-cost-decomposition}) also affects validity
of assumptions. For sequences of multiple measurements of the same
item or several closely located items, the movement cost is
effectively distributed between the measurements. Therefore, a batch
of measurements may have positive value while a single measurement's
intrinsic value of information cannot overcome its movement cost.

Sigmoid-shaped returns were discovered in marketing
\cite{Johansson.s-curve}.  As experimental results show
\cite{Zilberstein.sensing}, they are not uncommon in sensing and
planning. In such cases, an approach that can deal with increasing
returns must be used.

\subsubsection{Pathological Example}

Even in simple cases the myopic estimate may behave poorly.  Consider
the following example:

\begin {itemize}
\item $S$ is a set of two items, $s_1$ and $s_2$, where the value of
  $s_1$ is known exactly, $x_1=1$;
\item the prior belief about the value of $s_2$ is a normal
  distribution $p_2(x)=N(x;0,1)$;
\item the observation variance is $\sigma_e^2=5$;
\item the measurement cost is constant, and chosen so that the net
  value estimate of a two-observation step is zero: $c(j)=\frac {MVI_2^2} 2\approx
  0.00144$;
\item the utility is a step function:

\[u(x) = \left\{ 
\begin{array}{l l}
  0 & \quad \mbox{if $x<1$}\\
  0.5 & \quad \mbox{if $x=1$}\\
  1 & \quad \mbox{if $x>1$}\\
\end{array} \right. \]

\end {itemize}

The plot in Figure~\ref{fig:thr-mvilim-value-cost} depicts the intrinsic
value of information as a function of the number of observations in a single
step. The straight line corresponds to the measurement cost.

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.75,trim=0pt 15pt 0pt 45pt,clip]{mvilim-value-cost.pdf}
\end{center}
\caption{Intrinsic value and measurement cost} 
\label{fig:thr-mvilim-value-cost}
\end{figure} 

Under these conditions, the algorithm with one measurement per step
will terminate without gathering evidence because the value estimate
of the first step is negative, and will return item $s_1$ as
best. However, observing $s_2$ several times in a row has a positive
value, and the updated expected utility of $s_2$ can eventually become
greater than $u(s_1)$.  Figure~\ref{fig:thr-mvilim-value-cost} also
shows the intrinsic value growth rate as a function of the number of
measurements: the rate increases up to a maximum at 3 measurements,
and then goes down. Apparently, the myopic algorithm does not ``see''
as far as the initial increase.

\subsection{Semi-Myopic Schemes}
\label{sec:thr-semi-myopic-schemes}

The above pathological example was inspired by a phenomenon which was
encountered in a real-world problem, optimizing parameters in setups
of imaging machines (see Section~\ref{sec:emp-case-mms} for the case
study).  On data with varying prior beliefs, the myopic algorithm
almost never measured an item with high prior variance even when the
item was likely to become the best item after a sufficient number of
measurements.

Keeping the complexity manageable (the number of possible sensing
plans over continuous variables is uncountably infinite, in addition
to being multi-dimensional) while overcoming the premature termination
is the basis for the {\em semi-myopic} value of information
estimate. Let ${\mathcal M}$ be the set of all possible measurement
actions, and ${\mathcal C}$ be a constraint over sets of
measurements from ${\mathcal M}$. In the semi-myopic framework,
all possible subsets (batches) $B$ of measurements from ${\mathcal M}$
that obey the constraint ${\mathcal C}$ are considered, and for each such subset
${\mathcal B}$ a `batch' value of information estimate is computed
in assumption that all measurements in ${\mathcal B}$ are made, followed by
a decision (selection of an item). Then, the batch
${\mathcal B}^*$ with the best value estimate is chosen. Once the best
batch ${\mathcal B}^*$ is chosen, there are still several options
(\cite{BilgicGetoor.voila} discusses a similar approach to discrete
feature acquisition):

\begin{enumerate}
\item Actually do all the measurements in ${\mathcal B}^*$.
\item Attempt to optimize ${\mathcal B}^*$ into some form of
  conditional plan of measurements and the resulting observations.
\item Perform the best measurement in ${\mathcal B}^*$.
\end{enumerate}

In all cases, after measurements are made, the selection is repeated,
until no batch has a positive value, at which point the algorithm {\em
  terminates} and selects an item. Option 1, a combination of online
and offline selection, misses the possibility to choose better
measurements while executing a batch, and therefore is inferior to
options 2 and 3. Indeed, even if according to the prior beliefs all of
the measurements must be spent on item $x$, a single measurement of
$x$ can change the beliefs and perform further measurements of the same
item unworthy. While limited efficient implementation for option 2 is
possible, optimizing a conditional plan is intractable in general,
recreating the optimal policy problem on a smaller scale.  However,
option 3, with a suitable constraint, remains tractable and offers an
opportunity to make a better choice at each step.

The pseudocode for the algorithm that implements a semi-myopic scheme
is provided in Listing~\ref{alg:thr-semimyopic-algorithm}.
\begin{algorithm}
\caption{Semi-Myopic Algorithm}
\label{alg:thr-semimyopic-algorithm}
\begin{algorithmic}[1]
\STATE $budget \leftarrow C$
\STATE initialize beliefs
\LOOP
  \FORALL {items $s_i$}
    \STATE compute $\IE(U_i)$
  \ENDFOR
  \FORALL {batches $b_j$ satisfying constraint $\mathcal{C}$}
    \IF {$cost(b_j) \ge budget$}
      \STATE compute $V_j^b$
    \ELSE
      \STATE $V_j^b \leftarrow 0$
    \ENDIF
  \ENDFOR
  \STATE $j_{max} \leftarrow \arg \max\limits_j V_j^b$
  \IF {$V_{j_{max}}^b>0$}
    \FORALL {measurements $m_k \in b_{j_{max}}$}
       \STATE compute $V_k$
    \ENDFOR
    \STATE $k_{max} \leftarrow \arg \max\limits_k V_k$
    \STATE perform measurement $m_{k_{max}}$
    \STATE update beliefs
    \STATE $budget \leftarrow budget-c_{k_{max}}$
  \ELSE
    \STATE {\bf break}
  \ENDIF
\ENDLOOP
\STATE $\alpha \leftarrow \arg \max \IE(U_i)$
\RETURN $x_\alpha$
\end{algorithmic}
\end{algorithm}
In the algorithm, value of information is computed twice: value of
information $V_j^b$ of every batch $b_j$ satisfying the constraint
$\mathcal{C}$ is computed in lines 7--13; then, if the maximum value
of information of a batch is positive, value of information $V_k$ of
every single measurement $m_k$ belonging to the batch $b_{j_{max}}$ is
computed in lines 16--18, and a measurement with the highest value of
information (which can be negative) is performed in line 20.

The constraint $\mathcal{C}$ is crucial and defines a {\em
  scheme}. For the empty constraint --- the {\em exhaustive} scheme
--- all possible measurement sets are considered; this scheme has an
exponential computational cost, while still not guaranteeing
optimality.  At the other extreme is the constraint where only
singleton sets are allowed. This extreme results in the greedy
single-step assumption --- the original myopic scheme.
\cite{BilgicGetoor.voila} proposes a constraint based on the {\em
  value of information lattice}, a data structure introduced in the
paper. The induced number of subsets is still exponential in a general
case, but for certain kinds of dependency structure the constraint
gives significant improvement. Yet another --- {\em omni-myopic} ---
constraint can be constructed along the lines of
\cite{Heckerman.nonmyopic}, where initial subsequences of the sequence
of all measurements in the decreasing order of the myopic value of
information estimate are considered.

This work investigates the constraint that restricts the batches to
repeated identical measurements---the {\em blinkered} scheme. This
scheme is tractable, corresponds to a common approach in which noisy
experiments are repeated to increase confidence, and demonstrates
improved algorithm efficiency in empirical evaluations.

\subsection{Blinkered Estimate}
\label{sec:thr-blinkered-estimate}

As stated, the blinkered scheme considers sets of independent identical
measurements; this constitutes unlimited lookahead, but along a single
``direction'', as if one ``had one's blinkers on''.  Although this
scheme has a computational overhead over the myopic one, the factor is
only linear in the budget. \footnote{This complexity assumes either
  normal distributions, or some other distribution that has compact
  statistics. For general distributions, sets of observations may
  provide information beyond the mean and variance, and the resources
  required to compute value of information may even be exponential in
  the number of measurements.}  The ``blinkered'' value of information
is defined as:
\begin{eqnarray}
\label{eq:thr-bvi}
BVI_i&=&\max_k MVI_i^k\nonumber\\
\mathrm{s.t.}:&&c_{mi}+c_{ei}k \leq C
\end{eqnarray} 
where $c_{mi}$ and $c_{ei}$ are the movement and evaluation costs of
the $i$th measurement, correspondingly.

Driven by this estimate, the blinkered scheme selects a single
measurement of the item where {\em some} number of measurements gains
the greatest value of information.  A single step is expected to be
just the first one in the right direction, rather than to actually
achieve the value. Thus, the estimate relaxes the {\it single-step}
assumption, while still underestimating the value of information.

For budget $C$ and a single measurement cost bounded from below
 by $c$ the time to compute the estimate $T_{BVI}$ is:
$T_{BVI}=O\left(T_{MVI}\frac C c\right)$. This time bound is still
exponential in the representation, but can be made polynomial in one of
the following ways:

\begin{itemize}
\item If $MVI^k$ is a unimodal function of $k$, which can be shown for some
forms of distributions and utility functions,
the time is logarithmic in the budget $C$. Indeed, using the
bisection method to find $k$ for which $MVI^{k+1}-MVI^k$ changes sign
yields the logarithmic time.
\item Otherwise, $BVI$ can be estimated as the maximum for powers of
  2: $k=2^{k'}$.
\end{itemize}

In the presence of inexact recurring measurements, the blinkered
scheme plays a role similar to the role of the myopic scheme for exact
measurements. It seems to be the simplest approximation that still
works for a wide range of conditions under realistic assumptions.

\subsection{Utility Function}

The utility function should possess particular properties to justify the use
of the blinkered scheme. The scheme is worth using when $BVI$ is
likely to be different from $MVI$.

A simple case of a scalar feature and a measurement of a non-$\alpha$
item that does not affect the utility of the $\alpha$ item helps
understand influence of the utility function on the estimates. In the
case of an item with the normal belief $\mathcal{N}(\mu_-,
\sigma_-^2)$ and a measurement with the centered observation error
$\mathcal{N}(0, \sigma_e^2)$, the intrinsic value of information of
$n$ measurements, or the myopic estimate of an $n$-measurement step,
is:

\begin{equation}
MVI^n=\int\limits_{-\infty}^{\infty}p\left(\frac {x-\mu} {\sigma_\mu}\right)
 \max \left(\int\limits_{-\infty}^{\infty}u(y)p\left(\frac {y-x}
      {\sigma_+}\right)dy- \IE(U_\alpha), 0 \right)dx
\label{eq:thr-ustruct-mvi}
\end{equation}
where $u$ is the utility function, and $\sigma_+$, $\sigma_\mu$ are
defined by the following formulas:

\begin{eqnarray}
\sigma_+^2&=&\frac {\sigma_-^2\sigma_e^2} {n\sigma_-^2+\sigma_e^2}\nonumber\\
\sigma_\mu^2&=&\frac {n\sigma_-^4} {n\sigma_-^2+\sigma_e^2}
\end{eqnarray}


The inner integral depends on the utility function. When the utility
function is linear, $u(x)=x$, the integral's value is the mean,
$x$. If $u$ is concave in the range in which the posterior utility is
greater than $\IE(U_\alpha)$, the inner integral increases when the
observation variance $\sigma_e$ decreases, and the intrinsic value of
information grows faster with the number of measurements.

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.75,trim=0pt 15pt 0pt 45pt,clip]{utility-shape.pdf}
\end{center}
\caption{Shape of utility function}
\label{fig:thr-utility-shape}
\end{figure} 

Utility functions are usually bounded and a variant of the S-shape
(Figure~\ref{fig:thr-utility-shape}). The further the utility
function from the linear function and the closer it is to the step
function, the more pronounced the difference between $MVI$ and $BVI$
can be.

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.75,trim=0pt 15pt 0pt 45pt,clip]{ustruct.pdf}
\end{center}
\caption{Isolines of intrinsic value}
\label{fig:thr-ustruct}
\end{figure} 

In Figure~\ref{fig:thr-ustruct} the solid lines correspond to intinsic
value of information levels equal to multiples of $c=0.000865$ for the
step utility function (see example in Section~\ref{sec:thr-mvi-shortcomings}), $\mu_-=0.0$, $U_\alpha=0.5$. When the
measurement cost is equal to $c$, the value on the lower isoline,
$MVI=0$ for points on the isoline. For $\sigma_-^2=1$, $\sigma_e^2=5$ a
single measurement has a negative value, while two, as well as three
and four, measurements have a positive value. Thus, the blinkered
scheme would perform the measurement, while the myopic scheme would
not.

\subsection{Theoretical Bounds}

Bounds on the blinkered scheme are established for two special cases.
The first bound shows that the scheme does not stop too early.

\subsubsection {Single Measurable Item}

\begin{thm}
\label{th:bound-single} 
Let $S=\{ s_1, s_2\} $, where the value of $s_1$ is known exactly. Let
$C_b$ be the remaining budget of measurements when the blinkered
scheme terminates. Then the (exact) value of information of an optimal
policy from this point onwards is at most $C_b$.
\end{thm}

\begin{proof}[Proof] 
The intrinsic value of information over the remaining budget when the
blinkered scheme terminates $\Lambda_b$ is at most the remaining
budget $C_b$, since otherwise the scheme would not have terminated. There is
only one kind of measurements, thus the intrinsic value of information
$\Lambda_o$ achieved by an optimal policy is at most equal to that of
all measurements, $\Lambda_o \leq \Lambda_b \leq C_b $.  Since
measurement costs are positive, the value of information $V_o$ of an
optimal policy must therefore also be at most $C_b$.
\end {proof}

The bound is asymptotically tight. Consider the following problem
instance:
\begin{itemize}
  \item the utility of $s_1$ is exactly known an equal $0$;
  \item according to the prior belief, $s_2$ can have utility $- \frac
h    {C_b} p$, $0$ or $\frac {C_b} p$ with equal probability, for some
    $0 \le p \le 1$;
  \item the measurement cost function is defined as
    follows\footnote{The second measurement is more expensive than the
      first one when, for example, in a physical experiment one has to
      wait in order to exclude inference between the measurements.}:
    \begin{eqnarray*}
      c(x_0, x_2)&=&0\\
      c(x_2, x_2)&=&C_b,
    \end{eqnarray*} that is, at most two measurements can be made,
    the first measurement is free, the cost of the second measurement
    is $C_b$;
  \item the first measurement of $s_2$ either discovers with
    probability $p$ that the utility of $s_2$ is $- \frac
    {C_b} p$ or $\frac {C_b} p$, or, with probability $1-p$ that the
    utility is exactly $0$;
  \item the second measurement of $s_2$ determines the exact utility
    of the item.
\end{itemize} 

The blinkered value of information estimate is $0$: the intrinsic
value of information of the first measurement is $0$, and of the first
and the second measurement is $\frac {C_b} p p=C_b$ --- the utility of
$s_2$ is always determined exactly, and is $\pm\frac {C_b} p$ with
probability $p$ and $0$ with probability $1-p$. Thus, the blinkered
scheme will not perform any measurements.

The optimal policy is to 
\begin{enumerate}
  \item perform the first measurement,
  \item if the utility of $s_2$ is not zero, perform the second
    measurement. 
\end{enumerate} The probability of the second measurement is $p$
and the expected cost of the policy is $pC_b$. The value of
information of the optimal policy is thus:
\[V_o=p\frac {C_b} p - pC_b=(1-p)C_b\]
and approaches $C_b$ when $p$ approaches $0$:
\[ \lim_{p\to 0}V_o=C_b \]
The latter limit shows that the bound stated in Theorem~\ref{th:bound-single} is asymptotically tight.

\subsubsection {Multiple Items}

The second bound is related to a common case of a finite budget and
free measurements. The bound provides certain performance guarantees
for the case when dependencies between items are sufficiently weak,
and shows that the blinkered scheme chooses reasonable search
directions under weaker assumptions than those for the myopic scheme.

\begin{dfn}
\label{dfn:mutually-subadditive}
Measurements of two items $s_1$, $s_2$ are mutually subadditive if,
given sets of measurements of each item $Q_1$ and $Q_2$, the intrinsic
value of information of the union of the sets is not greater than the
sum of the intrinsic values of each of the sets, i.e.: $\Lambda(Q_1
\cup Q_2) \le \Lambda(Q_1) + \Lambda(Q_2)$
\end{dfn}

\begin{thm}
\label{th:bound-multiple}
For a set of $M$ items, the zero measurement cost $c=0$, and a finite
budget of $N$ measurements, if measurements of every two items are
mutually subadditive, the value of information of the blinkered scheme
is no more than a factor of $M$ worse than the value of information of
an optimal algorithm.
\end {thm}

\begin{proof} Since the measurement cost is $0$, the net value of information
is the same as the intrinsic value, $V=\Lambda$.  Value of information cannot
decrease due to additional measurements, therefore the value of any
sequence of measurements containing all of the measurements in an optimal
sequence is at least as high as the value $V_o$ of an optimal
policy. Consider an exhaustive sequence containing $N$ measurements of each
of the $M$ items, $N\cdot M$ measurements total, with value $V_e$. The
exhaustive sequence contains all measurements made according to an optimal
policy within the budget, thus $V_o\le V_e$.

Let $s_i$ be the item with the highest blinkered value for $N$
measurements, denote its value by 
$V_{b\,i_{max}}=\max_i V_{b\,i}$. Since measurements of different items are mutually
submodular, $V_{e} \le MV_{b\,i_{max}}$, 
and thus $V_{b\,i_{max}}\ge \frac {V_o} n$.

The blinkered scheme selects at every step a measurement from a sequence
with value of information which is at least as high as the
value of measurements of $s_i$ for the rest of the
budget. Thus, its value of information $V_b \ge V_{b\,i_{max}}
\ge \frac {V_o} n$.
\end{proof}

The bound is asymptotically tight. Construct a problem instance
as follows: $M$ items, with value of information of $j$th item for $i$
measurements $v_j(i)$:

\[ v_1(i) = \sqrt[k] {\frac i N} \]

\[ v_{j>1}(i) = \left\{ 
\begin{array}{l l}
  0 & \quad \mbox{if $i<\frac N M$}\\
  \sqrt[k] {\frac 1 M} & \quad \mbox{otherwise}\\
\end{array} \right. \]

Value of information of a combination of the measurements
is the sum of the values for each item:

\[ v(i)=\sum_{j=1}^M v_j(i_j) \]

Here, the optimal policy is to measure each item $\frac N M$ times. The resulting
value of information for $N$ measurements is:

\[ V_o = v_1\left(\frac N M\right)+(M-1)v_{j>1}\left(\frac N M\right)
       = M \sqrt[k] {\frac 1 M} \]

\[ \lim_{k\to\infty}M \sqrt[k] {\frac 1 M} = M \]

But the blinkered scheme will always choose the first item with \[
V_b=v_1(N)=1. \]

\subsubsection {Comparative Example}

In the following example the blinkered scheme selects better
measurements than the myopic one. Consider a modification of the example in
Section~\ref{sec:thr-mvi-shortcomings}, with an additional item $s_3$ and
free measurements:

\begin {itemize}
\item $S$ is a set of three items, $s_1$, $s_2$, and $s_3$;
\item the value of $s_1$ is known exactly, $x_1=1$;
\item the prior belief about $s_2$ is nomal $p_2(x)=\mathcal{N}(x;0,1)$;
\item the observation variance of $s_2$ is $\sigma_{e2}^2=5$;
\item the prior belief about $s_3$ is  normal: $p_3(x)=\mathcal{N}(x;0.0,0.16)$;
\item a measurement of the item returns the exact value: $\sigma_{e3}^2=0.0$;
\item measurements are free: $c=0$;
\item the budget is two measurements;
\item the utility is a step function:

\[u(x) = \left\{ 
\begin{array}{l l}
  0 & \quad \mbox{if $x<1$}\\
  0.5 & \quad \mbox{if $x=1$}\\
  1 & \quad \mbox{if $x>1$}\\
\end{array} \right. \]

\end {itemize}

The myopic scheme measures $s_3$, and then $s_2$, since
$MVI_3 \approx 0.0024>MVI_2\approx 0.0004$. However, the blinkered
scheme first measures $s_2$, since $MVI_2^2 \approx
0.0028>MVI_3^2 \approx 0.0024$, and then chooses between $s_2$ and
$s_3$ depending on the obtained observation. In particular, if the
posterior mean of $s_2$ is approximately in the range $[0.39, 1.6]$,
$s_2$ is chosen, otherwise, $s_3$ is chosen.

It is easy to show that only these two scenarios are possible:
\begin{enumerate}
\item measure $s_3$, and then $s_2$;
\item measure $s_2$, and then either $s_3$ or $s_2$, depending on
  the value of each measurement;
\end{enumerate}
and the latter policy is optimal. Thus, the blinkered scheme
implements the optimal policy, while the myopic one misses cases where
$s_2$ should be measured twice for the best expected result.  Of
course, one can construct an example where the blinkered scheme is far
from optimal. Still, the empirical evaluations show that the blinkered
scheme behaves significantly better than the myopic one, and gives
good results in cases where the myopic scheme fails.

