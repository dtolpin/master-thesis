% $Id: intro.tex,v 1.14 2009/09/05 16:33:35 dvd Exp $

Optimization under uncertainty is a field of active research with
applications in diverse areas. Seemingly different in nature, problems
in which a best choice must be made without knowing the exact utilities of
available options can be uniformly described as a problem of
optimization under uncertainty and solved using common methods. Oil
and gas exploration, climate and water monitoring, and design of computer
or physical experiments are just some examples of such problems.

Both traditional (stochastic programming, fuzzy mathematical
programming) and emerging methods solve the optimization problem by
choosing an uncertainty model and selectively evaluating the function
that must be optimized. Efficiency and robustness of a method on a
certain type of problems depend on the extent to which the selected
method reflects the problem features and optimization constraints: the
amount of computation and the number of evaluations before the choice
is made can be limited, correlations between values of the function
for different arguments should be taken into account, and new
knowledge gathered at earlier stages of the optimization algorithm
should be used to drive the search for the optimal choice.

It is possible, often through trial and error, to find a viable
algorithm for each isolated case. However, a universal method equally
applicable to all or most of the challenges is preferable: such a
method allows, through translating each problem instance into a
compact abstract representation, to reason consistently about
performance and robustness, and universally reuse gained experience.
A popular universal method combines a Bayesian representation of
uncertainty and a search algorithm based on limited rationality.  The
decision that drives the search in this method is straightforward: the
algorithm always selects an action which on average---according to the
current beliefs---brings the highest benefit; due to its
short-sightedness, the search algorithm is metaphorically called {\em
myopic}.  Despite the seeming simplicity, the myopic algorithm is
known to perform well on many practical problems; in particular,
problems which exhibit {\em diminishing returns} on the search
effort. For some cases, the algorithm is proved to return
nearly-optimal solutions.

However, in a case when multiple inexact evaluations of the optimized
function for each combination of arguments are allowed and together
increase confidence about the value of the function, the algorithm
performs poorly; in particular, when a change in argument values
decreases the overall reward. This case is frequently met: one example
is a medical experiment in which a potentially dangerous combination
of drugs is tried on mice. Preparing a new mixture with different
proportions or constituents is expensive, and trying the same mixture
on multiple mice increases confidence in the absence (or presence) of
negative effects of the combination.

The objective of this work is to propose an extended, non-myopic
algorithm which handles well the case of multiple recurring
measurements, explore its theoretical properties and empirically
evaluate its performance and robustness. The rest of the work is
organized as follows: in Chapter~\ref{ch:bg}, the important background
in optimization under uncertainty, limited rationality and graphical
dependency models is reviewed, and related research directions are
presented. Chapter~\ref{ch:greedy} formally states the problem of
optimization under uncertainty and reformulates the existing myopic
greedy algorithm. Chapter~\ref{ch:thr} introduces the semi-myopic
scheme, and, in particular, the blinkered scheme and the blinkered
value of information estimate. Empirical evaluation of the blinkered
scheme is provided in Chapter~\ref{ch:emp}. Chapter~\ref{ch:conclusion}
concludes the work giving general recommendations about
applicabitility of the new scheme, enumerating the contributions of
the work and pointing at possible future research direction. Appendix~\ref{app:uncertima} describes {\sc uncertima}, the toolkit for
optimization under uncertainty developed in the course of the research.

\section*{Acknowledgments}

The research is partially supported by the IMG4 consortium under the
\mbox{MAGNET} program, funded by the Israel Ministry of Trade and
Industry, and by the Lynne and William Frankel Center for Computer
Sciences.
