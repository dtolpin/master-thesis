% $Id: emp-case-svm.tex,v 1.14 2009/09/05 16:33:35 dvd Exp $

\section{Case Study: Optimization of SVM Parameters}

Support Vector Machine (SVM) is a popular family of supervized
learning methods for classification and regression
\cite{Bishop.learning}. SVM-based classifiers are efficient and
robust. However, most kernel functions are parameterized, and the
parameters must be properly chosen for each particular kind of data in
order to achieve good classification accuracy.

An SVM classifier based on the {\em radial basis function} has two
parameters: $C$ and $\gamma$. While there are heuristics for selecting
an initial approximation for a good parameter combination, an
efficient algorithm for determining their values is not known and
several different combinations must be tried. The parameter search
space is usually exponential, such that $C_i=\alpha^i$ and
$\gamma_j=\beta^j$, and dependency of the classification
accuracy on the parameters has a shape similar to shown in Figure~\ref{fig:emp-svm-accuracy}. 
\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.8,trim=100pt 75pt 100pt 50pt]{svm.pdf}
\end{center}
\caption{SVM classification accuracy as function of $C, \gamma$.}
\label{fig:emp-svm-accuracy}
\end{figure}
A point in the upper flat region, preferably away from the edges,
corresponds to a good parameter combination, and points lying in the
lower flat region yield accuracy of a random choice.

A trial for a combination of parameters determines estimated accuracy
of the classifier through cross-validation. One possibility is to use
the full training data set for each trial, determining the accuracy
with high confidence, that is, to perform an exact, or an almost
exact, measurement. However, the time required to estimate the
accuracy is roughly quadratic in the size of the data set, and with
larger sets the computation time can become prohibitive.

An alternative approach is to perform the trials incrementally: the
data set is divided into smaller subsets, such that estimating the
accuracy on each subset is imprecise but fast, and the trials are
repeated multiple times in some of the locations to increase the
confidence, each time with a different subset. The latter approach is
more time-efficient, and is based on recurring imprecise measurements.
This case study compares the myopic scheme for the full approach with
both the myopic and the blinkered scheme for the incremental
approach. It is anticipated that the best results are achieved through
combining the incremental approach with the blinkered scheme.
\subsection{Data Sets}

Two data sets are used for the case study: {\sc usps}
\cite{Hull.dataset} and {\sc svmguide2} \cite{Hsu.dataset}. While the 
data sets have a different number of features and specimens, the
most essential difference is in the number of classes: 10 and 3
correspondingly. Due to the difference in the number of classes, the
accuracy for a poor combination of parameters, which can be
estimated as $\frac 1 {number-of-classes}$, is different, as well as
the steepness of the slope between the good and the bad regions.

\begin{description}
\item[{\sc usps}] --- 10 classes, 2007 specimens, 256 features;
\item[{\sc svmguide2}] --- 3 classes, 391 specimens, 20 features.
\end{description}

\subsection{Optimization Problems}

Two optimization problems are defined, one for each of the approaches. The
problem definitions are the same for both data sets, and the
parameters are estimated from the data. For the incremental approach,
the data sets are divided into 8 equal parts. It is essential that the
same problem definitions used for both data sets; the same algorithm
should be applicable to different problem instances without much
tuning.

The problem definitions are provided below. The utility function in
both problems is $tanh(4(x-0.5))$, the $C$ and $\gamma$ axes are
scaled for uniformity to ranges $[1..21]$ and there are uniform
dependencies along both axes. The difference is in the accuracy and
the cost of a trial (a measurement).

\subsubsection{Full Trials}

\begin{verbatim}
problem svm {
  space (cost, gamma) {
    cost = [1, 4 .. 21];
    gamma = [1, 4 .. 21];
  }
  observe accuracy {
    1=> accuracy = 0.01 / 0.025;
  }
  find max util(cost, gamma) = tanh(4*(accuracy-0.5));
  model lattice(cost, gamma) {
    accuracy = (0.3, 1.0) / (0.4, 0.4);
  }
}
\end{verbatim}

\subsubsection{Incremental Trials}

\begin{verbatim}
problem svm {
  space (cost, gamma) {
    cost = [1, 4 .. 21];
    gamma = [1, 4 .. 21];
  }
  observe accuracy {
    1=> accuracy = 0.25 / 0.01;
  }
  find max util(cost, gamma) = tanh(4*(accuracy-0.5));
  model lattice(cost, gamma) {
    accuracy = (0.3, 1.0) / (0.4, 0.4);
  }
}
\end{verbatim}

\subsection{Experiment and Representation of Results}

The experiment design is similar to that of the function optimization
in the previous section (see Section~\ref{sec:emp-functions-experiment}). The myopic scheme is run for
the full approach, and both the myopic and the blinkered schemes
are run on each data set for the incremental approach, 64 times in
each case. The plots comparing the selected locations and the utility
distributions are presented, and an analysis of the results is
provided.  In each figure (Figures~\ref{fig:emp-svm-usps},~\ref{fig:emp-svm-svmguide2}), the contour plot of the actual accuracy as
a function of the parameters is provided for
reference (the only plot in the left column). In the middle and
the right column, the first row corresponds to the full
trials, the second and the third row---to the myopic and the
blinkered scheme for the incremental trials. The plots in the middle
column show the combinations of parameters chosen by the algorithm on
each run (slightly randomly moved, as before, to show multiply
selected combinations), and the plots in the right column
depict distributions of the net utility of the result.

\subsection{Discussion of Results and Conclusions}.

Parameters for the {\sc usps} data set (Figure~\ref{fig:emp-svm-usps})
were successfully found both by the full and the incremental
approaches, apparently due to the high contrast between the utilities of
the best and the worst combinations. For the incremental approach, the
blinkered scheme gave better results: the mean reward is $1.23$ for
blinkered, $1.18$ for myopic; the greater reward was achieved because
the total measurement cost was smaller on average for the blinkered
scheme: the budget surplus is $0.33$ for blinkered, $0.26$ for
myopic while the mean utility is the same: $0.90$ for both schemes.

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.75,trim=100pt 25pt 100pt 0pt]{svm-usps.pdf}
\end{center}
\caption{The {\sc usps} data set.}
\label{fig:emp-svm-usps}
\end{figure}

{\sc svmguide2} data set was difficult for the full approach; most
runs fully exhausted the budget, and in several cases the selected
parameter combinations yielded the accuracy of a random choice. The
myopic scheme for the incremental approach selected an optimal
combination in most runs, but there still were quite a few
outliers. The blinkered scheme, however, succeeded in always selecting an
optimal combination. The mean reward is $0.88$ for the
full approach, $1.10$ for the myopic, $1.12$ for the blinkered scheme.

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.75,trim=100pt 25pt 100pt 0pt]{svm-svmguide2.pdf}
\end{center}
\caption{The {\sc svmguide2} data set.}
\label{fig:emp-svm-svmguide2}
\end{figure}

Advantage of the blinkered scheme over the myopic one depends on
problem parameters and features of a particular problem
instance. However, as the study shows, the blinkered scheme is at
least as good as the myopic one, and maintains robustness for a wider
range of problems and their instances.
