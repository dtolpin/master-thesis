\section{Conclusions}

The semi-myopic scheme introduced in this work extended applicability
of the greedy optimization algorithm to problems with recurring
inexact evaluations of the function being optimized. A particular kind
of the myopic scheme, the blinkered scheme, and the correspoding
blinkered value of information estimate were shown to cope well with
problems for which the myopic scheme fails to maintain robustness. 
Theoretical bounds were proved for important boundary cases, thus
providing certain guarantees that the blinkered scheme selects evaluation
parameters rationally and does not stop too early.

The conjecture is that the blinkered value of information estimate
plays in optimization problems with recurring inexact evaluations a
role similar to the myopic estimate for problems with exact
evaluations: it is the simplest estimate that ensures robustness of
the optimization algorithm under realistic assumptions. Advantage of
the blinkered estimate is most pronounced when a change in 
evaluation parameters decreases the final reward (``the movement
has a cost''), or when the utility function is S-shaped.

The blinkered scheme can be implemented efficiently, with only a small
overhead compared to the myopic scheme. A reference implementation of
the blinkered scheme showed good performance and consistent robustness
on a number of challenging optimization problems. Careful design of
data structures and deliberation logic resulted in an optimization
algorithm capable of solving optimization problems with multi-argument
utility functions, hundreds of nodes in the grid, and thousands of
possible measurements at each point in time.

\section{Contributions}

The blinkered scheme constitutes both a theoretical and a practical
contribution to optimization under uncertainty using limited
rationality. The main aspects of this contribution are summarized
below.

\begin{description}
\item[Semi-myopic framework:] The semi-myopic framework extends the
  myopic algorithm design. The framework provides means for a more
  efficient on-line selection of measurements while still maintaining
  computability. Different semi-myopic schemes within the framework
  are suitable for different classes of problems and allow adaption of
  the general algorithm through parameterization by the scheme.

\item[Blinkered scheme:] The blinkered scheme, and the corresponding
  blinkered value of information estimate, extends applicability of the
  greedy algorithm to the case of recurring inexact measurements. The
  blinkered scheme is believed to be the simplest scheme that is still
  robust and efficient enough under realistic assumptions when
  measurements are inexact and multiple measurements of the same item
  are allowed. Theoretical bounds are proved for two important special
  cases. Results of empirical evaluations on both artificial and
  real-world data are provided, and demonstrate advantage of the
  blinkered scheme over the purely myopic approach.

\item[Toolkit for optimization under uncertainty:] A toolkit for
  optimization under uncertainty has been developed. The toolkit is
  both a research environment and a software package suitable for
  industrial deployment. Different optimization algorithms and
  dependency models are implemented in the toolkit; new algorithms can
  be added, and their performance can be evaluated using the graphical
  analysis tools. In particular, the toolkit was used to conduct the
  empirical evaluation described in this work, and proved to be a
  convenient and powerful research tool.
\end{description}

\section{Future Research}

Properties of the estimate have been investigated only partially for
the dependent case, which is of more practical importance.  In
particular, when, due to sufficiently strong dependencies,
observations in different locations are not mutually subadditive, the
blinkered estimate alone may not prevent premature termination of the
measurement plan, and its combination with the approach proposed in
\cite{Heckerman.nonmyopic} may be worthwhile.

During the algorithm analysis, several assumptions have been made
about the shape of utility functions and belief distributions.
Certain special cases, such as normally distributed beliefs and
s-shaped utility functions, are frequently met in applications and may
lead to stronger bounds and discovery of additional features of semi-myopic
schemes.

An efficient implementation of the optimization algorithm demands
ubiquitous reuse of computations. While some ideas were formulated and
implemented (Section~\ref{sec:thr-computation-reuse}), a lot of work
remains to be done both in theoretical analysis and in empirical
evaluation. The work should result in an algorithm that scales to
large problems and can be deployed in real-time environments.

The myopic algorithm acts as though the time is unlimited, and
takes the budget constraint into account only as a termination
condition. This behavior is reasonable when the utility and
the costs are such that in most runs the algorithm stops because it
cannot find a measurement with a positive value of
information. However, when the budget is tight, an algorithm
that chooses measurements based on the net value obtained per cost
unit, similar to the greedy approximation algorithm for the Knapsack
problem \cite{Wikipedia.Knapsack_problem}, is worth consideration.
