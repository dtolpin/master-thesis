% $Id: emp-case-mms.tex,v 1.13 2009/09/07 13:29:46 dvd Exp $

\clearpage
\section{Case Study: Metrology Machine Setup}
\label{sec:emp-case-mms}

The concluding case study considers a complicated optimization
problem. The problem arised in the field of metrology equipment setup:
two parameters, the focus and the filter color must be chosen to
optimize confidence of outcomes of subsequent measurements of a wafer
for electronic chip production.

For each focus and color filter, the measurements can be performed in
several locations (sites), and for two orientations of the wafer. The
utility function (see the problem definition below) depends on 6
compound features; and each of the features is an aggregate expression
over the sites.

The dependency model consists of 5 (by the number of filter colors)
unconnected three-dimensional lattices (an axis along the focuses and
two axes in the wafer plane, with measurement sites in the nodes).

The movement cost is a step function of parameter combinations. The
fact of change of a particular parameter, not the magnitude,
determines the movement cost. There are less and more expensive
movements: for example, changing the wafer orientation is 3 times more
expensive than changing the filter. 

\subsection{Optimization Problem}

To save the computation time for simulations, only each third focus (1, 4,
7, ... 31) and only 3 of 9 sites are considered. Even with this
simplifications, there are 330 possible measurements and 1980
optimization features in the dependency model at each point in time. 

\begin{verbatim}
problem wafer {
  space (sitex, sitey, focus, color, angle) /
    0.004*(nz(abs(@sitex)+abs(@sitey)+abs(@focus))+nz(@color)+3*nz(@angle)) {
    sitex = [-1 .. 1];
    sitey = 0;
    focus = [1, 4 .. 31];
    color = [1 .. 5];
    angle = [0, 180];
  }
  observe (x, y, px, py) {
    1 => (x, y, px, py) = (1.0, 1.0, 1.0, 1.0) / 0.002;
  }
  find max merit(focus, color) =
    0.5-0.083*(tanh(3*(xprec-1))+tanh(3*(yprec-1))
               +tanh(1.5*(xtis-2))+tanh(1.5*(ytis-2))
               +tanh(1.5*(xtsv-2))+tanh(1.5*(ytsv-2))) {
    xprec = mean(px);
    yprec = mean(py);
    xtis = abs(mean(x[angle=180]+x[angle=0]));
    ytis = abs(mean(y[angle=180]+y[angle=0]));
    xtsv = sd(x[angle=180]+x[angle=0]);
    ytsv = sd(y[angle=180]+y[angle=0]);
  }
  model lattice(focus, sitex, sitey) {
    x = (1.0, 25.0) / (1.0, 4.0, 4.0);
    y = (1.0, 25.0) / (1.0, 4.0, 4.0);
    px = (1.0, 25.0) / (1.0, 4.0, 4.0);
    py = (1.0, 25.0) / (1.0, 4.0, 4.0);
  }
}
\end{verbatim}

\subsection{Experiment and Representation of Results}

Simulation data for two wafers, A and B, was used. For each of the
wafers, the myopic and the blinkered scheme were run 10 times. The
selected combinations are marked by circles on the true utility plots
(Figures~\ref{fig:wafer-156-130-myopic}--\ref{fig:wafer-412-120-blinkered})\footnote{As
  before, the coordinates were slightly randomized to show multiple
  choices in the same location.}. In the plots, the horizontal axis
corresponds to the focus, the color of each line denotes the filter
color, and the vertical axis corresponds to the intrinsic utility of
the combination.

\subsection{Discussion of Results and Conclusions}

The experiment results are presented in
Figures~\ref{fig:wafer-156-130-myopic}--\ref{fig:wafer-412-120-blinkered}. The
results for both wafers are similar: the blinkered scheme selected
parameters with an almost optimal utility in all but one case, while
the myopic scheme exhausted the budget or failed to select a next
measurement before gathering sufficient evidence in about one third of
the runs, selecting parameter combinations with a low utility.

Both the myopic and the blinkered scheme used up the measurement
budget in most runs. Therefore, the blinkered scheme was advantageous
in this problem not just because this scheme prevents premature termination,
but also because better measurement locations  are chosen and
the evidence is gathered more efficiently.

\newpage
\begin{figure}[t]
\begin{center}
\includegraphics[scale=0.75,trim=100pt 25pt 100pt 50pt]{wafer-156-130-myopic.pdf}
\end{center}
\caption{Wafer A, myopic scheme.}
\label{fig:wafer-156-130-myopic}
\end{figure}

\begin{figure}[t]
\begin{center}
\includegraphics[scale=0.75,trim=100pt 25pt 100pt 50pt]{wafer-156-130-blinkered.pdf}
\end{center}
\caption{Wafer A, blinkered scheme.}
\label{fig:wafer-156-130-blinkered}
\end{figure}

\newpage
\begin{figure}[t]
\begin{center}
\includegraphics[scale=0.75,trim=100pt 25pt 100pt 75pt]{wafer-412-120-myopic.pdf}
\end{center}
\caption{Wafer B, myopic scheme.}
\label{fig:wafer-412-120-myopic}
\end{figure}

\begin{figure}[t]
\begin{center}
\includegraphics[scale=0.75,trim=100pt 25pt 100pt 75pt]{wafer-412-120-blinkered.pdf}
\end{center}
\caption{Wafer B, blinkered scheme.}
\label{fig:wafer-412-120-blinkered}
\end{figure}
