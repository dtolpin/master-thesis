% $Id: emp-toolkit.tex,v 1.10 2009/09/05 16:33:35 dvd Exp $

\section{Algorithm Implementation}

{\sc uncertima}, a toolkit for optimization under uncertainty, has
been developed with the purpose to explore optimization approaches and
deploy efficient optimization algorithms in applications. For a
detailed description of {\sc uncertima} see Appendix~\ref{app:uncertima}. 

At the core of the toolkit is a generalized implementation of the greedy
algorithm (see Chapter~\ref{ch:greedy}).  The greedy algorithm is
parameterized by the measurement selection scheme, the dependency
model, and the optimization object.
\begin{itemize}
\item The myopic and blinkered schemes are available; other schemes,
  both based on value of information and fixed, used for burn-in,
  may be added in the future.
\item The currently implemented dependency model is a stationary
  multi-dimensional rectangular lattice. The lattice parameterized by
  prior beliefs and covariances in each direction. The work is
  underway to add other topologies and non-stationary models.
\item The algorithm can either perform measurements on an external
  object connected via a networked interface or use a built-in object
  simulator parameterized by observation distributions in each
  location. The simulator is useful for developing and comparing
  optimization algorithms.
\end{itemize}

The output of the optimization algorithm is the chosen combination of
parameters, the expected utility and the measurement budget
surplus. For example, optimization of a function of two arguments may
result in the following output:
\begin{verbatim}
solution: x=0.2 y=-2.2 => util=0.579 / surplus=0.14
\end{verbatim}
Additionally, the final beliefs and the log of all measurements are
stored for analysis. The algorithm can switch between different
schemes during a single run, and the budget is flexibly divided
between the stages.

An optimization problem is defined in a simple language specifying
parameters of each of the algorithm constituents. Here is an example
of optimizing a single-argument function $y=f(x)$ with the logarithmic
utility $util(y)=log(y)$.
\begin{verbatim}
problem example {
  space (x)                # one argument
        / 0.01*abs(@x) {   # movement cost is proportional to the distance
    x = [1, 1.5 .. 5];     # 9 locations with step 0.5 between 1 and 5
  }
  observe y {
    1=> y = 0.5 / 0.02;     # observation variance is 0.5, cost is 0.02
  }
  find max util(x) = log(y); # maximize log(y) 
  model lattice(x) {         
    f = (0, 1) / (0.5);      # prior belief N(0, 1),
  }                          # covariance between neigbors 0.5
}
\end{verbatim}
Appendix~\ref{app:uncertima} defines the language formally and
provides more examples.

\subsection {Running Times}

In the experiments that follow, the optimization algorithms are run
multiple times on randomized inputs, and the results are accumulated
and analyzed. To ensure that the experiments can be performed within a
reasonable time frame, the algorithm parameters were chosen so that a
single run takes between a few seconds for simulated random inputs
(Section~\ref{sec:emp-random}) and a few minutes for the Metrology
Machine Setup case study (Section~\ref{sec:emp-case-mms}).

The running time of the blinkered scheme depends on the measurement
budget. An implementation with linear dependency of the running time
on the budget would make the running time infeasible for cases where
the budget is sufficient for hundreds of measurements, as is the case
with Metrology Machine Setup, for example. Therefore, the
implementation of the blinkered scheme considers sequences of
measurements with lengths as powers of $2$ (as suggested in
Section~\ref{sec:thr-blinkered-estimate}). Consequently, for the
chosen combinations of algorithm parameters, the blinkered scheme run
5--10 times longer than the myopic scheme.

