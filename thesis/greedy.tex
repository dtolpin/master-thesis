% $Id: greedy.tex,v 1.45 2009/10/18 19:38:57 dvd Exp $

\section{Problem Statement}

The problem of interest is optimization under uncertainty (Section~\ref{sec:bg-opt}); the Bayesian approach (Section~\ref{sec:bg-opt-bayesian}) will be used to solve the problem. To
select observations from which to update the beliefs, an algorithm
based on rational metareasoning (section~\ref{sec:bg-limited-meta})
will be employed.

\subsection{Sensor Metaphor}

The range of problems to be solved can be described informally through
a {\em sensor metaphor}:

\vspace {\baselineskip}
\hangindent=\parindent
\hspace{\parindent}A single sensor (Figure~\ref{fig:sensor-metaphor})
can be moved in a multi-dimensional space. The sensor can make inexact
measurements in each location to which it is moved. The sensor sends
obtained observations to the control unit, and receives from the unit
instructions on further movements and measurements. At the end, the
control unit returns the coordinates of the best location according to
a certain criterion.

\begin{figure}[h]
\begin{center}
\input{greedy-sensor-metaphor.tex}
\end{center}
\caption{Sensor Metaphor}
\label{fig:sensor-metaphor}
\end{figure}

\vspace {\baselineskip}

\hangindent=0pt

\subsection{Real-World Examples}
\label{sec:greedy-examples}

Seemingly different problems can be seen as instantiations of this
metaphor. Although the nature of the measurements and movements and the
selection criteria are different, all such problems share a common
structure.

\begin{description}
 \item[Tool setup:] The same product can be produced with different
   combinations of tool parameters. The parameters may affect
   robustness of the production process. To choose a preferable
   parameter combination, batches of items are produced with several
   combinations.  Since examining every item in a batch would take too
   much time, a relatively small sample is taken from the batch, and
   the robustness is estimated from the sample.
\item[Machine learning:] The ability of a classifier to learn
  classification from training data may depend on its structural
  properties. In many cases, the only reliable way to select
  appropriate properties is to try the classifier on a few
  combinations and deduce the best values.
\item[Contamination detection:] In order to detect malicious
  introduction of contaminants, sensors are placed in multiple
  locations in a water distribution network \cite{Krause.water}. While
  the sensors are sparse and their readings are noisy, the
  contamination source must be discovered quickly and accurately.
\item[Traveling cobbler problem:] A cobbler travels in a rural area
  looking for a village for his shop. A good place would have a
  relatively dense population, many people wearing leather
  shoes, and few competitors. The time to settle is limited by the
  food the cobbler has in the sack, and the trips either wear the
  cobbler's own shoes or cost money if he takes a train or a coach.
\end{description}

\subsection{Problem Formulation}

Formally, an optimization under uncertainty problem which corresponds
to the sensor metaphor and covers a sufficiently wide range of
interesting cases can be stated as follows:

There is an unknown function $u(\textbf{x})\colon\mathbb{R}^n \to
\mathbb{R}$, the {\em utility function}. $u(\textbf {x})$ can be
evaluated in a finite number of ways $K$, with known error
distributions, for some values of the argument $\textbf
{x}$. Measurements --- evaluations of the function in a location ---
are sequential. There are functions
$c^k(\textbf{x},\textbf{x}')\colon\mathbb{R}^{2n} \to \mathbb{R}$, the
{\em cost functions}, for each type of evaluation, $k\in 1..K$, which
are always applied to subsequent argument values in the measurement
sequence $Q=\{(k_i, \textbf{x}_i)\:\vline\;i \in 1..N\}$:
$\textbf{x}=\textbf{x}_{i-1},\,\textbf{x}'=\textbf{x}_i$ for $i \in 1..N$,
and $\textbf{x}_0$ is set arbitrarily (for example, at the 
coordinate origin). The total measurement cost should not exceed the budget
$C$.

The objective is to discover a measurement selection policy that finds
a sequence of measurements
$Q^\alpha=\{(k_i^\alpha,\textbf{x}_i^\alpha)\:\vline\;i \in
1..N^\alpha\}$ and an argument $\textbf{x}_\alpha$ maximizing the
reward, or net utility, $W$:

\begin{eqnarray}
\label{eq:greedy-statement-reward}
\mbox{max:}&&W=u(\textbf{x}_\alpha)
              -\sum_{i=1}^{N^\alpha}c_i^\alpha(\textbf{x}_{i-1}^\alpha,\textbf{x}_i^\alpha)\nonumber\\
\mbox{s.t.:}&&\sum_{i=1}^{N^\alpha}c_i^\alpha(\textbf{x}_{i-1}^\alpha,\textbf{x}_i^\alpha)\le C.
\end{eqnarray}

This work focuses on {\em online measurement selection}
\cite{Rish.efficient}: every next measurement is selected after the
outcomes of all preceding measurements are known. An alternative
problem is {\em offline measurement subsequence selection}, in which
the sequence of measurements is determined in advance and executed in
full, without taking outcomes of individual measurements into account.

\subsubsection{Free measurements}

In some applications, the measurements do not affect the reward 
as long as they do not exceed the budget. In these cases,
(\ref{eq:greedy-statement-reward}) takes the following form:


\begin{eqnarray}
\label{eq:greedy-statement-reward-free-measurements}
\mbox{max:}&&W=u(\textbf{x}_\alpha)\nonumber\\
\mbox{s.t.:}&&\sum_{i=1}^{N^\alpha}c_i^\alpha(\textbf{x}_{i-1}^\alpha,\textbf{x}_i^\alpha)\le C
\end{eqnarray}

However, the general form (\ref{eq:greedy-statement-reward}) can still
be used: both the cost functions and the budget are scaled down such
that the constraint still holds but the influence of the total cost on the
reward can be neglected.

\subsection{Simplifications}

The problem in its general setting is difficult to approach. In many
cases, simplifications are possible. The simplifications help
construct efficient approximation algorithms for a wide range of
important special cases.

\subsubsection{Cost function}

The cost function is often {\em stationary}: it can be defined as a
function $c(\textbf{y})\colon\mathbb{R}^n \to \mathbb{R}$ of the
difference $\Delta \textbf{x}=\textbf{x}'-\textbf{x}$:

\begin{equation}
\label{eq:greedy-statement-stationary-cost}
c(\textbf{x},\textbf{x}')= c(\Delta \textbf{x})
\end{equation}

Further on, the cost can usually be decomposed into a movement cost
that depends only on the distance and not on the evaluation type,
and a fixed evaluation cost. There can be multiple such decompositions:

\begin{equation}
\label{eq:greedy-statement-cost-decomposition}
c(\Delta \textbf{x})=c_m(\Delta \textbf{x})+c_e
\end{equation}

In the simplest case, the movement cost can be set to zero, and any
permutation of $Q^\alpha$ has the same total cost. However, an
algorithm that discovers $Q^\alpha$ still depends on the order of
evaluations, since such an algorithm takes into account outcomes of
earlier evaluations in order to select a next one.

\subsubsection{Parameter space}

An approximate algorithm acting on a continuous domain chooses an
optimal point up to a specified accuracy. One way to deal with the
approximation is to discretize the domain, replacing the infinite
continuous set with a finite set of points $S=\{s_i\:\vline\;i\in
1..M\}$, each lying in a vertex of a rectangular grid.

The number of measurements to choose from at each step is thus finite
and equal to $K\cdot M$. While the number of different measurement
sequences is still exponential in the sequence length, there are only
polynomially many choices at each decision point.

\subsubsection{Uncertainty model}

There are two interrelated kinds of uncertainty in the problem. 

\begin{description}
\item[Utility uncertainty:] The measured utility values are inexact;
  the function to optimize can be learned up to an error margin for any
  given evaluation cost.

\item[Parameter uncertainty:] There is an uncertainty about parameter
  robustness, dependence of the utility on a deviation of the
  parameters from the chosen values.
\end{description}

While the utility uncertainty can be represented directly, the utility
distribution may be of a general form, and belief updating becomes
complicated. There is a popular approach that simplifies the analysis:
the utility function is represented as a known function of {\em
features}, $u(\textbf{z})\colon\mathbb{R}^m \to \mathbb{R}$, and the features
themselves are functions of the optimization parameters. The features are
known inexactly, but their inexactness is modeled by `convenient',
usually normal, distributions. Evaluations return values of the features
with a known centered normally distributed error, an evaluation is a function
$e(\textbf{x}) \to \textbf{z}\colon\mathbb{R}^n \to \mathbb{R}^m$.

The parameter uncertainty can be dually thought of as a probabilistic
dependency among features in different locations. In the discrete
optimization domain, features in different vertices of the grid are
correlated, and the correlation decreases with the distance.
Frequently, the correlation is stationary, it depends only on the
distance, and not on the parameter values.

Consequently, the uncertainty is represented as a belief about an
unknown multivariate normal distribution of features, correlated
between grid vertices and among different features in the same
vertex. The utility is a known function of the features in the
vertex. Measurements---evaluations of the features in a
vertex---update the belief about the features.


\subsection{Simplified Problem}
\label{sec:greedy-simplified-problem}

The simplified problem can be re-stated as follows:

Given:
\begin{itemize}
\item a set of items $S=\{s_i\:\vline\;i\in 1..M\}$, each
  characterized by parameters $\textbf{x}_i$, exactly known, and 
  features $\textbf{z}_i$;
\item an initial belief about the features $P(\{\textbf{z}_i\:\vline\;i\in
  1..M\})$;
\item a movement cost function $c_m(\Delta
  \textbf{x})$;\footnote{In the
    case when the only item parameter is the item index and there is
    no order imposed on the items, $c_m$ has two different values: when the
    subsequent measurements are of the same item or of two different ones.}
\item a set of evaluations $E=\{(e, c_e, p_e)_k\:\vline\; k \in
  1..K\}$, where $e$ is a function from parameters to features, with
  potentially different evaluation cost $c_e$ and error distribution
  $p_e$ for each evaluation;
\item a real-valued utility function $u(\textbf{z})\colon\mathbb{R}^m\to
  \mathbb{R}$ on features;
\item a measurement budget $C$;
\end{itemize}
discover a measurement policy that finds a sequence of measurements
$Q^\alpha=\{(k_i^\alpha,\textbf{x}_i^\alpha)\:\vline\;i \in
1..N^\alpha\}$ and an item $s_\alpha$ which maximize the
reward $W$:

\begin{eqnarray}
\label{eq:greedy-simplified-reward}
\mbox{max:}&&W=u(\textbf{z}_\alpha)
              -\sum_{i=1}^{N^\alpha}\left(c_{ei}^\alpha
                +c_m\left(\textbf{x}_i^\alpha-\textbf{x}_{i-1}^\alpha\right)\right)\nonumber\\
\mbox{s.t.:}&&\sum_{i=1}^{N^\alpha}\left(c_{ei}^\alpha
                +c_m\left(\textbf{x}_i^\alpha-\textbf{x}_{i-1}^\alpha\right)\right)\le C
\end{eqnarray}

Outcomes of earlier measurements are known by the time a next
measurement is chosen. As before, $\textbf{x}_0$ can be set
arbitrarily. The case when measurements are free as long as the total
measurement cost does not exceed the budget is approximated by
scaling down both the measurement costs and the budget so that the influence of
the total cost on the reward can be neglected.

\section{The Metareasoning Approach}

An algorithm that solves the optimization problem must answer the following
questions:

\begin{itemize}
\item[---] How is a measurement selected?
\item[---] When does the algorithm terminate?
\item[---] How is a measurement outcome incorporated into the model?
\item[---] Upon termination of the algorithm, which combination of
  parameters is the best?
\end{itemize}

In the framework of the rational metareasoning approach
(Section~\ref{sec:bg-limited-meta}), a measurement corresponds to a
meta-level action, and the final selection is a base-level
action. Thus, a measurement with the highest net value of information
is selected, and a combination of parameters corresponding to an item
with the highest expected utility, according to the simplified problem
formulation (Section~\ref{sec:greedy-simplified-problem}), is returned
as the best one. The algorithm terminates when no measurement has
positive net value.

For belief updating, the Bayesian approach is adopted
(Section~\ref{sec:bg-opt-bayesian}). Initially, the joint feature
distribution is initialized to a {\em prior belief}, and then new
evidence is incorporated into the belief through Bayesian inference
(Section~\ref{sec:bg-depmod-inference}).

It remains to define the way the expected utility is computed and the
value of information of a measurement is estimated.

The expected utility is computed according to
(\ref{eq:bg-limited-eu}), with integration instead of summation for
the continuous case:
\begin{equation}
\IE[U_i]=\int\!\!\!\int\limits_{\mathbb{R}^m}\!\! u(\textbf{z}) p_i(\textbf{z})d\textbf{z}
\label{eq:greedy-meta-eu}
\end{equation}
where $p_i$ is the current belief about feature values of item
$s_i$.

The net value of information $V$ is the difference between the
intrinsic value, $\Lambda$, and the measurement cost. There are
$K\cdot M$ different measurements at each step, each determined by the
evaluation type $k$ and the measured item $s_m$. Assuming that the
measurements are enumerated arbitrarily, the value of the $j$th
measurement is:
\begin{equation}
V_j=\Lambda_j-c_m(\textbf{x}_{j}-\textbf{x})-c_{ej}
\label{eq:greedy-meta-v}
\end{equation}
where $\textbf{x}$ is the parameter vector of the last measured item.
Under the simplifying assumptions
(Section~\ref{sec:bg-limited-simplifying}), the intrinsic value of
information is estimated myopically: as the expected immediate effect
of a single measurement. Following (\ref{eq:bg-limited-benefit}),
the intrinsic value of information is determined by the following equation:
\begin{equation}
\Lambda_j = \IE(\IE[U_{\alpha_j}]-\IE[U_\alpha])
          =\int\!\!\!\int\limits_{\mathbb{R}^{2m}}(\IE[U_{\alpha_j}]-\IE[U_\alpha])
                p_{ej}(\textbf{z}_e-\textbf{z})p_j(\textbf{z}) 
                d\textbf{z}_ed\textbf{z} 
\label{eq:greedy-meta-benefit}
\end{equation}
where $s_{\alpha}$ is the item with the highest expected utility
before the measurement, $s_{\alpha_j}$ --- after the $j$th
measurement, and the expected utilities are computed for the updated
beliefs.

\section{Algorithm Description}

The pseudocode for the algorithm is provided in Listing~\ref{alg:greedy-algorithm}.

\begin{algorithm}
\caption{Greedy Myopic Algorithm}
\label{alg:greedy-algorithm}
\begin{algorithmic}[1]
\STATE $budget \leftarrow C$
\STATE initialize beliefs
\LOOP
  \FORALL {items $s_i$}
    \STATE compute $\IE(U_i)$
  \ENDFOR
  \FORALL {measurements $m_j$}
    \IF {$c_j \le budget$}
      \STATE compute $V_j$
    \ELSE
      \STATE $V_j \leftarrow 0$
    \ENDIF
  \ENDFOR
  \STATE $j_{max} \leftarrow \arg \max\limits_j V_j$
  \IF {$V_{j_{max}}>0$}
    \STATE perform measurement $m_{j_{max}}$
    \STATE update beliefs
    \STATE $budget \leftarrow budget-c_{j_{max}}$
  \ELSE
    \STATE {\bf break}
  \ENDIF
\ENDLOOP
\STATE $\alpha \leftarrow \arg \max \IE(U_i)$
\RETURN $x_\alpha$
\end{algorithmic}
\end{algorithm}

The algorithm maintains a persistent data structure which holds
beliefs about feature values of the items. The beliefs are initialized
to the prior beliefs (line 2), and then updated according to
measurement outcomes (line 17). The main loop (lines 3--22) continues
as long as there are measurements that fit within the budget (line 8)
with positive value (line 15). Otherwise, the loop terminates (line
20), and the algorithm returns the parameter vector of an item with
the maximum expected utility (lines 23--24).

The remaining budget is held in variable $budget$, which is initialized
to the total budget $C$, and decreased by the cost of each performed
measurement. Thus, the algorithm is guaranteed to terminate when the costs
of all measurements are strictly positive. Otherwise, if there is a
free measurement, the algorithm may run for an arbitrarily long time,
as long as value of information of the measurement is positive.

\section{Theoretical Bounds}

Active test selection is $\mathrm{NP}^{\mathrm{PP}}$-hard
\cite{Guestrin.optimal}, thus the need for approximation algorithms
arises. The greedy myopic algorithm is one feasible approach; however,
in the general case the algorithm can behave arbitrarily badly.  For
example, if at least two measurements are required to achieve a positive
value, the algorithm will terminate without making any measurements.

On the other hand, if the myopic value of information estimate of a
measurement is submodular, the algorithm is nearly optimal
\cite{Guestrin.submodular}. Let $S$ be a finite set, and $f\colon 2^S
\to \mathbb{R}$. $f$ is submodular if for $A \subset B \subset S$ and
$x \in S$,
\begin{equation}
f(A \cup \{x\})-f(A) \ge f(B\cup \{x\})-f(B)
\label{eq:greedy-submodular}
\end{equation} 
In other words, a submodular function exhibits {\em diminishing
  returns}: the effect of adding a new element to a set is greater
when the set is smaller.

Examples of submodular functions include \cite{Iwata.submodular}:
\begin{itemize}
\item cut capacity --- maximum flow through a cut boundary in a graph;
\item entropy and information gain --- influence of evidence in
  Bayesian models;
\item matroid rank --- the cardinality of a maximal subset in the matroid.
\end{itemize}

An example of a nonsubmodular increasing function on a set is the sigmoid
function of the set size:
\[
f(S)=\frac 1 {1+e^{4-|S|}}
\]
The values of the function for set sizes $1,\,2,\,\,3$ are
$\approx0.0474,\,0.1192,\,0.2689$. That is, adding an element to a
single-element set has lesser effect than to a two-element set:
\[
f(\{a,b,c\})-f(\{b,c\})\approx0.150>f(\{a,b\})-f(\{b\})\approx0.072
\]
and thus $f$ is nonsubmodular.

The myopic value of information estimate in the optimization problem
can be shown to approach submodularity in some cases, for example,
when the evaluations are exact and the utility function is
concave. Mostly, however, the estimate is nonsubmodular and no
non-trivial performance guarantees are provided.
