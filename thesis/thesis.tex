% $Id: thesis.tex,v 1.60 2010/04/13 11:12:30 dvd Exp $

\documentclass[twoside]{report}

\usepackage{algorithm,algorithmic}
\usepackage{url}
\usepackage{setspace,geometry}
\usepackage{framed}
\usepackage{amsfonts,amsmath,amsthm}
\usepackage{graphicx}

\usepackage{tikz}
\usetikzlibrary{positioning}
\usetikzlibrary{shapes.geometric}
\usetikzlibrary{shapes.symbols}
\usetikzlibrary{shadows}
\usetikzlibrary{arrows}

\geometry{bindingoffset=0.4in,margin=1.2in}
%\onehalfspacing
\doublespacing

\input{defs}

\title {Optimization under Uncertainty using Limited Rationality}

\author {David Tolpin}

\begin{document}
\pagestyle{headings}
%\maketitle

\begin{titlepage}
\begin{center}
{\large Ben-Gurion University of the Negev
\\
Department of Computer Science}
\vspace{1in}

{\bf\LARGE Optimization under Uncertainty using Limited Rationality}
\vspace{1in}

{\large Thesis for a Master Degree in the
\\
Department of Computer Science
\\
Faculty of Natural Science
\\
Ben Gurion University of the Negev

\vspace{1in}

by David Tolpin
\\
Under the supervision of Professor Solomon Eyal Shimony
\\
\vfill

\today}
\end{center}
\end{titlepage}

\setcounter{page}{1}
\pagenumbering{roman}

\input{abstract}

\tableofcontents
\listoffigures
\listoftables

\newpage
\setcounter{page}{1}
\pagenumbering{arabic}

\chapter{Introduction}
\label{ch:intro}
\input{intro}

\chapter{Background}
\label{ch:bg}

This chapter provides the necessary background knowledge. It is
comprised of four sections. Section~\ref{sec:bg-opt} describes the
state-of-the-art and the development directions in {\em optimization
  under uncertainty}, lays the ground for the positioning of the
proposed method among the existing ones and assists in comparing the
new method with the rest. Section~\ref{sec:bg-limited} explains the
notion of {\em limited rationality}, the approach at the core of the
proposed optimization method. Section~\ref{sec:bg-depmod} discusses
{\em graphical dependency models}, a popular formalism for
representing probabilistic dependencies. The models allow to compactly
describe a multivariate distribution, an important component of
optimization under uncertainty. A survey of ongoing work in
related research areas (Section~\ref{sec:bg-related}) concludes the
chapter.

\input{bg-opt}
\clearpage
\input{bg-limited}
\clearpage
\input{bg-depmod}
\clearpage
\input{bg-related}

\chapter{Greedy Myopic Optimization}
\label{ch:greedy}

This chapter reformulates the existing greedy myopic approach
to optimization under uncertainty. The approach implements the Bayesian
approach to optimization under uncertainty, and relies on the
principles of limited rationality to achieve efficiency. Applicability
criteria are examined, and certain known theoretical bounds are
presented.

\input{greedy}

\chapter{Semi-Myopic Optimization}
\label{ch:thr}

The myopic scheme presented in chapter~\ref{ch:greedy} is shown
to have serious shortcomings when applied to optimization under
uncertainty with inexact observations. In this chapter, an improved scheme
which overcomes the shortcomings is proposed. A qualitative analysis
of the scheme is provided, and theoretical bounds for two important
extreme cases are proved. Additional improvements ensure that the
computations at the meta-level are efficient.

\input{thr-blinkered-estimate}
\input{thr-computation-reuse}

\chapter {Empirical Evaluation}
\label{ch:emp}

An algorithm implementing the blinkered scheme is evaluated
empirically. The algorithm is both applied to artificial data and used
in real-world case studies. The blinkered and the myopic scheme are
compared, and causes of performance differences are analysed.

\input{emp-toolkit}
\input{emp-random}
\input{emp-functions}
\input{emp-case-svm}
\input{emp-case-mms}

\chapter {Summary}
\label{ch:conclusion}

Based on both the theoretical results (Chapter~\ref{ch:thr}) and the
empirical evaluation (Chapter~\ref{ch:emp}), the chapter provides
recommendations for the use of each of the algorithms. Contributions
of the work are described. Possible future research directions are
enumerated with their expected impact on further improvements in the
algorithm.

\input{conclusion}

\appendix

\chapter {{\sc uncertima:} Toolkit for Optimization under Uncertainty}
\label{app:uncertima}

The toolkit for optimization under uncertainty is presented. Its
capabilities and features are introduced. The problem definition
language, the input and output data formats, and the command-line
interface to the core tool are described and illustrated by examples.

\input{app-uncertima.tex}

\bibliographystyle{alpha}
\bibliography{refs}

\end{document}
