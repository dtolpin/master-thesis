% $Id: app-uncertima.tex,v 1.21 2009/09/05 16:33:34 dvd Exp $

\section{Objectives and Capabilities of the Toolkit}

The {\sc uncertima} Toolkit for Optimization under Uncertainty is an
ongoing project. The objective of the project is to provide designers
of optimization algorithms with a convenient and flexible environment
for trying new ideas, implementing, testing and comparing new
optimization algorithms, and packaging the algorithms for industrial
deployment.

The toolkit is composed of several components with well-defined
interfaces based on text files. Different tasks are accomplished
through interaction of varying groups of components. Most of the
components can be run independently as stand-alone programs or
packaged as a linkable library for inclusion into third-party
software.

The core algorithm libraries are written in the Objective Caml
programming language (\url{http://caml.inria.fr/})
\cite{Challoux.ocaml}. Programs compiled by the Objective Caml
optimizing compiler are fast and compact, achieving performance
comparable to fine-tuned C/C++ code \cite{Harrop.ocaml}. Still, the
code can be deployed in modern managed environments such as Common
Language Runtime and Java Virtual Machine: Microsoft develops and
distributes a programming environment for its F\# programming language
(\url{http://msdn.microsoft.com/fsharp/}), an extension of Objective
Caml; and there is a version of the original Objective Caml compiler
that produces Java Virtual Machine byte codes
(\url{http://ocamljava.x9c.fr/}).

While the basic components are working and stable, there is a lot of
work to be done in the core functionality as well as in improvement of
monitoring, graphical analysis and experiment management tools. The
toolkit is being actively developed based both on the original vision
and on the experience gathered from its use in the current research.

\section{Toolkit Components}

A graphical overview of the structure of the toolkit is presented in
Figure~\ref{fig:app-toolkit-components}. In the figure, rectangles
correspond to components, cylinders to libraries, and tapes to
inputs. Arrows denote directions of data flow.
\begin{figure}[h]
\begin{center}
\input{app-toolkit-components.tex}
\end{center}
\caption{Toolkit components}
\label{fig:app-toolkit-components} 
\end{figure}
At the core of the toolkit is the {\em solver}, a component that
solves an optimization problem. The computation is monitored through
the {\em monitor}, a graphical tool that displays the algorithm
progress in a user-friendly way. Intermediate and final results are
recorded in logs and can be analyzed using the {\em analysis
  tools}. In the course of development of a new algorithm, the
algorithm performance is assessed from experiments involving multiple
runs of the algorithm and statistical processing of the results. The
{\em batch runner} performs such experiments, invoking the solver
multiple times and post-processing the outputs. The logs and results
are stored in simple tabular formats, and can be passed to third-party
tools for further processing and analysis.

Figure~\ref{fig:app-solver} depicts the structure and the environment
of the solver. Rectangles correspond to program modules, tapes with
lower bent edge to inputs, and tapes with upper bent edge to
output. Arrows denote directions of data flow.
\begin{figure}[h]
\begin{center}
\input{app-solver.tex}
\end{center}
\caption{Solver}
\label{fig:app-solver} 
\end{figure}
The three main components are the {\em optimization algorithm}, the
{\em object abstraction} and the {\em belief model}. The object
abstraction encapsulates either a real object, or the object
simulator; object simulation is useful for testing new algorithms
during development. The solver reads the problem definition, the model
description and the simulation data (in the simulation mode), runs the
algorithm, and records the algorithm progress and the results. The
algorithm calls the object abstraction to evaluate values of features
for a particular combination of parameters, and relies on the belief
model to maintain the beliefs.

The output files are analyzed using the monitor and the analysis
tools. A graphical display of the final expected utilities for a
two-dimensional optimization problem is shown in Figure~\ref{fig:app-analysis-mockup}.
\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.8,trim=100pt 25pt 100pt
  0pt]{analysis-mockup.pdf}
\end{center}
\caption{Analysis of final expected utilities.}
\label{fig:app-analysis-mockup}
\end{figure}
 In the figure, the black
cross on the contour graph (labeled $\alpha$) marks the best
combination of parameters; the auxiliary plots display dependency of
the expected utility on each of the parameters along the dashed lines,
which pass through the point (marked by the square) chosen by the user.
Expressive and flexible graphical facilities proved to be powerful
in development of optimization algorithms. Work is underway on
design and implementation of extended graphical capabilities of the
tools for algorithm monitoring and result analysis.

The problem definition language, the input and the output data formats
are described in the following sections
(\ref{sec:app-pdl}--\ref{sec:app-beliefs-format}). The solver can be
run in a number of ways, the simplest one is to invoke the component
from the command line; this way also allows to call the solver from
shell scripts, adding pre-processing of data and post-processing of
the results into the pipeline. The command-line interface of the
Solver is described in the last section of this chapter (Section~\ref{sec:app-solver-cli}).

\section{Problem Definition Language}
\label{sec:app-pdl}

An optimization problem is defined by  
\begin{itemize}
\item a parameter space;
\item an utility as a function of features corresponding to a combination
  of optimization parameters;
\item available evaluations (measurements);
\item costs of the evaluations and sensor movements.
\end{itemize}
In addition, to direct the search, the problem is augmented by 
\begin{itemize}
\item a dependency model in the parameter space.
\end{itemize}


The utility function depends on features in the parameter space, and
is specified as operations on both 
\begin{itemize}
\item \textit{scalar values} at given parameter combinations, and
\item \textit{vectors} (element-wise and aggregate operations---mean, sd,
  max, min).
\end{itemize}
The dependency model is used to propagate beliefs about features in
the parameter space.

\subsection {Problem Definition Example}

The following sample problem definition illustrates the definition
syntax:

\begin{verbatim}
problem sample {
  space (x, y, z, t)            # four parameters
        / 0.005*@x+0.02*step(@y)+0.04*t { # linear in x, fixed in y,
                                          # free in z, and the time ticks { 
    # each one is specified by range of values and by change const 
    x = [0.0, 10.0 .. 100.0];
    y = [0.0, 5.0 .. 100.0]; 
    z = [0, 1];
    t = [1..20]; 
  }
  observe (a, b) {               # two observable features in every point
    # two evaluations
    1 => (a,b) = (10.0, 10.0) / 0.01; # both features, imprecise, cheap 
    2 => a = 2.0 / 0.1;               # only a, accurate, expensive
  }
  find max w(x,y) = log(u)+log(v) { # optimize x,y for maximum of w
    u = mean(a[z=1]-a[z=0]);
    v = sd(b); 
  }
  model lattice(x,y,t)  { # the dependency model is lattice
    # with dependencies on x, y, and t
    a = (0.0, 10.0) / (5, 5, 5);
    b = (0.0, 10.0) / (10, 10, 2);
  }
  # another options would be 
  # model bif "sample-model.bif";
  # model gdl "sample-model.gdl";
}
\end{verbatim}

\subsection{Problem Definition Syntax}

In the following productions, non-terminal references are in \textit{italic},
terminals are in \textbf{bold}. Each group of productions is followed by
an informal explanation.

\noindent
\texttt{\\
problem ::= \textbf{problem} \textit{name} \textbf{\{} \textit{space
  bserve find model} \textbf{\}} .}

\hangindent=\parindent
A problem has a name for ease of reference, and contains
descriptions of the parameter space, the observable features, the optimization
function, and the dependency model in the parameter space.

\noindent
\texttt{\\
space ::= \textbf{space} \textit{nametuple}   [ \textbf{/} \textit{cost} ] \textbf{\{} \textit{parameter}+ \textbf{\}} .\\
cost ::= \textit{expression} .}

\hangindent=\parindent
The space section header lists space parameters. The section body defines the
range of values for each parameter, and the costs of sensor
movements. A sensor movement cost can depend on the parameter difference
(\textbf{@} followed by parameter name) and on location in
the parameter space.

\noindent
\texttt{\\
parameter ::= \textit{name} \textbf{=} \textit{range} \textbf{;} .\\
range ::=  number | \textbf{[} \textit{first} [\textbf{,} \textit{next} ] \textbf{..} \textit{last}  \textbf{]} | \textbf{[} \textit{first} {\textbf{,} \textit{next}} \textbf{]} .\\
first, next, last ::= \textit{number} .\\
\\
observe ::= \textbf{observe} \textit{nametuple} \textbf{\{} \textit{evaluation}+ \textbf{\}} .}

\hangindent=\parindent
The `observe' section header lists observable features. The section
body defines available evaluations (measurements).

\noindent
\texttt{\\
evaluation ::= \textit{number} \textbf{=>} \textit{nametuple} \textbf{=} \textit{numbertuple} \textbf{/} \textit{cost} \textbf{;} .}

\hangindent=\parindent
Every kind of evaluation is marked by its numerical index and
specified by the evaluated features, the accuracy and the cost of the
evaluation (sensor model). The index is used to drive the object
controller or simulator, and is recorded in the log.

\noindent
\texttt{\\
find ::= \textbf{find} (\textbf{max\textbf{|}min}) name nametuple = \textit{expression} (\textbf{;} | \textbf{{} \textit{binding}* \textbf{}} ) .\\
binding ::= \textit{name} \textbf{=} \textit{expression} \textbf{;} .}

\hangindent=\parindent
The optimization criterion finds best values for parameters listed
in the square brackets according the optimization function.

\noindent
\texttt{\\
model ::= \textbf{model} ( \textit{indepmodel} | \textit{latticemodel} | \textit{bifmodel} ) .\\
bifmodel ::= \textbf{bif} \textit{filename} \textbf{;} .}

\hangindent=\parindent
There are several ways to specify a model. Already implemented ones
are the independent model and the rectangular lattice, other model
names reference an external description in on of the the supported formats.

\noindent
\texttt{\\
indepmodel ::= \textbf{indep\textbf{ }{} \textit{prior}* \textbf{}} .}

\hangindent=\parindent
 The prior belief is defined by its mean and variance.

\noindent
\texttt{prior ::= \textit{name} \textbf{=} \textit{prior-distribution} \textbf{;} .}

\noindent
\texttt{\\
latticemodel  ::= \textbf{lattice} \textit{nametuple} \textbf{{} \textit{dependency}* \textbf{}} .}

\hangindent=\parindent
In a rectangular lattice, neighbor locations with a single differing
parameter are probabilistically dependent.

\noindent
\texttt{\\
dependency ::= \textit{name}  \textbf{=} \textit{prior-distribution} \textbf{/} \textit{noise} \textbf{;} .\\
prior-distribution ::= \textit{number-tuple} .\\
noise ::= \textit{number-tuple} .}

\hangindent=\parindent
Each dependency is specified by the feature to propagate, the prior
evidence in a node and the noise variances in each direction.

\noindent
\texttt{\\
nametuple ::= \textit{name} | \textbf{(} \textit{names} \textbf{)} .\\
numbertuple ::= \textit{number} | \textbf{(} \textit{numbers} \textbf{)} .\\
names ::= \textit{name} (\textbf{,} \textit{name})* .\\
number ::= \textit{number} (\textbf{,} \textit{number})* .\\
filename ::= \textit{quoted-string} .}

\vspace{1em}
Several examples of problem definitions of varying complexity can be
found in Chapter~\ref{ch:emp}.

\section{Input and Output Formats}

Data and results are represented as space separated tabular data,
suitable for easy reading in both general-purpose programming
languages and environments and dedicated packages for graphing and
statistical processing. The data files are interrelated, but designed
to be self-contained for ease of processing.

The first row of a table is column names. The second row is numeric
codes of column contents, defined for every table type.

\subsubsection{Example}

\begin{framed}
\begin{verbatim}
HEIGHT WIDTH WEIGHT ACTION XMU XSIGMA YMU YSIGMA
0       0      0      1     2    2     2    2
10     3.5    1.0     0   0.0   Inf   0.0  0.0
10     4.5    2.0     1   1.0    4    0.0  Inf
\end{verbatim}
\end{framed}

Lines starting with \texttt{\#} are inserted into the output at the
beginning of each algorithmic stage (more than one algorithm can be run
sequentially on the model). \texttt{\#} is followed by the algorithm
name.

\subsubsection{Example}

\begin{framed}
\begin{verbatim}
LAST  C gamma OBS C gamma util
0 1 1 2 3 3 4 
# planhead myopic
1 -8. 12. 1 -8. -12.  -0.67571188545
1 -8. -12.  1 8.  12. -0.722110894823
1 8.  12. 1 6.  -12.  -0.748073786086
1 6.  -12.  1 8.  3.  -0.785676155845
# planhead blinkered
1 0.  -12.  1 8.  0.  -0.756497744352
1 8.  0.  1 8.  -9. -0.826545159607
1 -8. 3.  1 8.  -9. -0.826545159607
1 -2. -6. 1 -6. -12.  -0.86674039588
1 2.  -6. 1 8.  -12.  -0.86319290184
1 8.  -12.  1 -8. 6.  -0.879094023009
\end{verbatim}
\end{framed}


\subsection{Simulated Observations and Initial Hypothesis}

The simulated observations and the initial hypothesis are
space-separated tables of observation parameters followed by means and
(optionally) variances of all observable features. When the variance
is missing for a particular feature, the variance from the problem
definition is used. The object simulator randomly samples measurement
outcomes from the normal distribution with the given mean and
variance.

\begin{verbatim}
simulation-row ::= parameter+ feature+ .
parameter ::= number .
feature ::= mean variance?  .
mean ::= number .
variance ::= number .
\end{verbatim}

The column type codes are:

\begin{description}
\item[0] parameter
\item[1] feature mean
\item[2] feature variance
\end{description}

\subsubsection{Example}

\begin{framed}
\begin{verbatim}
C         GAMMA          ACC             ACC
0            0            1                2  
0.0        0.0           0.3             0.05
2.0        0.0           0.5             0.07
\end{verbatim}
\end{framed}


\subsection{Measurement Log}

The log is a table in which the sequence of measurements is stored.
The log's role is to track the algorithms walk through the observation
actions. The important bits of information are
\begin{itemize}
\item what is measured and where;
\item what is the  best combination of optimization parameters after the measurement;
\item when does the algorithm (deliberation can result in a
  series of measurements, not just one).
\end{itemize}

A simple way to denote a series is to mark the last measurement in
each series. If the algorithm deliberates after each measurement,
every series will consist of a single measurement, and all
measurements will be marked as last.

\subsubsection{Format}

A table row is the `last' flag, followed the measurement parameters,
followed by the evaluation kind (denoted by a small integer, the
evaluation index), followed by the currently best optimization
parameters. If the measurement is the last one in a series, the last
flag is 1, otherwise 0. If the measurement is not the last one in a
series, the currently best parameters are not updated.

\begin{verbatim}
log-row ::= last measurement-parameter+ evaluation-kind 
            optimization-parameter+ utility .
last ::= 0|1 .
\end{verbatim}

\subsubsection{Column Types}

\begin{description}
\item[0] last measurement flag
\item[1] measurement parameter
\item[2] evaluation kind
\item[3] optimization parameter
\item[4] expected utility
\end{description}

\subsubsection{Example}

\begin{framed}
\begin{verbatim}
LAST COLOR FOCUS SITE ORIENTATION EVALUATION COLOR FOCUS   MERIT
  0    1     1    1       1             2       3      3      4
# local-search
  1    2     2    0       1             0       1      1     2.3
  0    1     2    0       0             0       1      1     2.3
  1    1     3    1       0             1       1      2     1.8
\end{verbatim}
\end{framed}

In the example, the second measurement does not complete a series, and
the best parameter combination is not updated. The third measurement
updates the best parameters and will be followed by
deliberation. After the deliberation, the algorithm will either stop
or choose a new measurement series.

\subsection{Expected Utilities}

Solving an optimization problem is accompanied by computing the
expected utility for every combination of optimization parameters. The
utility is often expressed as a function of several named
subexpressions (see, for example, the problem definition in Section~\ref{sec:emp-case-mms}).  The expected values of the subexpressions
are helpful in tuning and assessing the utility function.

The expected utility and named subexpressions for every combination of
optimization parameters are recorded at the end of each algorithmic
stage.

\subsubsection{Format}

A row is a cobmination of optimization parameters, followed by the
expected utility and expected values of named subexpressions.

\begin{verbatim}
results-row ::= parameter+ utility components* .
\end{verbatim}

\subsubsection{Column Types}

\begin{description}
\item[0] optimization parameter
\item[1] named utility subexpression
\item[2] expected utility
\end{description}

\subsubsection{Example}

\begin{framed}
\begin{verbatim}
 COLOR FOCUS TIS_X TIS_Y UTILITY
   0     0     1    1     2  
# planhead myopic
   1     1   0.001 0.003  0.7 
   1     2   0.002 0.002  0.7 
   1     3   0.002 0.001  0.8 
   2     1   0.0   0.004  0.7 
   2     2   0.001 0.001  0.9 
   2     3   0.003 0.004  0.6 
   3     3   0.004 0.004  0.5  
\end{verbatim}
\end{framed}

In the example above, the best combination is (2,2). 

\subsection{Beliefs}
\label{sec:app-beliefs-format}

The solver maintains beliefs about every feature for every combination
of parameters. The beliefs are normally distributed and described by
their mean and variance. They are recorded at the end of each
algorithmic stage.

\subsubsection{Format}

A row is a combination of parameters, followed by belief means and variances.

\begin{verbatim}
belief-row ::= parameter+ (feature-mean feature-variance)+ .
\end{verbatim}

\subsubsection{Column Types}

\begin{description}
\item[0] parameter
\item[1] feature mean
\item[2] feature variance
\end{description}

\subsubsection{Example}

\begin{framed}
\begin{verbatim}
COLOR FOCUS SITE ORIENTATION X_MEAN X_VARIANCE Y_MEAN Y_VARIANCE
 0      0    0       0         1       2         1       2
# planhead blinkered
 1      1    1       0        0.01    0.0001    0.01    0.00005
 1      1    2       0        0.02    0.0001    0.005   0.002
\end{verbatim}
\end{framed}

\section{Command Line Interface to Solver}
\label{sec:app-solver-cli}

\subsection{Synopsis}
\begin{quote}
\texttt{\mbox{{\bf solve} {\bf[}OPTIONS{\bf]} {\em problem} {\em object} [{\em
        hypothesis}]}
\\
\mbox{{\bf solvex} {\bf[}OPTIONS{\bf]} {\em problem} {\em object} [{\em
        hypothesis}]}}
\end{quote}

\subsection{Description}

The \textit{solve} utility solves a problem of optimization under
uncertainty, taking a problem definition file and an object
identifier and maximizing the expected utility of the found
solution. \textit{solvex} is the same utility compiled with aggressive
optimization options and turned off integrity checks.

Optionally, \textit{solve} can prime its beliefs with a
hypothesis. The hypothesis data are specified as the third, optional
parameter. The file format is the same as for the object simulator.

A sequence of algorithms, each using up a fraction of the total budget
allocated according to the sub-budget function, can be specified.  The
algorithms, the parameters, the total budget, and the sub-budget
functions are defined using command-line options. The object is either
controlled via XML-RPC, or simulated based on data read from the file.

\textit{solve} prints the problem synopsis and the solution to the
standard output, in the following format:

\begin{quote}
\texttt{problem: \textit{problem}\\
object: \textit{object}\\
log: \textit{log prefix}\\
hypothesis: \textit{hypothesis}\\
solution: {\textit{parameter-name}=\textit{value}} =>
\textit{utility-name}=\textit{value} / surplus=\textit{value}}
\end{quote}
and stores information about actions and beliefs in the log files.
Additionally, the utility may print warning, error messages, and
debugging information to the error output.

\subsection{Options}

The command-line options control the algorithms to be used, budget allocation
for each of the algorithms, the computation precision, and the
logging.

\subsubsection{General Options}
\begin{description}
\item[-help, --help]
Display the help message and exit. The help message is also displayed
if there are errors in the command-line syntax.
\end{description}
\subsubsection{Algorithms and Budgets}
\begin{description}
\item[-a {\em algorithm}]
The algorithm. The currently implemented algorithms are {\bf myopic} (default) and 
{\bf blinkered}. Each algorithm uses up at most the currently specified sub-budget.
\item[{\bf -B {\em budget}}]
The total budget. The program stops when either a solution is found or the
budget is exhausted. By default, the budget is infinite.
\item[{\bf -b {\em sub-budget}}]
The sub-budget for the current algorithm. The interpretation of the
number depends on the sub-budget function used. No default.
\item[{\bf -f {\em function}}]
The sub-budget function. The supported functions are 
\begin{description}
\item [ {\bf greedy}]
Use up all of the budget left. The sub-budget value is ignored. This
is the default function.
\item [ {\bf fixed}]
Use at most the sub-budget.
\item [ {\bf surplus}]
Leave at least the sub-budget for the rest of the algorithms.
\item [ {\bf fraction}]
Use at most the fraction of the total budget.
\end{description}
\item[{\bf -s}]
Run in the simulation mode. The object is the simulation data file.
\item[{\bf -r {\em limit}}]
The maximum number of recurring measurements in the simulation mode; ignored
if {\bf -s} is not specified.
\end{description}
\subsubsection{Computation Precision}
Reasonable defaults are used for the options in this section.
\begin{description}
\item[{\bf -i {\em number}} ]
Number of integration points in Monte-Carlo integration.
\item[{\bf -j {\em number}}]
Number of belief propagation passes in the lattice dependency model.
\item[{\bf -p {\em precision}}]
Monte-Carlo integration precision.
\item[{\bf -q {\em precision}}]
Belief update precision in the lattice dependency model.
\end{description}
\subsubsection{Logging and Debugging}
\begin{description}
\item[{\bf -l {\em prefix}}]
The log file prefix. Three log files are created for each invocation:
\item [ {\bf {\em prefix}.obs}]
The observation log.
\item [ {\bf {\em prefix}.bel}]
The beliefs in each observation location at the end of each
algorithm.
\item [ {\bf {\em prefix}.opn}]
The utilties and their components (`opinions') in each optimization
location at the end of each algorithm.
\item[{\bf -d}]
Print debugging information.
\end{description}

\subsection{Examples}
\begin{verbatim}
solve -s svm.p svm-ft.s
\end{verbatim}

Solve the {\em svm} problem using simulated measurement outcomes
specified in {\em svm-ft.s}.

\begin{verbatim}
solve -f fraction -b 0.3 -a myopic -f greedy -a blinkered -B 10 \
           telescope.p http://example.com/telescope
\end{verbatim}

Solve the {\em telescope} problem, controlling the object via the
XML-RPC interface at \url{http://example.com/telescope}. Spend no more
than 10 units of time. Run the myopic algorithm for at most 30% of the
total budget, and the blinkered algorithm for the rest of the time.


