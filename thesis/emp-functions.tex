% $Id: emp-functions.tex,v 1.17 2009/09/05 16:33:35 dvd Exp $


\section {Optimization Benchmarks}

A popular way to compare optimization algorithms is to apply them to
optimization benchmarks in the form of real-valued functions with
multiple minima and maxima. Such functions are difficult for
optimization and reflect the challenges optimization algorithms meet
in real world problems. Two functions are chosen for the study, the
Ackley function \cite{Ackley.function} and a slightly modified version
of the Himmelblau function \cite{Wikipedia.Himmelblau_function}. The
functions are scaled into a cube with coordinate range $[-1, 1]$
along each edge.

\subsubsection{The Ackley Function}

The two-argument form of the Ackley function is used. The function is
defined by the expression (\ref{eq:emp-ackley}):
\begin{equation}
\label{eq:emp-ackley}
A(x,y)=20\cdot \exp\left(-0.2\sqrt { \frac {x^2+y^2} 2}\right)
       +\exp\left(\frac{cos(2\pi x)+cos(2\pi y)} 2\right)
\end{equation}
and is plotted in Figure~\ref{fig:emp-ackley}. One can see multiple
interspersing minima and maxima which present potential difficulty
for an optimization algorithm.

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.8,trim=100pt 75pt 100pt 50pt]{ackley.pdf}
\end{center}
\caption{The Ackley function.}
\label{fig:emp-ackley}
\end{figure}

\subsubsection{The Himmelblau Function}

The Himmelblau function is defined by expression (\ref{eq:emp-himmelblau}):
\begin{equation}
\label{eq:emp-himmelblau}
H(x,y)=16x-12y-(x^2+y-11)^2-(x+y^2-7)^2
\end{equation}

The plot of the function (Figure~\ref{fig:emp-himmelblau}) suggests
that this function is much smoother than the Ackley function. However,
the relative smoothness does not necessarily makes optimization
easier: a slope-driven algorithm, for example, would have difficulty
determining the search direction in flat regions.

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.8,trim=100pt 75pt 100pt 50pt]{himmelblau.pdf}
\end{center}
\caption{The Himmelblau function.}
\label{fig:emp-himmelblau}
\end{figure}

\subsection{Optimization Problems}

Two optimization problems are considered, with a sigmoid utility
function $\tanh(2z)$. In the former problem the
movements are free:
\begin{verbatim}
problem obmark_free {
  space (x, y) {
    x = [-1.0, -0.8 .. 1.0];
    y = [-1.0, -0.8 .. 1.0];
  }
  observe f {
    1=> f = 0.5 / 0.01;
  }
  find max util(x, y) = tanh(2*f);
  model lattice(x, y) {
    f = (0, 1) / (0.5, 0.5);
  }
}
\end{verbatim}
In the latter one the movement cost is proportional to the Manhattan
distance:
\begin{verbatim}
problem obmark_manhattan {
  space (x, y)  / 0.02*(abs(@x)+abs(@y)) {
    x = [-1, -0.8 .. 1];
    y = [-1, -0.8 .. 1];
  }
  observe f {
    1=> f = 0.5 / 0.002;
  }
  find max util(x, y) = tanh(2*f);
  model lattice(x, y) {
    f = (0, 1) / (0.5, 0.5);
  }
}
\end{verbatim}
In both problems, the measurements are noisy and there are uniform
dependencies between neighbor nodes in both directions of the
coordinate grid.

\subsection {Experiment and Representation of Results}
\label{sec:emp-functions-experiment}

For each problem and each function, the myopic and the blinkered
scheme are run 64 times. The chosen locations are recorded and the
results are represented graphically (see Figures~\ref{fig:emp-obmark-free-ackley}--\ref{fig:emp-obmark-manhattan-himmelblau}). In
each figure, the contour plot of the function is provided for
reference (the leftmost plot). Then, the plots in the upper and the lower
rows depict the selected locations\footnote{The coordinates of selection
  locations are slightly randomly moved in the plots to show that a
  particular location was selected multiple times.} and the histogram
of rewards (the sum of the utility and the budget surplus) for the
myopic and the blinkered scheme, correspondingly. The extent to
which the locations are grouped around the maxima, and the
utility distribution histograms provide for easy visual comparison of
the results. The accompanying textual description includes some
quantitative performance measures.

\subsection {Free Movements}

The case of free movements is relatively easy for the
information-based optimization, and both schemes behave comparably
on either function (Figures~\ref{fig:emp-obmark-free-ackley},~\ref{fig:emp-obmark-free-himmelblau}). In particular, both schemes
select locations close to the global maximum of the Ackley function
but fail to identify the drops in the utility in the saddles
$(\pm0.2, 0)$, $(0, \pm0.2)$. For the Himmelblau function, most
selected locations are in high utility areas, but since the proximity
of the global maximum is rather flat, the algorithm terminates without
achieving the exact maximum: further measurements are not worth their
cost.

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.75,trim=125pt 25pt 100pt 0pt]{obmark-free-ackley.pdf}
\end{center}
\caption{The Ackley function, free movements.}
\label{fig:emp-obmark-free-ackley}
\end{figure}

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.75,trim=125pt 25pt 100pt 0pt]{obmark-free-himmelblau.pdf}
\end{center}
\caption{The Himmelblau function, free movements.}
\label{fig:emp-obmark-free-himmelblau}
\end{figure}

Still, for the Ackley function (Figure~\ref{fig:emp-obmark-free-ackley}), the myopic scheme produced two
outliers:
\begin{verbatim}
solution: x=0.6 y=0.6 => util=0.447507 / surplus=0.01 # true utility -0.71
solution: x=-1 y=-1 => util=0.824489 / surplus=0.01 # true utility -1
\end{verbatim}
The mean reward is $0.92$ for the blinkered, $0.81$ for the myopic scheme.

For the Himmelblau function, the utility histogram for the myopic scheme
has a longer left tail, with rewards as low as $\approx 0.6$,
while the lowest reward for the blinkered scheme is $\approx
0.9$. The myopic scheme  also chose a few locations with low
true utilities:
\begin{verbatim}
solution: x=-0.2 y=-0.8 => util=0.900673 / surplus=0.01 # true utility=0.57
solution: x=0.0 y=-0.6 => util=0.286363 / surplus=0.31 # true utility=0.66
\end{verbatim}
The difference in the mean reward is less prominent: $1.25$ for the
blinkered, $1.23$ for the myopic scheme.

Thus, while on average in the case of free movements both schemes
behave comparably for the given parameters, the myopic scheme is
more likely to select a location with a low utility.

\subsection {Manhattan Distance Movement Costs}

When the movement cost is non-zero, the myopic value of information
estimate, which cannot take into account amortization of the movement
cost over multiple measurements, is essentially inferior to the
blinkered estimate. Indeed, the experiment shows better performance of
the blinkered estimate on both functions (Figures~\ref{fig:emp-obmark-manhattan-ackley},~\ref{fig:emp-obmark-manhattan-himmelblau}).

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.75,trim=125pt 25pt 100pt 0pt]{obmark-manhattan-ackley.pdf}
\end{center}
\caption{The Ackley function, manhattan movement cost.}
\label{fig:emp-obmark-manhattan-ackley}
\end{figure}

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.75,trim=125pt 25pt 100pt 20pt]{obmark-manhattan-himmelblau.pdf}
\end{center}
\caption{The Himmelblau function, manhattan movement cost.}
\label{fig:emp-obmark-manhattan-himmelblau}
\end{figure}

For the Ackley function (Figure~\ref{fig:emp-obmark-manhattan-ackley}),
the mean reward is $0.59$ for the blinkered, $0.40$ for the myopic
scheme. The locations selected by the myopic scheme are spread
over a wide area and are less focused around the true maximum $(0,0)$;
$9$ locations among selected by the myopic scheme have a negative
utility, compared to $3$ for the blinkered scheme.

The difference in performance for the Himmelblau function is less
obvious from the location plot due to the flatter shape of the
function, but the reward distribution histogram shows more locations
with lower utility. The mean reward is $1.20$ for the blinkered, $1.10$
for the myopic scheme.

\subsection {Conclusions}

Empirical evaluation of the myopic and the blinkered scheme on two
functions confirmed superiority of the blinkered scheme for inexact
measurements. When the function has areas with steep gradients, the
myopic scheme tends to terminate prematurely, selecting a
suboptimal location even for free movements. When movement costs are
positive, the blinkered scheme outperforms the myopic one even in
flatter areas, apparently by sampling the optimization space more
efficiently.
