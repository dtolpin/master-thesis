% $Id: bg-opt.tex,v 1.21 2009/09/07 13:29:46 dvd Exp $

\section{Optimization under Uncertainty}
\label{sec:bg-opt}

Problems of optimization under uncertainty are characterized by the
necessity of making a decision without being able to predict its exact
effect. Such problems appear in many different areas and are both
conceptually and computationally challenging. Optimization under
uncertainty shares many basic notions with other branches of
optimization, but still differs from them in several formulation,
modeling, and solution issues.

\subsection{Review of Terms}

\begin{description}
\item[Optimization:] Making a single choice from a set of ``feasible''
  ones, the {\em feasible set}. The goal of optimization is to make a
  ``best'' choice, or at least a better choice than a random one. The
  choices are compared according to the values they give to a certain
  function---the {\em objective function} of the problem.

Optimization problems are classified according to their domain as
either {\em discrete} or {\em continuous}. In discrete problems, the
feasible set is countable and often finite; in the continuous ones,
the feasible set can be modeled as a subset of ${\mathcal R}^n$.

\item[Uncertainty:] Lack of exact knowledge about the values the
  objective function takes for all or some members of the feasible
  set. The choice depends on the belief about the distribution of the
  objective function values.

\item[Observation:] Obtaining information about the objective
  function. An observation can be either {\em exact} or {\em
    inexact}. An exact observation is the true value of the objective
  function for a particular set member. An inexact observation may deviate
  from the true value, and the deviation distribution may be unknown.

\end{description}

\subsection{Applications}

Optimization under uncertainty is used to solve problems in diverse
domains. A few examples are below; they all can be 
formulated as problems of minimizing or maximizing a function under
constraints, when some of the data is unavailable or inexact.

\begin{description}
\item[Design of experiments:] Errors are inherent to outcomes of an
  experiment. The same experiment is often repeated multiple times,
  with either the same or altered parameter values. Planning the
  experiments helps save time and resources.
\item[Industrial design:] Complex design tasks, such as aerospace
  vehicle design \cite{PadulaGumbertLi.aerospace}, involve modeling of
  a complex process with uncertain parameters. Parameters that
  ensure robust and efficient operation in varying or inexactly known
  conditions must be chosen.
\item[Medical diagnostics:] A diagnosis can be made or confirmed
  through a series of tests. Tests can be costly and harmful, or take
  too long to treat the disease in a timely manner. Choosing the right set
  and sequence of medical tests is crucial for health care.
\item[Environmental control:] Large systems such as water distribution
  networks \cite{Krause.water} are monitored by sensor arrays. The
  sensors must be placed in a way that maximizes confidence about the
  state of the system.
\end{description}

\subsection{Approaches}

Over the years, various uncertainty modeling philosophies have been
developed \cite{Sahinidis2004}. The range of approaches to
optimization under uncertainty includes, among others, stochastic
and fuzzy programming.

\begin{description}

\item[Stochastic programming:] Mathematical programming where some of
  the data incorporated into the objective or constraints are
  uncertain. Uncertainty is characterized by probability distributions
  of the data. One looks either for a solution with the {\em best
    expected value}, or for a {\em robust} one, that is, good enough
  in all feasible cases.
  
\item[Fuzzy mathematical programming:] Addresses the same class of
  problems, but instead of modeling uncertainty probabilistically,
  defines the objective function parameters as fuzzy members and
  uncertain constraints as fuzzy sets. The solution techniques are
  based on the fuzzy set theory \cite{Zimmermann.fuzzy}.
  
\item[Stochastic dynamic programming:] Focuses on states and controls
  in a context of stochastic dynamics. In its center is Bellman's {\em
    principle of optimality} stating that in each state, the remaining
  decisions must be optimal for the tail subproblem. The multi-stage
  problem is very intensive computationally, and thus approximate
  algorithms had to be developed.

\end{description}

\subsubsection{Bayesian Approach}
\label{sec:bg-opt-bayesian}

The Bayesian approach to optimization under uncertainty is outlined in
\cite{CaramanisMannor.bayesian}. In this approach, the uncertainty
model incorporates prior information or beliefs and is then updated
based on observed data using a Bayesian step.

Consider an optimization problem with uncertain coefficients:
\begin{eqnarray}
\label{eq:bg-opt}
 \mbox{min:}&&f(x,\omega) \nonumber\\
 \mbox{s.t.:}&&x \in X
\end{eqnarray}
where a value of $x$ that minimizes $f$ must be found, and the
parameter $\omega$ is uncertain. The typical robust optimization
approach selects an uncertainty set $\Omega$, and then solves the
robustified version of the optimization problem:
\begin{eqnarray}
\label{eq:bg-opt-robust}
 \mbox{min:}&&\max_{\omega\in\Omega}f(x,\omega)\nonumber\\
 \mbox{s.t.:}&&x \in X
\end{eqnarray}
Here, a value of $x$ that minimizes the maximum value of $f$ for the
given $x$ and any value of $\omega \in \Omega$ must be found.

A typical stochastic optimization approach is to assume a
distribution, conditional on $x$, for the uncertain parameter $\omega$
and optimize the expected value of the function instead:
\begin{eqnarray}
\label{eq:bg-opt-stochastic}
\mbox{min:}&&\IE_\omega\left[f(x,\omega)\right]=\int\limits_\Omega f(x, \omega)
p(\omega|x) d\omega \nonumber\\
\mbox{s.t.:}&&x \in X
\end{eqnarray}
where $p(\omega|x)$ is the conditional probability density function of
the uncertain parameter $\omega$. This approach is based on the
assumption that a good probabilistic model of the uncertainty is
available.

In the Bayesian approach, the distribution for $\omega$ is not known
in advance, as required by stochastic optimization, but rather is
gradually learned by combining prior beliefs with observed data into a
posterior distribution for $\omega$. Realizations of the Bayesian
approach differ in the way in which data about $\omega$ are obtained.

Optimization of the expected value of the objective function is only
one approach viable under the Bayesian model. It is possible to
consider risk-based or other distribution-based constraints instead.
