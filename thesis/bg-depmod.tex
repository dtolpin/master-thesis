% $Id: bg-depmod.tex,v 1.34 2009/09/05 16:33:34 dvd Exp $

\section{Graphical Models}
\label{sec:bg-depmod}

A probabilistic graphical model \cite{Wikipedia.Graphical_model} is a
probabilistic model for which a graph denotes the conditional
dependencies between random variables. Graphical models are commonly
used in probability theory, statistics---particularly Bayesian
statistics---and machine learning. Two commonly used types of
graphical models are {\em Bayesian networks} and {\em Markov networks}
(or {\em Markov random fields}).

Bayesian networks are useful for expressing causal relationships
between random variables, whereas Markov random fields are better
suited to expressing correlation constraints between random
variables. For the purposes of solving inference problems, it is often
convenient to convert both models into a different representation
called a {\em factor graph}.

\subsection{Bayesian Networks}

A Bayesian network \cite{Pearl.PRIS} is a direct acyclic graph
$G=(V,E)$ whose nodes $V$ represent variables, and the arcs $E$
signify the existence of direct causal influences between the linked
variables, and the strengths of these influences are expressed by
forward conditional probabilities.

The joint distribution defined by a Bayesian network is given by the
product, over all of the nodes of the network graph, of the conditional
distribution for each node conditioned on the variables corresponding
to the parents of the node in the network graph. Thus, for a graph
with $K$ nodes, the joint distribution is given by

\begin{equation}
\label{eq:bg-depmod-bn}
p(\mathbf{x})=\prod_{k=1}^Kp(x_k|pa_k)
\end{equation}
where $x_k$ is the variable corresponding to the $k$-th node $v_k$,
$pa_k$ denotes the set of parents $\{u\;\vline\;\left<u,v_k\right> \in E\}$ of $x_k$, and $\mathbf{x} = \{x_1 , \ldots, x_K\}$.

\begin{figure}[h]
\begin{center}
\input{bg-depmod-bn-example.tex}
\end{center}
\caption{Example of a Bayesian network graph}
\label{fig:bg-depmod-bn-example}
\end{figure} 

The graph in Figure~\ref{fig:bg-depmod-bn-example}, taken from
\cite{Bishop.learning}, corresponds to a Bayesian network of 7
nodes. The joint distribution of variables $x_1, \ldots, x_7$
corresponding to the nodes is given by~\ref{eq:bg-depmod-bn-example}:

\begin{eqnarray}
\label{eq:bg-depmod-bn-example}
p(x_1,x_2,x_3,x_4,x_5,x_6,x_7)&=&p(x_1)\cdot p(x_2)\cdot p(x_3)\nonumber\\
                             &&\cdot p(x_4|x_1,x_2,x_3 )\nonumber\\
                             &&\cdot p(x_5|x_1,x_3)\nonumber\\
                             &&\cdot p(x_6|x_4)\nonumber\\
                             &&\cdot p(x_7|x_4,x_5)
\end{eqnarray}

\subsubsection {Gaussian Bayesian Networks}

A multivariate Gaussian can be expressed as a directed graph
corresponding to a linear-Gaussian model over the component
variables \cite{Bishop.learning}.

Consider an arbitrary directed acyclic graph over $D$ variables in which node $i$
represents a single continuous random variable $x_i$ having a Gaussian distribution.
The mean of this distribution is taken to be a linear combination of the states of the
parent nodes $pa_i$ of node $i$:

\begin{equation}
  \label{eq:bg-depmod-bn-lg}
  p(x_i|pa_i)=\mathcal{N}\left(x_i\vline \sum_{j\in pa_i} w_{ij} x_j + b_i, v_i\right)
\end{equation}
where $w_{ij}$ and $b_i$ are parameters governing the mean, and $v_i$ is
the variance of the conditional distribution for $x_i$. It is easy to
see that the joint distribution $p(\mathbf{x})$ is Gaussian. The mean
and covariance of the joint distribution are recursively determined
by equations~\ref{eq:bg-depmod-bn-gm} and~\ref{eq:bg-depmod-bn-gv}:
\begin{equation}
  \label{eq:bg-depmod-bn-gm}
  \IE[x_i]=\sum_{j\in pa_i} w_{ij} \IE[x_j]+b_i
\end{equation}
\begin{equation}
  \label{eq:bg-depmod-bn-gv}
  \cov [x_i, x_j] = \sum_{k \in pa_j} w_{jk} \cov [x_i, x_k]+I_{ij}v_j
\end{equation}
where $I_{ij}$ is the $i, j$ element of the identity matrix.

\subsection{Markov Networks}

A Markov network \cite{Wikipedia.Markov_random_field}, or a Markov
random field, is a graphical model in which a set of random variables
has a Markov property described by an undirected graph. A Markov
network is similar to a Bayesian network in its representation of
dependencies. A markov network can represent certain dependencies that
a Bayesian network cannot (such as cyclic dependencies); on the other
hand, a markov network cannot represent certain dependencies that a
Bayesian network can (such as induced and non-transitive dependencies
\cite{Pearl.PRIS}).

For convenience and without loss of generality, nodes in the graph
are often divided into two sets, such that
\begin{itemize}
\item $x_i$ are nodes corresponding to unobserved variables;
\item $y_i$ are nodes corresponding to observed variables;
\item each observed node is connected to and only to
  the corresponding unobserved node.
\end{itemize}


\begin{figure}[h]
\begin{center}
\input{bg-depmod-mrf.tex}
\end{center}
\caption{Markov network}
\label{fig:bg-depmod-mrf}
\end{figure}

The joint probability distribution of the variables in the graph is
determined by product of {\em potential functions} of the maximal
cliques in the graph. Denoting by $\Psi_c$ the potential of clique
$x_c$, and by $\Psi_{ii}$ the potential of the two-element clique
$(x_i,y_i)$, the joint probability distribution is expressed by
equation~\ref{eq:bg-depmod-mrf-p}:
\begin{equation}
\label{eq:bg-depmod-mrf-p}
P(x,y)=\alpha \prod_c\Psi_c(x_c)\prod_i\Psi_{ii}(x_i,y_i)
\end{equation}
where $\alpha$ is a normalization constant.

\subsubsection {Gaussian Markov Random Fields}

A Gaussian Markov random field \cite{WeissFreeman.correctness} is a
Markov random field in which the joint distribution is
Gaussian. Without the loss of generality the joint mean can be assumed
zero, and the joint distribution of $z=\left(\begin{array}{c}x\\y\end{array}\right)$ is thus given by~\ref{eq:bg-depmod-gmrf-dist}:
\begin{equation}
\label{eq:bg-depmod-gmrf-dist}
P(z)=\alpha e ^{-\frac 1 2 z^T V z}
\end{equation}
where $\alpha$ is a normalization constant and $V$ has the following
structure:

\begin{equation}
\label{eq:bg-depmod-gmrf-vxy}
V=\left(
\begin{array}{c c}
 V_{x x} & V_{x y} \\
 V_{y x} & V_{y y} 
\end{array}
\right)
\end{equation}

If the maximal cliques in the graph are pairwise cliques, then the
joint distribution has a representation as a product of pairwise
potentials. That is, there exist matrices $V_{ij}$ such that~\ref{eq:bg-depmod-gmrf-dist}
can be represented as~\ref{eq:bg-depmod-gmrf-pairwise-dist}:
\begin{equation}
\label{eq:bg-depmod-gmrf-pairwise-dist}
P(z)=\alpha \prod_{i,j}e ^{-\frac 1 2 (x_i x_j)V_{i j}(x_i x_j)^T}
\prod_ie ^{-\frac 1 2 (x_i y_i)V_{i i}(x_i y_i)^T}
\end{equation}
where the first product is over connected pairs of nodes.

\subsection{Inference}
\label{sec:bg-depmod-inference}

Bayesian inference \cite{Wikipedia.Bayesian_inference} is statistical
inference in which evidence or observations are used to update or to
newly infer the probability that a hypothesis may be true. The name
``Bayesian'' comes from the frequent use of Bayes' theorem in the
inference process. Bayes' theorem was derived from the work of the
Reverend Thomas Bayes.

Let $(E,F)$ be a partioning of the indices of the nodes in a graphical
model into disjoint subsets, such that $(X_E, X_F)$ is a partioning of
random variables represented by the graphical model
\cite{JordanWeiss.inference}. There are two basic kinds of inference
problems:

\begin{itemize}
\item {\em Marginal probabilities:}
\begin{equation}
\label{eq:bg-depmod-inf-mpe}
p(x_E)=\sum_{x_F} p(x_E, x_F)
\end{equation}
\item{\em Maximum a posteriori (MAP) probabilities:}
\begin{equation}
\label{eq:bg-depmod-inf-map}
p^*(x_E)=\max_{x_F} p(x_E, x_F)
\end{equation}
\end{itemize}

The most popular approaches to design of inference algorithms are {\em
variable elimination}, {\em message passing}, and {\em junction
tree} \cite{MacKay.itp}\cite{JordanWeiss.inference}.

\begin{description}
\item[Variable elimination:] Algorithms in this class eliminate
  variables from the $X_F$ subset one by one. The algorithms are
  exact. They are applicable to general graphs, but mostly used on
  trees.
\item[Message passing algorithms:] Inference is viewed in terms of
  local (per-node) computation and routing of messages between nodes.
  The belief propagation algorithm based on message passing is correct
  for trees \cite{Pearl.PRIS} and is used to obtain approximate
  solutions for general graphs \cite{WeissFreeman.correctness}. The
  belief propagation algorithm applied to graphs with loops is
  customarily called {\em loopy belief propagation}.
\item[Junction tree algorithm:] A generalization of message
  passing algorithms for general graphs. The underlying data structure,
  also called {\em junction tree} is a tree in which each node is a
  clique from the original graph; messages pass between these cliques.
\end{description}

There also inherently approximate algorithms for inference; two widely
used classes are {\it Monte Carlo algorithms} and {\it variational
methods} \cite{FreyJojic.inference}\cite{JordanWeiss.inference}.

\begin{description}
\item[Monte Carlo algorithms:] Monte Carlo algorithms use the fact
that while it may not be feasible to compute expectations under a
general distribution $p(x)$, it may be possible to obtain samples from
p(x) such that marginals and other expectations can be approximated using
sample-based averages. Three examples of Monte Carlo algorithms 
used in the graphical model setting are Gibbs sampling, the Metropolis-Hastings
algorithm and importance sampling \cite{FreyJojic.inference}\cite{JordanWeiss.inference}.
\item[Variational Methods:] In the variational approach, the
probabilistic inference  problem is converted into an optimization
problem; to solve the problem, the standard tools of constrained
optimization can be exploited  \cite{FreyJojic.inference}\cite{JordanWeiss.inference}.
\end{description}

Inference in graphical models representing multivariate Gaussian
distribution can be done exactly in polynomial time ($O(n^3)$ in the
number of variables $n$). Still, in many cases this time complexity is
too high for practical applications, and approximate inference based
on loopy belief propagation is used instead.

A general belief propagation algorithm works on {\em factor graphs},
bi-partite graphs where the original graph's nodes are connected to
nodes corresponding to cliques to which they belong. However for both
general Bayesian networks and a restricted form of Markov random
fields there are versions of the algorithm that do not require
building the factor graph and act on the original graph directly.
In the following two sections, formulae for belief propagation
messages in directed and undirected graphs are provided.

\subsection{Belief Propagation in Gaussian Bayesian Networks}

In Bayesian networks, two kinds of messages are sent \cite{Pearl.PRIS}: 
\begin{itemize}
\item $\pi_{ij}=\mathcal{N}(\mu_{ij}^+,\nu_{ij}^+)$ to each child $x_j$ of node $x_i$;
\item $\lambda_{ij}=\mathcal{N}(\mu_{ij}^-,\nu_{ij}^-)$ to each parent
  $x_j$ of node $x_i$;
\end{itemize}
and the belief about $x_i$ is a normal distribution $\BEL(x_i)=\mathcal{N}(\mu_i, \nu_i)$.

Defining the parameters according to Equation~\ref{eq:bg-depmod-bn-lg}:

\begin{eqnarray}
\label{eq:bg-depmod-gbn-lpar}
\nu_i^\lambda=\left[\sum_{j \in ch_i}{\frac 1 {\nu_{ji}^-}}\right]^{-1},&&
\mu_i^\lambda=\nu_i^\lambda\sum_{j \in ch_i}\frac {\mu_{ji}^-} {\nu_{ji}^-} \\
\nu_i^\pi=v_i+\sum_{j \in pa_i} w_{ji}\sigma_{ji}^+,&&
\mu_i^\pi=b_i+\sum_{j \in pa_i}\sqrt{w_{ji}}\mu_{ji}^+
\end{eqnarray}

where $ch_i$, $pa_i$ are correspondingly the sets of indexes of
children and parents of node $x_i$, $\BEL(x_i)$ and the messages
emerging from $x_i$ are given the following formulae:

\begin{eqnarray}
\label{eq:bg-depmod-gbn-bel}
\mu_i=\frac {\nu_i^\pi\mu_i^\lambda+\nu_i^\lambda\mu_i^\pi}
   {\nu_i^\pi+\nu_i^\lambda},&&
\nu_i=\frac {\nu_i^\pi\nu_i^\lambda} {\nu_i^\pi+\nu_i^\lambda}
\end{eqnarray}

\begin{eqnarray}
\label{eq:bg-depmod-gbn-msg}
&&\mu_{ij}^+=\frac {\sum_{k \in pa_i\backslash j}\frac {\mu_{ki}^-}
  {\nu_{ki}^-}+\frac {\mu_i^\pi} {\nu_i^\pi}} {\sum_{k \in pa_i\backslash j}\frac 1
  {\nu_{ki}^-}+\frac 1 {\nu_i^\pi}},\;
\nu_{ij}^+=\left[\frac 1 {\nu_i^{\pi}}+\sum_{k \in pa_i\backslash j}
    \frac 1 {\nu_{ki}^-}\right]^{-1} \\
&&\mu_{ij}^-=\frac 1 {\sqrt {w_{ji}}}\left[\mu_i^\lambda-b_i-\sum_{k \in ch_i
    \backslash j} \sqrt {w_{ki}} \mu_{ki}^+ \right],\nonumber\\
&&\nu_{ij}^-=\frac 1 w_{ji}\left[\nu_i^\lambda+\nu_i+\sum_{k \in
    ch_i \backslash j} w_{ki}\nu_{ki}^+\right]
\end{eqnarray}

The boundary conditions appropriate for this propagation scheme can be
established by assigning $\nu=\infty$ (representing totally unknown
inputs) and $\nu=0$ (representing observed inputs) to the peripheral
nodes. 

\subsection{Belief Propagation in Gaussian Markov Random Fields}

Belief propagation in Gaussian Markov random fields takes a simple
form for graphs with maximal cliques of size 2. Graphs of this
form are widely used in the form of rectangular grids. General graphs
can be converted to satisfy this condition
{\cite{WeissFreeman.correctness}.

As in the case of Gaussian Bayesian networks, the updates can be
written directly in terms of the means and inverse covariance
matrices. Each node sends and receives a mean vector and inverse
covariance matrix to and from each neighbor, in general, each
different.

To explicitly write the updates in terms of means and covariances,
let $\mu_{ij}$ be the mean of the message that node $x_i$
sends to node $x_j$ and $P_{ij}$ the precision or inverse covariance
matrix that node $x_i$ sends to node $x_j$. Similarly, let $\mu_i$ be
the mean of the belief at node $x_i$ and $P_i$ the inverse covariance matrix of the
belief. $\mu_{ii}$, $P_{ii}$ are used for the message that $y_i$ sends
to $x_i$. Matrix $V_{ij}$ is partitioned into:

\begin{equation}
\label{eq:bg-depmod-gmrf-v}
V_{ij}=\left(\begin{array}{c c}a&b\\b^T&c\end{array}\right)
\end{equation}

The message update rules are:

\begin{equation}
\label{eq:bg-depmod-pij}
P_{ij} \leftarrow c-b(a+P_0)^{-1}b^T
\end{equation}

\begin{equation}
\label{eq:bg-depmod-muij}
\mu_{ij} \leftarrow -P_{ij}^{-1}b(a+P_0)^{-1}P_0\mu_0
\end{equation}
with:
\begin{equation}
\label{eq:bg-depmod-p0}
P_0 = P_{ii}+\sum_{x_k \in N(x_i)\backslash x_j} P_{ki}
\end{equation}

\begin{equation}
\label{eq:bg-depmod-mu0}
\mu_0 = P_0^{-1}\left(P_{ii}\mu_{ii}+\sum_{x_k \in N(x_i)\backslash x_j} P_{ki}\mu_{ki}\right)
\end{equation}
where $N(x_i)$ stands for the set of members of $x_i$. The beliefs are
given by:

\begin{equation}
\label{eq:bg-depmod-pi}
P_i \leftarrow P_{ii}+\sum_{x_k \in N(x_i)} P_{ki}
\end{equation}

\begin{equation}
\label{eq:bg-depmod-mui}
\mu_i \leftarrow P_i^{-1}\left(P_{ii}\mu_{ii}+\sum_{x_k \in N(x_i)} P_{ki}\mu_{ki}\right)
\end{equation}

$P_{ii}$, $\mu_{ii}$ are computed from equations~\ref{eq:bg-depmod-pij},~\ref{eq:bg-depmod-muij} with $\mu_0=y$ and
$P_0=\infty I$:


\begin{equation}
\label{eq:bg-depmod-pii}
P_{ii} \leftarrow c
\end{equation}

\begin{equation}
\label{eq:bg-depmod-muii}
\mu_{ii} \leftarrow c^{-1}b\mu_0
\end{equation}
