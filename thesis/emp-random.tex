% $Id: emp-random.tex,v 1.14 2009/09/07 07:01:55 dvd Exp $

\section{Simulated Random Inputs}
\label{sec:emp-random}

In this set of experiments, for a given number of items, their exact
values and initial prior beliefs are randomly generated. Then, for a
range of measurement costs, budgets, and observation precisions, the
search is run, randomly generating observation outcomes according to
the exact values of the items and the measurement model. The
performance measure is the regret---the difference between the
utility of the best item and the utility of the selected item, taking
into account the measurement costs.  Results of using the blinkered
scheme vs. other semi-myopic schemes are compared.

The first comparison is the difference in regret between the myopic
and blinkered schemes, done for 2 items, one of which has a known
value (Table~\ref{tbl:n2-t5}). Positive values in the cells
indicate an improvement due to the blinkered estimate. Note that the
absolute value is bounded by 0.5, the difference in the utility of the
exactly known item and the extremal utility.  Table~\ref{tbl:n4-t10}
repeats the experiment for 4 items.

\begin{table}[h] 
\begin{center} 
\begin{tabular}{l|r r r r}
\begin{tabular}{l|r}$\sigma_o$ & $C$\end{tabular}
    & 0.0005  &  0.0010 & 0.0015 & 0.0020 \\ \hline
3.0      & 0.0147 & 0.0156 & 0.0199 & 0.2648 \\
4.0      & 0.0619 & 0.2324 & 0.2978 & 0.2137 \\
5.0      & 0.2526 & 0.2322 & 0.1729 & 0.1776 \\
6.0      & 0.1975 & 0.1762 & 0.1466 & 0.0000 \\
\end{tabular} 
\caption{Myopic vs. blinkered: 2 items, 5 measurement budget} 
\label{tbl:n2-t5} 
\end{center} 
\end{table} 

\begin{table}[h] 
\begin{center} 
\begin{tabular}{l|r r r r}
\begin{tabular}{l|r}$\sigma_o$ & $C$\end{tabular}
    & 0.0005  &  0.0010 & 0.0015 & 0.0020 \\ \hline
3.0          & 0.0113 & -0.00459 & -0.0024 & 0.4352 \\
4.0          & 0.0374 &  0.43435 &  0.4184 & 0.3902 \\
5.0          & 0.4060 &  0.40004 &  0.3534 & 0.3599 \\
6.0          & 0.4082 &  0.37804 &  0.3337 & 0.0000 \\
\end{tabular} 
\caption{Myopic vs. blinkered: 4 items, 10 measurement budget} 
\label{tbl:n4-t10} 
\end{center} 
\end{table} 

Averaged over 100 runs of the experiment, the difference is
significantly positive for most combinations of the parameters. In the
first experiment (Table~\ref{tbl:n2-t5}), the mean regret of the myopic scheme compared
to the blinkered scheme is 0.15 with standard deviation 0.1.  In the
second experiment (Table~\ref{tbl:n4-t10}), the mean regret is 0.27
with standard deviation 0.19.

\subsection {Other Semi-Myopic Estimates}

Here, three semi-myopic schemes, introduced in
Section~\ref{sec:thr-semi-myopic-schemes}, are compared: blinkered,
omni-myopic, and exhaustive\footnote{The omni-myopic and the
  exhaustive scheme were added to the implementation for these
  experiments but are not available generally due to their
  intractability.}. All schemes were run on a set of 5 items with a 10
measurement budget.


\begin{table}[h] 
\begin{center} 
\begin{tabular}{l|r r r r}
\begin{tabular}{l|r}$\sigma_o$ & $C$\end{tabular}
    & 0.0005  &  0.0010 & 0.0015 & 0.0020 \\ \hline
3.0 & -0.1477 & 0.0946 & 0.1889 & 0.2807 \\
4.0 & 0.0006 & 0.0382 & 0.4045 & 0.4180 \\
5.0 & 0.2300 & 0.3954 & 0.2925 & 0.3222 \\
6.0 & 0.0494 & 0.2374 & 0.1452 & 0.2402 \\
\end{tabular} 
\caption{Omni-myopic vs. blinkered: 5 items, 10 measurements budget} 
\label{tbl:planhead-mb} 
\end{center} 
\end{table} 

\begin{table}[h] 
\begin{center} 
\begin{tabular}{l|r r r r}
\begin{tabular}{l|r}$\sigma_o$ & $C$\end{tabular}
    & 0.0005  &  0.0010 & 0.0015 & 0.0020 \\ \hline
3.0 & 0.0218 & 0.0307 & 0.1146 & -0.1044 \\
4.0 & -0.0502 & 0.0703 & 0.0598 & 0.0022 \\
5.0 & 0.0940 & -0.1508 & -0.0865 & 0.1432 \\
6.0 & 0.0146 & 0.1485 & -0.0505 & 0.2068 \\
\end{tabular} 
\caption{Blinkered vs. exhaustive: 5 items, 10 measurements budget} 
\label{tbl:planhead-be} 
\end{center} 
\end{table} 

\begin{table}[h] 
\begin{center} 
\begin{tabular}{l|r r r r}
\begin{tabular}{l|r}$\sigma_o$ & $C$\end{tabular}
    & 0.0005  &  0.0010 & 0.0015 & 0.0020 \\ \hline
3.0 & -0.0781 & -0.0167 & 0.0391 & 0.0125 \\
4.0 & 0.0000 & 0.0974 & 0.1848 & -0.0982 \\
5.0 & 0.0609 & -0.0002 & 0.0000 & 0.0000 \\
6.0 & 0.1272 & 0.0000 & 0.0000 & 0.0000 \\
\end{tabular} 
\caption{Myopic vs. omni-myopic: 5 items, 10 measurements budget} 
\label{tbl:planhead-mo} 
\end{center} 
\end{table} 

The results show that
\begin{itemize}
\item blinkered is significantly better than omni-myopic
  (Table~\ref{tbl:planhead-mb}): the mean regret is 0.20 with standard deviation
  0.16;
\item exhaustive is only marginally better than blinkered
  (Table~\ref{tbl:planhead-be}), despite evaluating an exponential
  number of sets of measurements: the mean regret is 0.03 with standard deviation
  0.10;
\item omni-myopic is only marginally better than myopic
  (Table~\ref{tbl:planhead-mo}): the mean regret is 0.02 with standard deviation
  0.07.
\end{itemize}

\subsection {Dependencies between Items}

When the values of the items are linearly dependent, e.g. when: $y_i =
y_{i-1} + w$ with $w$ being a random variable distributed as
$N(0,\sigma_w^2)$, the VOI of series of observations of several items
can be greater than the sum of VOI of each observation. The experiment
examines the influence of dependencies on the relative quality of the
blinkered and the omni-myopic scheme.

When there are no dependencies, i.e. $\frac {\sigma_o^2} {\sigma_w^2}
= 0$, the blinkered scheme is significantly better. But as $\sigma_w$
decreases, the omni-myomic estimate performs better. Figure~\ref{fig:emp-random-dependency} shows the difference between achieved
utility of the blinkered and the myopic schemes with dependencies. The
experiment was run on a set of 5 items, with the prior estimate
$N(0,1)$, measurement precision $\sigma_o^2=4$, measurement cost
$C=0.002$ and a budget of 10 measurements. The results are averaged
over 100 runs of the experiment.

\begin{figure}[ht] 
\begin{center}
\includegraphics[scale=0.8,trim=0pt 15pt 0pt 50pt,clip]{random-dependency.pdf}
\end{center}
\caption{Influence of dependencies} 
\label{fig:emp-random-dependency}
\end{figure} 

In the absence of dependencies, the omni-myopic algorithm does not perform measurements
and chooses an item at random, thus performing poorly. 
As the dependencies become stronger, the omni-myopic
scheme collects evidence and eventually outperforms the blinkered scheme.
In the experiment, the omni-myopic scheme begins to outperform the blinkered scheme
when dependencies between the items are roughly half as strong as the 
measurement accuracy.
