from scipy.stats import beta
from scipy.integrate import quad 
from numpy import argmax

# A simple demonstration of computation of expected
# utility and intrinsic VOI. Every item has one feature,
# distributed as Beta(a, b) (a bandit with unknown mean,
# where the mean is the feature).

def p(a,b):
    "belief about feature value"
    return lambda x: beta.pdf(x, a, b)

# A measurement returns either 0 or 1,
# with probability p=a/(a+b).

def pm(p):
    "belief about measurement outcome given feature value"
    return lambda y: (y==1) and p or (1.-p)

# Expected utility for a given belief is simply the integral
# of the utility function over the belief.

def eu(u, p):
    "expected utility"
    return quad(lambda x: u(x)*p(x), 0, 1)[0]

# VOI is a double integral. The outer integral is over belief
# distribution, the inner integral is over the measurement outcomes
# given the belief. Since the measurements are discrete, we replace
# the integral with summation.

def voi(u, items, i):
    "value of information of measuring the ith item"
    # current best
    eus = [eu(u, p(a, b)) for a, b in items]
    current_best = argmax(eus) 

    def du(y):
        """difference between utilities 
        given outcome y of measurement i"""
        # make a local copy of items 
        items_ = items[:]; items_[i] = items_[i][:]
        # update items_ according to the measurement outcome
        items_[i][y]+= 1
        # select next best
        eus = [eu(u, p(a, b)) for a, b in items_]
        next_best = argmax(eus)
        # finally return the different between posterior 
        # expected utilities of next and current bests
        return eus[next_best]-eus[current_best]

    # Integrate over possible measurement outcomes of item i
    a,b = items[i]
    # The summation is in fact the inner integral
    return quad(lambda x: (du(0)*pm(x)(0) + du(1)*pm(x)(1))*p(a,b)(x),
                0, 1)[0]


# Let's demonstrate now how this works. Define an array of items,
# with their current beliefs, and compute the VOI of measuring
# every single item.

def demo():
    items = [[1,1], [2,2], [13, 11], [120, 100]]
    u = lambda x: x
    for i in range(len(items)):
        print voi(u, items, i),
    print

if __name__=="__main__":
    demo()
