% $Id: emp-random.tex,v 1.1 2009/11/29 14:33:00 dvd Exp $

\section{Simulated Random Inputs}
\label{sec:emp-random}

In this set of experiments, a set of independent items with a single
feature is given. The utility function is the step function (see the
example in Section \ref{sec:greedy-mvi-shortcomings}), and the initial
beliefs are normal distributions $N(\mu=0,\sigma^2=1)$. True feature value is
randomly generated for each item from the initial belief distribution;
then, for a range of measurement costs, budgets, and observation
precisions, the simulation is run. Observation outcomes are generated
randomly according to the exact values of the item features and the
measurement model. The performance measure is the regret---the
difference between the utility of the best item and the utility of the
selected item less the measurement costs. Results of using the myopic
scheme, the blinkered scheme and other semi-myopic schemes are
compared.

\subsection{Blinkered vs. Myopic}

The first comparison is the difference in regret between the myopic
and the blinkered scheme, done for 2 items (Table~\ref{tbl:n2-t5}).
Positive values in the cells indicate an improvement due to the
blinkered estimate. Table~\ref{tbl:n4-t10} repeats the experiment
for 4 items.

\begin{table}[h] 
\caption{Myopic vs. blinkered: 2 items, measurement budget 5} 
\label{tbl:n2-t5} 
\small
\centering
\begin{tabular}{l|r r r r}
$\sigma_o \quad C$
    & 0.0005  &  0.0010 & 0.0015 & 0.0020 \\ \hline
3.0      & 0.0147 & 0.0156 & 0.0199 & 0.2648 \\
4.0      & 0.0619 & 0.2324 & 0.2978 & 0.2137 \\
5.0      & 0.2526 & 0.2322 & 0.1729 & 0.1776 \\
6.0      & 0.1975 & 0.1762 & 0.1466 & 0.0000 \\
\end{tabular} 
\end{table} 

\begin{table}[h] 
\caption{Myopic vs. blinkered: 4 items, measurement budget 10} 
\label{tbl:n4-t10} 
\small
\centering
\begin{tabular}{l|r r r r}
$\sigma_o\quad C$
    & 0.0005  &  0.0010 & 0.0015 & 0.0020 \\ \hline
3.0          & 0.0113 & -0.00459 & -0.0024 & 0.4352 \\
4.0          & 0.0374 &  0.43435 &  0.4184 & 0.3902 \\
5.0          & 0.4060 &  0.40004 &  0.3534 & 0.3599 \\
6.0          & 0.4082 &  0.37804 &  0.3337 & 0.0000 \\
\end{tabular} 
\end{table}

Averaged over 100 runs of the experiment, the difference is
significantly positive for most combinations of the parameters. In the
first experiment (Table~\ref{tbl:n2-t5}), the mean regret of the myopic scheme compared
to the blinkered scheme is 0.15 with standard deviation 0.1.  In the
second experiment (Table~\ref{tbl:n4-t10}), the mean regret is 0.27
with standard deviation 0.19.

\subsection {Other Semi-Myopic Estimates}

Three semi-myopic schemes, introduced in
Section~\ref{sec:thr-semi-myopic-schemes}, are compared: blinkered,
omni-myopic, and exhaustive\footnote{The omni-myopic and the
  exhaustive scheme were added to the implementation for these
  experiments but are not available generally due to their
  intractability.}. All schemes are run on a set of 5 items with a
measurement budget of 10.

The results show that
\begin{itemize}
\item blinkered is significantly better than omni-myopic: the mean
  regret is 0.20 with standard deviation 0.16;
\item exhaustive is only marginally better than blinkered, despite
  evaluating an exponential number of sets of measurements: the mean
  regret is 0.03 with standard deviation 0.10;
\item omni-myopic is only marginally better than myopic: the mean
  regret is 0.02 with standard deviation 0.07.
\end{itemize}

\subsection {Dependencies between Items}

When linear dependencies between the items are added, e.g. when: $z_i =
z_{i-1} + w$ with $w$ being a random variable distributed as
$N(0,\sigma_w^2)$, the VOI of series of observations of several items
can be greater than the sum of VOI of each observation. The experiment
examines the influence of dependencies on the relative quality of the
blinkered and the omni-myopic scheme.

Without dependencies, i.e. for $\frac {\sigma_o^2} {\sigma_w^2}
= 0$, the blinkered scheme is significantly better. But as $\sigma_w$
decreases, the omni-myomic estimate performs
better. Figure~\ref{fig:emp-random-dependency} shows the difference
between achieved utility of the blinkered and the myopic schemes with
dependencies. The experiment is run on a set of 5 items, with the
prior belief $N(0,1)$, measurement precision $\sigma_o^2=4$,
measurement cost $C=0.002$ and a budget of 10 measurements. The
results are averaged over 100 runs of the experiment.

\begin{figure}[ht] 
\centering
\includegraphics[scale=0.75]{random-dependency.pdf}
\caption{Influence of dependencies} 
\label{fig:emp-random-dependency}
\end{figure} 

In the absence of dependencies, the omni-myopic algorithm does not
perform measurements and chooses an item at random, thus performing
poorly.  As the dependencies become stronger, the omni-myopic scheme
collects evidence and eventually outperforms the blinkered scheme.  In
the experiment, the omni-myopic scheme begins to outperform the
blinkered scheme when dependencies between the items are roughly half
as strong as the measurement accuracy.
