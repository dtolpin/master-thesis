% $Id: emp-functions.tex,v 1.2 2009/11/29 20:00:53 dvd Exp $


\section {Optimization Benchmarks}

A popular way to compare optimization algorithms is to apply them to
real-valued functions with multiple minima and maxima. Such functions
are difficult for optimization and reflect the challenges which
optimization algorithms meet in real world problems. Two functions
were used for the study, the Ackley function \cite{Ackley.function}
and a modified version of the Himmelblau function
\cite{Wikipedia.Himmelblau_function}. The functions are scaled into a
cube with coordinate range $[-1, 1]$ along each edge.

The two-argument form of the Ackley function is used. The function is
defined by the expression (\ref{eq:emp-ackley}):
\begin{eqnarray}
\label{eq:emp-ackley}
A(x,y)&=&20\cdot \exp\left(-0.2\sqrt { \frac {x^2+y^2} 2}\right)\nonumber\\
      &&+\exp\left(\frac{cos(2\pi x)+cos(2\pi y)} 2\right)\nonumber\\
\end{eqnarray}


\subsection{Optimization Problems}

Two optimization problems are considered, with a sigmoid utility
function $\tanh(2z)$. In the former problem the movements are free;
in the latter one the movement cost is proportional to the Manhattan
distance. In both problems, the measurements are noisy with $\sigma_e^2=0.5$ and
and there are uniform dependencies with $\sigma_w^2=0.5$ between
neighbor nodes in both directions of the coordinate grid with a step
of $0.2$ along each axis.

\subsection {Experiment and Representation of Results}
\label{sec:emp-functions-experiment}

For each problem, the myopic and the blinkered
scheme were run 64 times. The chosen locations were recorded, as shown
graphically (see
Figures~\ref{fig:emp-obmark-free-ackley}, \ref{fig:emp-obmark-manhattan-ackley}). In
each figure, the plots in the upper and the lower
rows depict the selected locations\footnote{The coordinates of selection
  locations are slightly randomly moved in the plots to show that a
  particular location was selected multiple times.} and the histogram
of rewards (the sum of the utility and the budget surplus) for the
myopic and the blinkered scheme, correspondingly. The extent to
which the locations are grouped around the maxima, and the
utility distribution histograms provide for easy visual comparison of
the results. The accompanying textual description includes some
quantitative performance measures.

\subsection {Free Movements}

The case of free movements is relatively easy for the
information-based optimization, and both schemes behave comparably
(Figure~\ref{fig:emp-obmark-free-ackley}). close to the global maximum of the Ackley function
but fail to identify the drops in the utility in the saddles
$(\pm0.2, 0)$, $(0, \pm0.2)$. For the Himmelblau function, most
selected locations are in high utility areas, but since the proximity
of the global maximum is rather flat, the algorithm terminates without
achieving the exact maximum: further measurements are not worth their
cost.
$(\pm0.2, 0)$, $(0, \pm0.2)$.
\begin{figure}[h]
\centering
\includegraphics[scale=0.8,trim=200pt 0pt 0pt 0pt,clip]{obmark-free-ackley.pdf}
\caption{The Ackley function, free movements.}
\label{fig:emp-obmark-free-ackley}
\end{figure}
\begin{figure}[h]
\centering
\includegraphics[scale=0.8,trim=200pt 0pt 0pt 0pt,clip]{obmark-free-himmelblau.pdf}
\caption{The Himmbelblau function, free movements.}
\label{fig:emp-obmark-free-himmelblau}
\end{figure}
Still, for the Ackley function, the myopic scheme produced two
outliers: $(0.6, 0.6)$ and $(-1, -1)$.  The mean reward is $0.92$ for
the blinkered, $0.81$ for the myopic scheme.  Thus, while on average
in the case of free movements both schemes behave comparably for the
given parameters, the myopic scheme is more likely to select a
location with a low utility.

For the Himmelblau function, the utility histogram for the myopic
scheme has a longer left tail, with rewards as low as $\approx 0.6$,
while the lowest reward for the blinkered scheme is $\approx 0.9$. The
myopic scheme also chose a few locations with low true utilities:
$(-0.2,-0.8)$, $(0.0,-0.6)$. The difference in the mean reward is less
prominent: $1.25$ for the blinkered, $1.23$ for the myopic scheme.

Thus, while on average in the case of free movements both schemes
behave comparably for the given parameters, the myopic scheme is
more likely to select a location with a low utility.

\subsection {Manhattan Distance Movement Costs}

When the movement cost is non-zero, the myopic scheme, which cannot
take into account amortization of the movement cost over multiple
measurements, is essentially inferior to the blinkered scheme. Indeed,
the experiment shows better performance of the blinkered scheme
(Figure~\ref{fig:emp-obmark-manhattan-ackley}) on both functions.

\begin{figure}[h]
\centering
\includegraphics[scale=0.8,trim=200pt 0pt 0pt 0pt,clip]{obmark-manhattan-ackley.pdf}
\caption{The Ackley function, manhattan movement cost.}
\label{fig:emp-obmark-manhattan-ackley}
\end{figure}
\begin{figure}[h]
\centering
\includegraphics[scale=0.8,trim=200pt 0pt 0pt 0pt,clip]{obmark-manhattan-himmelblau.pdf}
\caption{The Himmelblau function, manhattan movement cost.}
\label{fig:emp-himmelblau-manhattan-ackley}
\end{figure}

For the Ackley function, the mean reward is $0.59$ for the blinkered,
$0.40$ for the myopic scheme. The locations selected by the myopic
scheme are spread over a wide area and are less focused around the
true maximum $(0,0)$; $9$ locations among selected by the myopic
scheme have a negative utility, compared to $3$ for the blinkered
scheme.

The difference in performance for the Himmelblau function is less
obvious from the location plot due to the flatter shape of the
function, but the reward distribution histogram shows more locations
with lower utility. The mean reward is $1.20$ for the blinkered, $1.10$
for the myopic scheme.
