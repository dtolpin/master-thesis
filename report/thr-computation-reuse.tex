% $Id: thr-computation-reuse.tex,v 1.1 2009/11/29 14:33:02 dvd Exp $

\section{Reusing Computations}
\label{sec:thr-computation-reuse}

On the one hand, limited rationality approach (Section~\ref{sec:bg-limited}) assumes that the time required to estimate value of
information of a meta-level action is negligible. On the other hand,
the greedy algorithm (Chapter~\ref{sec:greedy}) performs massive
computations at each step: 
\begin{itemize}
\item evaluates the expected utilitiy of every item;
\item evaluates the value of information of every measurement;
\item updates the beliefs by propagating the newly obtained evidence;
\end{itemize}
and the need to implement the computations efficiently is even more
prominent when a semi-myopic value of information estimate is used,
because more evaluations of value of information per step must be
performed.

This section suggests improvements to the greedy algorithm
that make the algorithm run faster and scale better to large numbers of items and
evaluation kinds. Most of the improvements are implemented in the code
used for empirical evaluation (Chapter~\ref{sec:emp}) and proved useful
in practice. 

\subsection{Expected Utilities}

At each step, the greedy algorithm evaluates the expected utility of
every item. The expected utility of an item depends only on beliefs
about the item features. If the beliefs have not been affected by the
last update, the computation from an earlier step can be re-used.

The beliefs are affected by an update when either the item has been
directly observed, or when the beliefs about the item's features have
been changed due to belief propagation after observing another
item. Thus, to implement selective recomputation of expected
utilities, the dependency model must be extended to provide
information about a change in the belief at every item. With this
modification, the utility updating loop (lines 4--6) of the algorithm
in Figure~\ref{alg:greedy-algorithm} will take the following form:

\begin{algorithmic}[0,start=4]
  \FORALL {items $s_i$}
    \IF {$s_i$ updated}
      \STATE compute $\IE(U_i)$
    \ENDIF
  \ENDFOR
\end{algorithmic}

This change is exact: the algorithm still recomputes all expected
utilities which might have changed. However, the change considerably
decreases the computation time, in particular, in the case of large
dependency models with disconnected components.

\subsection{Domain-specific Inference}

The greedy algorithm spends a significant portion of the deliberation
time updating beliefs. When the dependency model is large or
complicated, a straightforward implementation of an inference
algorithm is too slow. Better results are achieved when the structure
of updates is exploited.

An iterative belief propagation algorithm is commonly used for
inference in networks of general topology. The algorithm initializes
messages between nodes to some values, and then traverses the network
repeatedly until the computed beliefs converge or the iteration limit
is reached.

\subsubsection{Incremental Updating}
\label{sec:thr-incremental-updating}

Generally, the messages are initialized arbitrarily. Then, if
the algorithm converges, the messages approach a fixed point. Good
initial values cannot be chosen easily and are not worth bothering with:
the algorithm rapidly approaches the proximity of a fixed point and
then slowly, in the presence of loops,\footnote{When the network graph
is a polytree, the algorithm converges to an exact solution in a
fixed number of iterations \cite{Cowell.inference}.} achieves the
required precision.

However, in the optimization problem, the evidence is added gradually,
and the model drives the search but does not significantly affect the
final result: at the end of the search, best and close to best nodes
are known with high confidence. Therefore, the messages can be reused
and the computation can be sped up:

\begin{itemize}
\item final message values in each iteration become initial values for
  the next iteration;
\item since only a small amount of evidence is added at each
  iteration, most messages remain almost unchanged;
\item relatively low precision is sufficient, therefore good initial
  values significantly decrease the computation time.
\end{itemize}

Messages are initialized once in the beginning, and then the model is
updated incrementally.

\subsubsection {Propagation Order}

The convergence speed depends on the traversal order. The following example
illustrates the dependence: in a one-dimensional Markov random field,
$x_1,\ldots,x_n$ (Figure~\ref{fig:thr-markov-chain}), the only
obtained evidence is $y_n$ for the rightmost node $x_n$. 

\begin{figure}[h]
\begin{center}
\input{thr-markov-chain.tex}
\end{center}
\caption{Propagation direction}
\label{fig:thr-markov-chain}
\end{figure}

When the propagation algorithm traverses the chain from left to right
$(A)$, it takes $O(n)$ iterations to propagate the
evidence through the chain. However, if the propagation direction is
from right to left $(B)$, only $O(1)$ iterations are required. 

Every time the optimization algorithm updates beliefs, new observations are
available for just a small number of variables.

\begin{itemize}
\item when a measurement is chosen by the algorithm, only a single
  node is measured;
\item before a measurement is chosen, the algorithm simulates
  batches of measurements; but every batch should be reasonably small
  for the algorithm to remain tractable. 
\end{itemize}

Like in the one-dimensional case, the propagation should start from
the newly observed variables and recursively proceed to the
neighbors. This order ensures that the convergence speed does not
depend on the measurement location. Besides that, if the dependency
model is disconnected---consists of multiple components with
probabilistic dependencies only within the components but not between
them (see Section~\ref{sec:emp-case-mms} for a case study)---only
affected components participate in message passing and the computation
time is not wasted in the parts which were not updated.

\subsubsection{Value of Information Estimates}

The utility function depends on the parameter vector (see
Section~\ref{sec:greedy-simplified-problem}). Thus, to compute value
of information of a batch of measurements, a multidimensional integral
must be evaluated. The integral is commonly estimated using a Monte
Carlo method, which means a function must be evaluated multiple times
with different arguments. Each such evaluation corresponds to a new
evidence, and the dependency model must be updated (and reverted)
multiple times per computation of value of information of each batch.

With large models and multiple measurements this amount of computation
becomes prohibitive. Fewer iterations of belief propagation is a
natural solution which can be justified in the following way:

\begin{itemize}
\item an error in value of information estimate results in a less
  efficient measurement (in the worst case), but does not affect the
  dependency model and the final choice;
\item the propagation error does not accumulate: the evidence is added
  incrementally, and while the estimated influence of the batch is
  computed with a lower precision, the propagation messages are
  are initialized from precise inference of all the evidence up
  to date. 
\end{itemize}

The approximation can be taken to extreme, and value of information
can be computed without propagating beliefs at all: only the beliefs that
are directly affected by a measurement are updated. The algorithm
used for the empirical evaluation (Section~\ref{sec:emp}) does not
propagate beliefs while computing value of information. This approximation
has two potential drawbacks:

\begin{itemize}
\item value of information of measurements in close vicinity of the
  $\alpha$ item can be overestimated;
\item superadditivity of measurements caused by dependencies cannot be discovered.
\end{itemize}

However, these cases are infrequent, and in practice computation of value of
information without belief propagation works well.
