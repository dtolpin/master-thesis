% $Id: app-tightness.tex,v 1.1 2009/11/29 14:32:59 dvd Exp $

\chapter {Tightness of Bounds}
\label{app:tightness}

\section {Tightness of the Bound in Theorem~\ref{th:bound-single}}

The bound is asymptotically tight. Consider the following problem
instance:
\begin{itemize}
  \item the utility of $s_1$ is exactly known an equal $0$;
  \item according to the prior belief, $s_2$ can have utility $- \frac
h    {C_b} p$, $0$ or $\frac {C_b} p$ with equal probability, for some
    $0 \le p \le 1$;
  \item the measurement cost function is defined as
    follows\footnote{The second measurement is more expensive than the
      first one when, for example, in a physical experiment one has to
      wait in order to exclude inference between the measurements.}:
    \begin{eqnarray*}
      c(x_0, x_2)&=&0\\
      c(x_2, x_2)&=&C_b,
    \end{eqnarray*} that is, at most two measurements can be made,
    the first measurement is free, the cost of the second measurement
    is $C_b$;
  \item the first measurement of $s_2$ either discovers with
    probability $p$ that the utility of $s_2$ is $- \frac
    {C_b} p$ or $\frac {C_b} p$, or, with probability $1-p$ that the
    utility is exactly $0$;
  \item the second measurement of $s_2$ determines the exact utility
    of the item.
\end{itemize} 

The blinkered value of information estimate is $0$: the intrinsic
value of information of the first measurement is $0$, and of the first
and the second measurement is $\frac {C_b} p p=C_b$ --- the utility of
$s_2$ is always determined exactly, and is $\pm\frac {C_b} p$ with
probability $p$ and $0$ with probability $1-p$. Thus, the blinkered
scheme will not perform any measurements.

The optimal policy is to 
\begin{enumerate}
  \item perform the first measurement,
  \item if the utility of $s_2$ is not zero, perform the second
    measurement. 
\end{enumerate} The probability of the second measurement is $p$
and the expected cost of the policy is $pC_b$. The value of
information of the optimal policy is thus:
\[V_o=p\frac {C_b} p - pC_b=(1-p)C_b\]
and approaches $C_b$ when $p$ approaches $0$:
\[ \lim_{p\to 0}V_o=C_b \]
The latter limit shows that the bound stated in Theorem~\ref{th:bound-single} is asymptotically tight.

\section {Tightness of the Bound in Theorem~\ref{th:bound-multiple}}

The bound is asymptotically tight. Construct a problem instance
as follows: $M$ items, with value of information of $j$th item for $i$
measurements $v_j(i)$:

\[ v_1(i) = \sqrt[k] {\frac i N} \]

\[ v_{j>1}(i) = \left\{ 
\begin{array}{l l}
  0 & \quad \mbox{if $i<\frac N M$}\\
  \sqrt[k] {\frac 1 M} & \quad \mbox{otherwise}\\
\end{array} \right. \]

Value of information of a combination of the measurements
is the sum of the values for each item:

\[ v(i)=\sum_{j=1}^M v_j(i_j) \]

Here, the optimal policy is to measure each item $\frac N M$ times. The resulting
value of information for $N$ measurements is:

\[ V_o = v_1\left(\frac N M\right)+(M-1)v_{j>1}\left(\frac N M\right)
       = M \sqrt[k] {\frac 1 M} \]

\[ \lim_{k\to\infty}M \sqrt[k] {\frac 1 M} = M \]

But the blinkered scheme will always choose the first item with \[
V_b=v_1(N)=1. \]

\subsection {Comparative Example}

In the following example the blinkered scheme selects better
measurements than the myopic one. Consider a modification of the example in
Section~\ref{sec:thr-mvi-shortcomings}, with an additional item $s_3$ and
free measurements:

\begin {itemize}
\item $S$ is a set of three items, $s_1$, $s_2$, and $s_3$;
\item the value of $s_1$ is known exactly, $x_1=1$;
\item the prior belief about $s_2$ is nomal $p_2(x)=\mathcal{N}(x;0,1)$;
\item the observation variance of $s_2$ is $\sigma_{e2}^2=5$;
\item the prior belief about $s_3$ is  normal: $p_3(x)=\mathcal{N}(x;0.0,0.16)$;
\item a measurement of the item returns the exact value: $\sigma_{e3}^2=0.0$;
\item measurements are free: $c=0$;
\item the budget is two measurements;
\item the utility is a step function:

\[u(x) = \left\{ 
\begin{array}{l l}
  0 & \quad \mbox{if $x<1$}\\
  0.5 & \quad \mbox{if $x=1$}\\
  1 & \quad \mbox{if $x>1$}\\
\end{array} \right. \]

\end {itemize}

The myopic scheme measures $s_3$, and then $s_2$, since
$MVI_3 \approx 0.0024>MVI_2\approx 0.0004$. However, the blinkered
scheme first measures $s_2$, since $MVI_2^2 \approx
0.0028>MVI_3^2 \approx 0.0024$, and then chooses between $s_2$ and
$s_3$ depending on the obtained observation. In particular, if the
posterior mean of $s_2$ is approximately in the range $[0.39, 1.6]$,
$s_2$ is chosen, otherwise, $s_3$ is chosen.

It is easy to show that only these two scenarios are possible:
\begin{enumerate}
\item measure $s_3$, and then $s_2$;
\item measure $s_2$, and then either $s_3$ or $s_2$, depending on
  the value of each measurement;
\end{enumerate}
and the latter policy is optimal. Thus, the blinkered scheme
implements the optimal policy, while the myopic one misses cases where
$s_2$ should be measured twice for the best expected result.  Of
course, one can construct an example where the blinkered scheme is far
from optimal. Still, the empirical evaluations show that the blinkered
scheme behaves significantly better than the myopic one, and gives
good results in cases where the myopic scheme fails.

