% $Id: greedy.tex,v 1.1 2009/11/29 14:33:01 dvd Exp $

\section{The Metareasoning Approach}

In the framework of the rational metareasoning approach
(Section \ref{sec:bg-limited}), a measurement corresponds to a meta-level
action, and the final selection is a base-level action. Thus, a
measurement with the highest net value of information is performed at each step.
The algorithm terminates when no measurement has positive apparent net
value of information, at which point the item with the highest expected utility
(given the current information) is selected and returned.
The joint feature distribution is initialized to a {\em prior
  belief}, and then new evidence is incorporated into the belief
through Bayesian inference~\cite{MacKay.itp}.

The expected utility is computed according to
(\ref{eq:bg-limited-eu}), with integration instead of summation for
the continuous case:
\begin{equation}
\IE[U_i]=\int\!\!\!\int\limits_{\mathbb{R}^{N_f}}\!\! u(\textbf{z}) p_i(\textbf{z})d\textbf{z}
\label{eq:greedy-meta-eu}
\end{equation}
where $p_i$ is the current belief about feature values of item
$s_i$.

The net value of information $V$ is the difference between the
intrinsic value, $\Lambda$, and the measurement cost. For a finite set
of items of size $N_s$, there are
$N_m\cdot N_s$ different measurements at each step, each determined by the
measurement type and the measured item. Assuming that the
measurements are arbitrarily indexed, the value of the $j$th
measurement is:
\begin{equation}
V_j=\Lambda_j-c_j=\Lambda_j-d(s,s_{j})-c^m_j
\label{eq:greedy-meta-v}
\end{equation}
where $s$ is the last measured item, and $c_j$ is the net measurement
cost---the sum of the intrinsic measurement cost and the movement
cost. Under the myopic simplifying assumptions
(Section~\ref{sec:bg-limited}), the intrinsic value of
information is estimated myopically: as the expected immediate effect
of a single measurement. Following (\ref{eq:bg-limited-benefit}),
the intrinsic value of information is determined by the following equation:
\begin{eqnarray}
\Lambda_j&=&\IE(\IE[U_{\alpha_j}]-\IE[U_\alpha])\nonumber\\
&=&\!\!\int\!\!\!\!\int\limits_{\mathbb{R}^{2N_f}}\!\!(\IE[U_{\alpha_j}]\!-\!\IE[U_\alpha])
                p^m_j(\textbf{z}_m|\textbf{z})p_j(\textbf{z}) 
                d\textbf{z}_md\textbf{z}\nonumber\\
                &&
\label{eq:greedy-meta-benefit}
\end{eqnarray}
where $s_{\alpha}$ is the item with the highest expected utility
before the measurement, $s_{\alpha_j}$ --- after the $j$th
measurement, and the expected utilities are computed for the updated
beliefs.

\section{Algorithm Description}

\begin{figure}
\begin{algorithmic}[1]
\STATE $budget \leftarrow C$
\STATE Initialize beliefs to $B_0$ \label{alg:greedy-initialize-beliefs}
\LOOP                              \label{alg:greedy-main-loop-start}
  \FORALL {items $s_i$} 
    \STATE Compute $\IE(U_i)$
  \ENDFOR
  \FORALL {measurements $m_j$} \label{alg:greedy-select-start}
    \IF {$c_j \le budget$} \label{alg:greedy-within-budget}
      \STATE Compute $V_j$
    \ELSE
      \STATE $V_j \leftarrow 0$
    \ENDIF
  \ENDFOR
  \STATE $j_{max} \leftarrow \arg \max\limits_j V_j$
  \IF {$V_{j_{max}}>0$}  \label{alg:greedy-positive-value}
    \STATE Perform measurement $m_{j_{max}}$
    \STATE Update beliefs \label{alg:greedy-update-beliefs}
    \STATE $budget \leftarrow budget-c_{j_{max}}$ \label{alg:greedy-select-end}
  \ELSE                       
    \STATE {\bf break} \label{alg:greedy-break}
  \ENDIF
\ENDLOOP                \label{alg:greedy-main-loop-end}
\STATE $\alpha \leftarrow \arg \max \IE(U_i)$ \label{alg:greedy-return-alpha}
\RETURN $s_\alpha$
\end{algorithmic}
\caption{Greedy Myopic Algorithm}
\label{alg:greedy-algorithm}
\end{figure}

The greedy myopic algorithm (see Figure~\ref{alg:greedy-algorithm})
maintains a persistent data structure which holds beliefs about
feature values of the items. The beliefs are initialized to the prior
beliefs (line~\ref{alg:greedy-initialize-beliefs}), and then updated
according to measurement outcomes
(line~\ref{alg:greedy-update-beliefs}). The main loop
(lines~\ref{alg:greedy-main-loop-start}--\ref{alg:greedy-main-loop-end})
continues as long as there are measurements that fit within the budget
(line~\ref{alg:greedy-within-budget}) with positive value
(line~\ref{alg:greedy-positive-value}). Otherwise, the loop terminates
(line~\ref{alg:greedy-break}), and the algorithm returns the parameter
vector of an item with the maximum expected utility
(line~\ref{alg:greedy-return-alpha}).

The remaining budget is held in variable $budget$, which is initialized
to the total budget $C$, and decreased by the cost of each performed
measurement. Thus, the algorithm is guaranteed to terminate when the costs
of all measurements are positive and bounded away from zero.

\section{Theoretical Bounds}

Online measurement selection is $\mathrm{NP}^{\mathrm{PP}}$-hard
\cite{Guestrin.optimal}, thus the need for approximation algorithms
arises. The greedy myopic algorithm is one feasible approach; however,
in the general case the algorithm can behave arbitrarily badly.  For
example, if at least two measurements are required to achieve a
positive value, the algorithm will terminate without making any
measurements (see below). On the other hand, if the myopic value of
information estimate of a measurement is submodular, the algorithm is
nearly optimal \cite{Guestrin.submodular}.

The myopic value of information estimate in the optimization problem
can be shown to approach submodularity in some cases, for example,
when the measurements are exact and the utility function is
concave. Mostly, however, the estimate is nonsubmodular and no
non-trivial performance guarantees can be provided.

\section{Shortcomings of the Myopic Estimate}
\label{sec:greedy-mvi-shortcomings}

The simplifying assumptions behind the myopic estimate  are related
to the notion of {\it non-increasing returns}: an implicit hypothesis that the intrinsic
value grows more slowly than the cost. When the hypothesis is correct,
the assumptions should work well; otherwise, the myopic algorithm either
gets stuck or makes measurements which gain little useful information.

\begin{figure}[h]
\centering
\includegraphics[scale=0.8]{s-curve.pdf}
\caption{The s-curve} 
\label{fig:greedy-s-curve}
\end{figure} 

However, the {\it law of diminishing returns} \cite{Johns.economics}
only holds asymptotically: while it is often true that starting with
some point in time the returns never grow, until that point they can
alternate between increases and decreases. Figure~\ref{fig:greedy-s-curve}
 presents the curve of diminishing returns (the
dashed curve), the s-curve (the solid curve), and the areas with
negative and positive values for the s-curved returns for a linear
cost (the dotted straight line). Investments for the first two units
of time do not pay off, and the maximum return is achieved at
approximately $2.8$.

Sigmoid-shaped returns were discovered in marketing
\cite{Johansson.s-curve}.  As experimental results show
\cite{Zilberstein.sensing}, they are not uncommon in sensing and
planning. In such cases, an approach that can deal with increasing
returns must be used.

Even in simple cases the myopic estimate may behave poorly.  Consider
the following example:

\begin {itemize}
\item $S$ is a set of two items , $s_1$ and $s_2$ with a scalar feature vector
  $\textbf{z}=z$; 
\item the value of feature $z$ of $s_1$ is known exactly, $z(s_1)=1$;
\item the prior belief about $s_2$ is a normal distribution $p_2(z)=N(z;0,1)$;
\item the observation conditional variance is $\sigma_m^2=5$;
\item the measurement cost is constant, and chosen so that the net
  value estimate of a two-observation step is zero: $c(j)=\frac {MVI_2^2} 2\approx
  0.00144$; the movement cost is zero.
\item the utility is a step function:
\[u(z) = \left\{ 
\begin{array}{l l}
  0 & \quad \mbox{if $z<1$}\\
  0.5 & \quad \mbox{if $z=1$}\\
  1 & \quad \mbox{if $z>1$}\\
\end{array} \right.
 \]
\end {itemize}

The plot in Figure~\ref{fig:greedy-mvilim-value-cost} depicts the intrinsic
value of information as a function of the number of observations in a single
step. The straight line corresponds to the measurement cost.

\begin{figure}[h]
\centering
\includegraphics[scale=0.8]{mvilim-value-cost.pdf}
\caption{Intrinsic value and measurement cost} 
\label{fig:greedy-mvilim-value-cost}
\end{figure} 

Under these conditions, the pure myopic algorithm (which considers
only one measurement per step)
will terminate without gathering evidence because the value estimate
of the first step is negative, and will return item $s_1$ as
best. However, observing $s_2$ several times in a row has a positive
value, and the updated expected utility of $s_2$ can eventually become
greater than $u(\textbf{z}(s_1))$.  Figure~\ref{fig:greedy-mvilim-value-cost} also
shows the intrinsic value growth rate as a function of the number of
measurements: the rate increases up to a maximum at 3 measurements,
and then goes down. Apparently, the myopic algorithm does not ``see''
as far as the initial increase.


