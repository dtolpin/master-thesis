% $Id: thr-blinkered-estimate.tex,v 1.1 2009/11/29 14:33:02 dvd Exp $

\section{Semi-Myopic Schemes}
\label{sec:thr-semi-myopic-schemes}

Keeping the complexity manageable (the number of possible sensing
plans over continuous variables is uncountably infinite) while
overcoming the limitations of the myopic algorithm
is the basis for the {\em semi-myopic} framework. 
Let ${\mathcal M}$ be the set of all possible measurement
actions, and ${\mathcal C}$ be a constraint over sets of
measurements from ${\mathcal M}$. In the semi-myopic framework,
all possible subsets (batches) $B$ of measurements from ${\mathcal M}$
that obey the constraint ${\mathcal C}$ are considered, and for each such subset
${\mathcal B}$ a `batch' value of information estimate is computed
under the assumption that all measurements in ${\mathcal B}$ are made, followed by
a decision (selection of an item). Then, the batch
${\mathcal B}^*$ with the best value estimate is chosen. Once the best
batch ${\mathcal B}^*$ is chosen, there are still several options
(\cite{BilgicGetoor.voila} discusses a similar approach to discrete
feature acquisition):

\begin{enumerate}
\item Actually do all the measurements in ${\mathcal B}^*$.
\item Attempt to optimize ${\mathcal B}^*$ into some form of
  conditional plan of measurements and the resulting observations.
\item Perform the best measurement in ${\mathcal B}^*$.
\end{enumerate}

In all cases, after measurements are made, the selection is repeated,
until no batch has a positive value, at which point the algorithm {\em
  terminates} and selects an item. Option 1, a combination of online
and offline selection, misses the possibility to choose better
measurements while executing a batch, and therefore is inferior to
options 2 and 3. While limited efficient implementation for option 2 is
possible, optimizing a conditional plan is intractable in general,
recreating the optimal policy problem on a smaller scale.  However,
option 3, with a suitable constraint, remains tractable and offers an
opportunity to make a better choice at each step.

In the semi-myopic version, the measurement selection part
(lines~\ref{alg:greedy-select-start}--\ref{alg:greedy-select-end})
of the algorithm in Figure~\ref{alg:greedy-algorithm} is
replaced with the algorithm in Figure~\ref{alg:semi-myopic-scheme}.
\begin{figure}
\begin{algorithmic}[0,start=4]
  \FORALL {batches $b_j$ satisfying constraint $\mathcal{C}$}
    \IF {$cost(b_j) \ge budget$}
      \STATE compute $V_j^b$
    \ELSE
      \STATE $V_j^b \leftarrow 0$
    \ENDIF
  \ENDFOR
  \STATE $j_{max} \leftarrow \arg \max\limits_j V_j^b$
  \IF {$V_{j_{max}}^b>0$}
    \FORALL {measurements $m_k \in b_{j_{max}}$}
       \STATE compute $V_k$
    \ENDFOR
    \STATE $k_{max} \leftarrow \arg \max\limits_k V_k$
    \STATE perform measurement $m_{k_{max}}$
    \STATE update beliefs
    \STATE $budget \leftarrow budget-c_{k_{max}}$
  \ENDIF
\end{algorithmic}
\caption{Semi-myopic Scheme}
\label{alg:semi-myopic-scheme}
\end{figure}
Here, value of information is computed twice: first, value of
information $V_j^b$ of every batch $b_j$ satisfying the constraint
$\mathcal{C}$ is computed; then, if the maximum value
of information of a batch is positive, value of information $V_k$ of
every single measurement $m_k$ belonging to the batch $b_{j_{max}}$ is
computed, and a measurement with the highest value of
information (which can be negative) is performed.

The constraint $\mathcal{C}$ is crucial and defines a {\em
  scheme}. For the empty constraint --- the {\em exhaustive} scheme
--- all possible measurement sets are considered; this scheme has an
exponential computational cost, while still not guaranteeing
optimality.  At the other extreme is the constraint where only
singleton sets are allowed. This extreme results in the greedy
single-step assumption --- the original myopic scheme.
\cite{BilgicGetoor.voila} proposes a constraint based on the {\em
  value of information lattice}, a data structure introduced in that
paper. The induced number of subsets is still exponential in a general
case, but for certain kinds of dependency structure the constraint
gives significant improvement. Yet another --- {\em omni-myopic} ---
constraint can be constructed along the lines of
\cite{Heckerman.nonmyopic}: the measurements are ordered according to
their myopic VOI estimates, and subsets of measurements with greatest
VOI estimates are considered.

This paper investigates the constraint that restricts the batches to
repeated identical measurements---the {\em blinkered} scheme. This
scheme is tractable, corresponds to a common approach in which noisy
experiments are repeated to increase confidence, and demonstrates
improved algorithm efficiency in empirical evaluations.

\section{Blinkered Estimate}
\label{sec:thr-blinkered-estimate}

As stated above, the blinkered scheme considers sets of independent identical
measurements; this constitutes unlimited lookahead, but along a single
``direction'', as if one ``had one's blinkers on''.  Although this
scheme has a computational overhead over the myopic one, the factor is
only linear in the budget. \footnote{This complexity assumes either
  normal distributions, or some other distribution that has compact
  statistics. For general distributions, sets of observations may
  provide information beyond the mean and variance, and the resources
  required to compute value of information may even be exponential in
  the number of measurements.}  The ``blinkered'' value of information
is defined as:
\begin{eqnarray}
\label{eq:thr-bvi}
BVI_i&=&\max_k MVI_i^k\nonumber\\
\mathrm{s.t.}:&&c_i=d_i+c^m_ik \leq C
\end{eqnarray} 
where, as before, $d_i$ and $c^m_i$ are the movement cost and the
intrinsic measurement cost of the $i$th measurement, correspondingly.

Driven by this estimate, the blinkered scheme selects a single
measurement of the item where {\em some} number of measurements gains
the greatest value of information.  A single step is expected to be
just the first one in the right direction, rather than to actually
achieve the value. Thus, the estimate relaxes the {\it single-step}
assumption, while still underestimating the value of information.

For budget $C$ and a single measurement cost bounded from below
 by $c$ the time to compute the estimate $T_{BVI}$ is:
$T_{BVI}=O\left(T_{MVI}\frac C c\right)$. This time bound is still
exponential in the representation, but can be made polynomial in one of
the following ways:

\begin{itemize}
\item If $MVI^k$ is a unimodal function of $k$, which can be shown for some
forms of distributions and utility functions,
the time is logarithmic in the budget $C$. Indeed, using the
bisection method to find $k$ for which $MVI^{k+1}-MVI^k$ changes sign
yields the logarithmic time.
\item Otherwise, $BVI$ can be estimated as the maximum for powers of
  2: $k=2^{k'}$.
\end{itemize}

In the presence of inexact recurring measurements, the blinkered
scheme plays a role similar to the role of the myopic scheme for exact
measurements. It seems to be the simplest approximation that still
works for a wide range of conditions under realistic assumptions.

% \input{thr-utility-shape}

\section{Theoretical Bounds}

Bounds on the blinkered scheme are established for two special cases.
The first bound shows that the scheme does not stop measuring too early.

\begin{thm}
\label{th:bound-single} 
Let $S=\{ s_1, s_2\} $, where the value of $s_1$ is known exactly. Let
$C_b$ be the remaining budget of measurements when the blinkered
scheme terminates. Then the (exact) value of information of an optimal
policy from this point onwards is at most $C_b$.
\end{thm}

The second bound is related to a common case of a finite budget and
free measurements. The bound provides certain performance guarantees
for the case when dependencies between items are sufficiently weak,
and shows that the blinkered scheme chooses reasonable search
directions under weaker assumptions than those for the myopic scheme.

\begin{dfn}
\label{dfn:mutually-subadditive}
Measurements of two items $s_1$, $s_2$ are mutually subadditive if,
given sets of measurements of each item $Q_1$ and $Q_2$, the intrinsic
value of information of the union of the sets is not greater than the
sum of the intrinsic values of each of the sets, i.e.: $\Lambda(Q_1
\cup Q_2) \le \Lambda(Q_1) + \Lambda(Q_2)$
\end{dfn}

\begin{thm}
\label{th:bound-multiple}
For a set of $N_s$ items, the zero measurement cost $c=0$, and a
finite budget, if measurements of every two items are mutually
subadditive, the value of information of the blinkered scheme is no
more than a factor of $N_s$ worse than the value of information of an
optimal algorithm.
\end {thm}

Proofs for the bounds are provided in Appendix~\ref{app:proofs}.
Both bounds are asymptotically tight, as can be shown by constructing
appropriate problem instances.
% appropriate problem instances
% are defined in Appendix~\ref{app:tightness}.
