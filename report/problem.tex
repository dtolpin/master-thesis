
% $Id: problem.tex,v 1.1 2009/11/29 14:33:01 dvd Exp $

The selection problem addressed in this paper is formally defined as follows.

Given:
\begin{itemize}
\item A set of items $S=\{s_1, s_2, \ldots\}$.
\item A set of $N_f$ item features $Z=\{ z_1, z_2, \ldots, z_{N_f}\}$. (Each feature
$z_i$ has a domain $\mathcal{D}(z_i)$.)
\item A joint distribution $B_0$ over the features of the items in $S$. That is,
a joint distribution over the random
variables $\{ z_1(s_1), z_2(s_1),\ldots , z_1(s_2), z_2(s_2),\ldots\} $.
\item A set of measurement types $M=\{(c^m, p^m)_k\:\vline\; k \in
  1..N_m\}$, with potentially different intrinsic measurement cost
  $c^m$ and observation distribution $p^m$, conditional on the true
  feature values, for each measurement type.
\item A known utility function $u(\textbf{z})\colon\mathbb{R}^{N_f}\to
  \mathbb{R}$ on features.
%\footnote{In the simplest case, there is just one real-valued
%feature, acting as the item's utility value, and $u$ is thus simply the identity function,
%see below.}
\item A ``movement cost'' function
  $d(s_i,s_j)\colon S\times S\to\mathbb{R}$ on pairs of items.
\item A measurement budget $C$.
\end{itemize}

Find a policy of measurement decisions and final selection
(in the POMDP sense of a mapping from belief states into actions)
that maximize the expectation of the following reward:
\begin{eqnarray}
\label{eq:problem-reward}
\mbox{max:}&&W=u(\textbf{z}(s_\alpha))
              -\sum_{i=1}^{N_q}\left(c^m_{k_i}
                +d\left(s_{i-1},s_i\right)\right)\nonumber\\
\mbox{s.t.:}&&\sum_{i=1}^{N_q}\left(c^m_{k_i}
                +d\left(s_{i-1},s_i\right)\right)\le C
\end{eqnarray}
where $Q=\{q_i=(k_i,s_i)\:\vline\;i \in 1..N_q\}$ is the performed
measurement sequence, where $k_i$ is the type, and $s_i$ is the
location of measurement $q_i$, and $s_\alpha$ is the selected item.
The expectation is taken according to the distribution $B_0$, which
acts as the initial belief in the POMDP sense, updated according to
information gathered by the measurements, if any.

There are several possible settings of the problem depending on the
way information obtained from measurements is treated.
This paper focuses on the {\em online measurement selection}
\cite{Rish.efficient} problem: a next measurement is selected after the
outcomes of all preceding measurements are known. An alternative
problem is {\em offline measurement subsequence selection}, in which
the policy is just a pre-determined sequence of measurements,
without taking outcomes of individual measurements into account.

The above problem statement is still too general to handle directly.
In typical settings of this selection problem, additional structure is
usually imposed as follows.

{\bf Parameters:} The set of items $S$ can be indexed according to a set
  of parameters $I$.  The parameter values of an item
  $s\in S$  are denoted by $I(s)$.  In the simplest case $I$ is just an integer
  index. Alternatively, $I$ can be a set of coordinates, and the
  items $S$ are indexed by points on a multi-dimensional grid.  The
  latter case provides an approach to approximate optimization of an
  unknown continuous function.

{\bf Distance metric:} When $I$ is a set of coordinates,
  one common set of cost functions is where $d$ is a function only of
  a distance metric over the item coordinates, i.e. $d(s_1, s_2) =
  f(||I(s_1), I(s_2)||)$ for some function $f$ and some metric
  $||\cdot||$. This would be the type of cost encurred by having to physically
  move sensing equipment from location to location.

{\bf Distribution:} The joint distribution over the feature space is
  also structured in many cases, with variables assumed
  independent except where indicated by a structured probability model
  such as a Bayes network, a Markov network, or a Markov random
  field. For example, when $I$ is a multi-dimentional grid, the
  dependency between features of the items is typically modeled as a
  Markov random field with a neighborhood determined according to the
  grid---only (features of) items $s^\prime$ that are neighbors on the grid of
  $s$ are in the Markov neighborhood of (features of) $s$.

The following examples can be viewed as instances of the selection problem:

{\bf Basic case:} In this variant there is a finite number of items, and only
one real-valued feature. The utility function is a simple monotonically
increasing function of the feature values, such as the identity function.
The features (of the different items) are mutually independent, the movement cost is zero,
and the budget is infinite. This simple setting is still hard to solve, and resembles
the well-known multi-armed bandit problem (see Section \ref{sec:related}).
Closed-form solutions are possible for very specific sub-cases, such as the example in
Section \ref{sec:greedy-mvi-shortcomings}.


{\bf Water reservoir monitoring:} A water reservoir is monitored for
contamination sources. Water probes can be taken in a number of
predefined spots, and the goal is to predict the location and
intensity of a contamination source based on analysis of water
quality. The contamination must be identified quickly, before it
distributes too far or affects the consumers. Here, the {\em features}
are concentrations of possible contaminants, the {\em utility
  function} is a function of the concentrations; the {\em measurement
  cost} is determined by the time required to perform the analysis of
a probe, and, in case of a large natural reservoir, the time and cost
to move the probe physically between locations.

{\bf SVM parameter optimization:} Classification quality of a support
vector machine depends on one or more parameters (see Section
\ref{sec:emp-case-svm} for a case study). While there are heuristics
for selecting good parameter values for particular kernel types and
data sets, several combinations of parameters must be tried before a
good one can be chosen. The classifier accuracy is the only {\em
  feature}, and the {\em utility  function} is a monotonicically increasing function
of accuracy; the classifier parameters define {\em coordinates} of the items in a
multi-dimensional grid, with distance-based {\em probabilistic dependencies}
between the items. The {\em measurement cost} is the time required to
train and validate the classifier for a combination of
parameters. Movement cost for this problem is usually negligible.

{\bf Metrology machine setup:} A focus and a filter color must be
chosen for optimal setup of metrology equipment (see Section
\ref{sec:emp-case-mms}). For each combination of the focus and the
color, the {\em item coordinates}, a number of {\em features} can be
observed inexactly, and the {\em utility function} depends in a rather
complicated way on the feature values. Both the measurements and the
equipment movements take time and determine the {\em measurement} and
{\em movement costs}, and the setup must be completed within a given {\em
time budget}.

