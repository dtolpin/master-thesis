% $Id: intro.tex,v 1.1 2009/11/29 14:33:01 dvd Exp $

Optimization under uncertainty  is a domain with
numerous important applications \cite{Rish.adaptive}, \cite{Krause.water}.
Problems of optimization under uncertainty are intractable in general,
and thus special cases are of interest. In this paper, the following selection
problem is examined: given a set of items of unknown utility (but a
distribution of which is known), an item with as high a utility as
possible must be selected.  Measurements (possibly noisy) of item
features are allowed prior to selection, at known costs. The objective
is to optimize the overall decision process of measurement and
selection. Even with the severe restrictions imposed by the above
setting, this decision problem is intractable \cite{RadovilskyOSS},
and yet it is important to be able to find at least an approximate
solution due its importance for potential applications such as sensor
network planning and oil exploration.

Other settings where this problem is applicable are: considering
which time-consuming deliberation steps to perform
(metareasoning \cite{Russell.right}) before selecting an action,
locating a point of high temperature using a limited number of
measurements (with dependencies between locations as in
\cite{Guestrin.graphical}), and the problem of finding a good set
of parameters for setting up an industrial imaging system.  The latter
problem is actually the original motivation for this research, and is
discussed in Section~\ref{sec:emp-case-mms}.

A widely adopted scheme for selecting measurements, also called
sensing actions in some contexts (or deliberation steps, in the
context of metareasoning), is based on value of information
(VOI) \cite{Russell.right}.  Computing value of information is
intractable in general, thus both researchers and practitioners often
use various forms of myopic VOI estimates \cite{Russell.right} coupled
with greedy search (see Section \ref{sec:greedy}).
Even when not based on solid theoretical
guarantees, such estimates lead to practically efficient solutions in
many cases.

However, in a selection problem involving items with real-valued
utilities (the main focus of this paper), coupled with the capability
of the system to perform more than one measurement for each item, the
myopic VOI estimate can be shown to badly underestimate the value of
information (Section \ref{sec:greedy-mvi-shortcomings}). This can lead
to inferior measurement policies, due to the fact that in many cases
either no measurement is seen to have a VOI estimate greater than its
cost, or measurements with high true VOI are ignored, due to the
myopic approximation.  The goal is to find a scheme that, while still
efficiently computable, can overcome these limitations of myopic
VOI. The framework of semi-myopic VOI is proposed
(Section \ref{sec:thr}), which includes the myopic VOI as the simplest
special case, but also much more general schemes, including exhaustive
subset selection at the other extreme.  Within this framework the
``blinkered'' VOI estimate is introduced, as one that is efficiently
computable and yet performs much better than myopic VOI for the
selection problem. In indicative special cases we get theoretical
bounds that show the benefit of the blinkered estimate
(Section \ref{sec:thr-blinkered-estimate}).  Empirical results on
artificial and real-world data further support using the blinkered
scheme (Section \ref{sec:emp}).
