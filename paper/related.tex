% $Id: related.tex,v 1.22 2011/09/07 12:15:46 dvd Exp $

We begin by discussing papers that introduce techniques used in this
paper, followed by work on related problems.

\subsection{Limited rationality}

Limited rationality, a model of deliberation based on value of utility
revision and deliberation time cost was introduced in
\cite{Russell.right}.  Notions of value of
computation and its estimate were defined, as well as the class of meta-greedy
algorithms and simplifying assumptions under which the algorithms
are applicable. The theory of bounded optimality, on which the
approach is based, is further developed in \cite{Russell.bounded}.
\cite{Zilberstein.PHD} employs limited rationality techniques to analyze
any-time algorithms and proves optimality of myopic algorithm
monitoring under assumptions about the class of value and time cost
functions. Techniques and assumptions used in this domain, such
as commonly used myopic assumptions are very relevant to our work
(which generalizes the conditions of the myopic assumption),
and are further examined in Section \ref{sec:bg-limited}.

\subsection{Optimization under uncertainty}

Various uncertainty modeling philosophies exist \cite{Sahinidis2004}. The Bayesian framework
\cite{CaramanisMannor.bayesian}, which is assumed in this paper,
is an extension of stochastic programming in which the uncertainty
model incorporates prior beliefs and is then updated based on observed
data using a Bayesian step. \cite{Artstein.information} offer
a value-of-information based framework for optimization under
stochastic uncertainty.  Methods of sequential parameter optimization under
uncertainty are reviewed and 
enhanced in \cite{Leyton.paramils}. \cite{Leyton.tbo}
analyze runtime of some algorithms for sequential optimization
and proposes a method for achieving a compromise between the runtime
and the optimization quality. 

Numerous sequential decision problems of optimization under 
uncertainty (within the Bayesian framework)
can be cast in terms of Partially Observable Markov Decision
Processes (POMDP) \cite{Kaelbling.POMDP}.
In the selection problems considered in this paper, observations are achieved by
making measurements, each at a known cost. The cost in some cases depends on
the previous measurement, as defined formally in Section \ref{sec:problem}.
The entire process, of selecting which measurements to perform (the selection
possibly depending on observations
obtained due to previous measurements) and
then finally settling on a choice of item
is a sequential stochastic decision process. It is therefore not surprising
that our selection problem can be stated in terms of
POMDPs. Unfortunately, despite the fact that POMDP is a well-studied
model with numerous (usually approximate) solution algorithms 
available \cite{Cassandra.POMDP}\cite{Shani.POMDP},
POMDP is a highly intractable problem \cite{Cassandra.POMDP}, 
with complexity typically exponential in the number of states of the system.
Since in this case the number of states is itself 
exponential in the number of items among which we
are trying to select, traditional POMDP solution 
methods seem highly impractical for the selection problem. 
Thus, the relation to the more general
POMDPs is not further examined in this paper.


\subsection{Optimization of submodular functions on sets}

\cite{Guestrin.submodular} consider greedy algorithms for offline
observation selection based on value of information. The paper shows
that many natural selection objective functions are submodular. A
function $f\colon 2^S \to R$ on a finite set $S$ is submodular if for
any $A \subset B \subset S$ and $x \in S$, $(A \cup \{x\})-f(A) \geq
f(B \cup \{x\})-f(B)$. When the objective function is submodular, the
algorithms are nearly optimal. The authors describe applications of
the algorithms to various problems, such as fault diagnosis, robotic
explorations, minimizing human attention and
others. \cite{Liao.sensor} describe improved versions of the
algorithms which exploit the structure of dependencies between
observation locations and make use of strongly polynomial algorithms
for submodular function optimization. \cite{Guestrin.graphical}
introduce an optimal algorithm for observation selection for a
restricted case of chain graphical models and compare it to the greedy
algorithm.

The greedy algorithm is also used for online measurement selection
\cite{Rish.efficient}, when value of information of a measurement
depends on outcomes of earlier ones. \cite{Guestrin.submodular}
mention that when the objective function is approximately submodular,
the performance guarantees might be adapted for the online setting. 
However, in many cases, including the case of recurring
noisy measurements investigated here, value of information
is not submodular, and the greedy algorithm that tries to maximize value of
information may perform arbitrarily badly.

\subsection{Non-myopic algorithms for online selection}

Several researchers consider non-myopic online selection of
measurements in the cases where the submodularity restriction does not
hold. The proposed approaches are better than the greedy myopic
algorithm, but are restricted to cases of exact
measurements. Additionally, the proposed improved algorithms often rely on
a particular structure of dependencies between measurement outcomes,
such as dependencies represented by trees or sparse acyclic graphs,
and are evaluated on cases with a small number (less than a hundred)
of different measurements available. In problems of optimization under
uncertainty of continuous functions, thousands of different
measurements can be required to achieve sufficient accuracy, and the
dependency structure can be rather dense. 
\cite{Guestrin.graphical} introduce an optimal algorithm for computing
conditional plans on chain graphical models for online selection of
measurement of discrete random variables, and prove that computing
conditional plans is $\mathrm{NP}^\mathrm{PP}$ hard even for discrete
polytrees.

In \cite{Heckerman.nonmyopic}, a case of discrete Bayesian networks
with a single decision node is analyzed. The authors propose to
consider subsequences of observations in the descending order of their
myopic VOI estimates. If any such subsequence has non-negative value
estimate, then the computation with the greatest myopic estimate is
chosen.  However, this approach always chooses a measurement for the
myopically best item, and when applied to the selection problem either
looks at sequences of measurements on a single item with the greatest
myopic VOI estimate, or, if sequences with one measurement per item
are considered, may fail to provide an improvement over the myopic
estimate even for simple cases. Still, in many cases their scheme shows
an improvement in performance. \cite{Liao.nonmyopic} describes and
experimentally analyzes an algorithm for influence diagrams based on a
non-myopic VOI estimate.

\cite{BilgicGetoor.voila} addresses the problem of efficient feature
value acquisition in the presence of varying acquisition costs. A new
structure, {\em Value~of~Information Lattice}, and an algorithm to
build the structure, are introduced in the paper. The paper compares
different acquisition strategies: greedy acquisition, acquisition in
sets and a mixed strategy, according to which the most valuable
feature in the most valuable set is selected and shows how the mixed
strategy can be implemented efficiently using the introduced
structure. However, the method is still intractable in the general
case, the features are assumed to be discrete and exactly measurable,
and the number of features to be relatively small (the largest set in
the empirical evaluation consisted of 20 features).

\subsection{Related Problems}

\subsubsection{Informative path planning} 

None-zero movement costs are considered in problems of informative
path planning, the reward depends on the path between the information
gathering locations as well as on the obtained information. The
problem is a special case of {\it traveling salesman problem with
profits}; a classification of TSPs is provided and the literature is
surveyed in \cite{Feillet.tsp}. \cite{Meliou.path} present and
efficient algorithm for planning of data collection tours based on the
structure of the distance graph and submodularity of the objective
function.

\subsubsection{Multi-armed bandits} 

The well-known Multi-armed bandit problem \cite{Vermorel.bandits} is
based on the analogy with traditional slot
machines. When pulled, each lever (``arm'')
provides a reward drawn from a distribution associated with that
specific lever. The objective of the gambler is to maximize the
collected reward sum through iterative pulls. 
These bandit problems bear similarity to the
measurement selection problem, especially when the reward
distribution is continuous and unknown. 
Some of the algorithms,
e.g. POKER (Price of Knowledge and Estimated Reward)
\cite{Vermorel.bandits} employ the notion of value of
information. In certain settings, dependencies between different arms
are considered and used to maximize the reward
\cite{Kleinberg.metric}. \cite{Guha.metric} study the case of
switching costs which depend on distance between the
arms. \cite{Bubeck.purebandits} provide theoretical analysis of pure
exploration in the multi-armed bandit problem, in which the
expected reward of the selected arm is maximized.

Bandit problems  differ from
the single-choice optimization problem in that the reward accumulates
in the process of the search, and a compromise must be found between
exploration (looking for the best lever) and exploitation (pulling the
lever that brings the highest benefit). Additionally, many solutions
concentrate on particular features of the value function, such as
linear dependence of reward from pushing a lever on the time left, and
do not facilitate generalization. 
%On the other hand, achievements in
%limited rationality techniques should be helpful in development of
%improved solutions in this domain. 
 

\subsubsection{Selective sampling of Gaussian processes}

\cite{GramacyLee.treegp} explores a methodology for selective sampling
of non-stationary Gaussian processes. By using
the Bayesian approach to measure predictive uncertainty and guide
sampling, an accurate model of the process can be discovered at a
lower cost. The objective of the method is  to
simulate a complex process over the whole field, rather than to optimize a
combination of parameters as in our selection problem. In the problem
of optimization under uncertainty, an accurate model of areas with low
utility is unnecessary, and measurements which do not affect the
selection of the best combination of parameters are wasted.
