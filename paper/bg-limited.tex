% $Id: bg-limited.tex,v 1.26 2011/09/07 12:15:45 dvd Exp $

\subsection{Limited Rationality}
\label{sec:bg-limited}

Finding an approximate solution to a counterpart of the
selection problem appears in past research under the guise of 
{\em Rational metareasoning} \cite{Russell.right}.
In this setting, an agent can perform base-level
actions from a known set $\{A_i\}$. Before committing to each
action, the agent deliberates by performing  a sequence of
meta-level ``deliberation'' actions from a set $\{M_j\}$. At any given
time there is a base-level action, $A_\alpha$, that appears
(in a decision-theoretic sense) best to
the agent. The goal of subsequent meta-level actions is to improve the
choice of $A_\alpha$. The current best action $A_\alpha$ is one which maximizes the agent's expected
utility:
\begin{eqnarray}
\label{eq:bg-limited-eu}
\IE_P[U(A_i)]&=&\sum_k P(W_k) U(A_i,W_k)\\
\alpha&=&\arg \max_i \IE_P[U(A_i)]
\end{eqnarray}
where $\{W_k\}$ is the set of possible world states, and $P(W_k)$ is
the probability that the current world state is $W_k$.

A meta-level action affects the choice of
the base-level action $A_\alpha$. The {\em value} of a meta-level
action is measured by the resulting increase in the utility of
$A_\alpha$. Since neither the outcomes of meta-level actions nor the
true utility of $A_\alpha$ are known in advance, a meta-level action
is selected according to its expected influence on the expected
utility of $A_\alpha$. The {\em value of information} of a meta-level
action $M_j$ is the expected difference between the expected utility
of $M_j$ and the expected utility of the current $A_\alpha$, where $P$
is the current belief distribution (about the state of the world), and $P^j$
is the belief distribution after the
computational action $M_j$ is performed, given the outcome of the action:
\begin{equation}
\label{eq:bg-limited-nv}
V(M_j)=\IE_P(\IE_{P^j}(U(M_j))-\IE_{P^j}(U(A_\alpha)))
\end{equation}

Under certain assumptions, it is possible to capture the dependence of utility on
time in a separate notion of {\em time cost} $TC$. Then, the utility of
an action $A_i$ taken after a meta-level action $M_j$ is the utility
of $A_i$ taken now less the cost of time for performing $M_j$:
\begin{equation}
\label{eq:bg-limited-iu}
U(A_i, M_j) = U(A_i) - TC(A_i, M_j)
\end{equation}

It is customary to call the current utility of a future base-level
action its {\em intrinsic utility}. The separation into intrinsic
utility and time cost allows for time-independent estimation of the utility of a base-level
action. Varying time-pressure is then easily incorporated into
the true final utility using $TC$.

When the time cost $TC$ depends only on the meta-level action $M_j$,
(\ref{eq:bg-limited-nv}) can be rewritten with the cost and the
intrinsic value of information of $M_j$ as separate terms.
\begin{equation}
\label{eq:bg-limited-v=bc}
V(M_j)=\Lambda(M_j)-TC(M_j)
\end{equation}
where {\em intrinsic value of information}
\begin{equation}
\label{eq:bg-limited-benefit}
\Lambda(M_j)=\IE_P\left(\IE_{P^j}(U(A_\alpha^j))-\IE_{P^j}(U(A_\alpha))\right)
\end{equation}
is the expected difference between the intrinsic expected utilities of the new
and the old selected base-level action, computed after the meta-level
action was taken.

Perfect meta-level rationality cannot be achieved; the simplest
approximation of the utility of a meta-level action is based on the
following assumptions \cite{Russell.right}:

{\bf Meta-greedy assumption:} Only sequences consisting of a single
  meta-level action are considered. The meta-level policy
  commits to the best single action and therefore is greedy. 

{\bf Single-step assumption:} The utility of a meta-level action is
  determined only by its immediate effect on the choice of a
  base-level action. VOI of a meta-level action is computed under the
  assumption that a base-level action is chosen immediately after
  performing the meta-level action.

The value of information estimate and the greedy algorithm which
follow from these assumptions are known as {\em myopic}
\cite{Russell.right}. The myopic value of information estimate $MVI$
need not be restricted to single atomic actions; multiple atomic actions
performed in a batch, without intermediate decisions, can be
considered a single meta-level action. For a batch of $n$ meta-level
actions $B=M_{j_1}\ldots M_{j_n}$ the myopic estimate is:
\begin{equation}
MVI_B\!=\!\IE_P\left(\IE_{P^B}(U(A_\alpha^B))-\IE_{P^B}(U(A_\alpha))\right)-\sum_{k=1}^n\!TC(M_{j_k})
\label{eq:bg-limited-mvi}
\end{equation}
where expectation $\IE_{P^B}$ is computed according to the
immediate posterior belief distributions. When all of the actions
in the batch are identical, notation $MVI_j^n$ will be used, denoting
the myopic estimate of a batch of $n$ actions $M_j$.

Theoretical bounds of the myopic estimate are proved for some
restricted cases (\cite{Guestrin.submodular}). In many applications,
experiments show that the assumptions work well. When the assumptions
turn out to be unjustified, better approximations can often be
designed by extending or replacing either the meta-greediness or the
single-step assumption.
