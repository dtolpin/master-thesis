% $Id: thr-blinkered-estimate.tex,v 1.48 2011/09/15 10:39:47 dvd Exp $

\subsection{Semi-Myopic Schemes}
\label{sec:thr-semi-myopic-schemes}

Keeping the complexity manageable while
overcoming the limitations of the myopic algorithm
is the basis for the {\em semi-myopic} framework.
One might consider finding an optimal measurement policy, but
the number of possible measurement plans,
even for discrete variables, is super-exponential
(and uncountably infinite for continuous variables), which 
makes this approach infeasible.
In the semi-myopic schemes we essentially assume (only for the sake of
estimating VOI), that we need to select
measurements offline, without observing the results of previous
observations. This makes the (simplified, but incomplete) search space ``only''
exponential in the general case, and polynomial in important,
useful special cases.

Let ${\mathcal M}$ be the set of all possible measurement
actions, and ${\mathcal C}$ be a constraint over sets of
measurements from ${\mathcal M}$. In the semi-myopic framework,
all possible subsets (batches) ${\mathcal B}$ of measurements from ${\mathcal M}$
that obey constraint ${\mathcal C}$ are considered: for each
such subset ${\mathcal B}$ a `batch' value of information estimate is computed
under the assumption that all measurements in ${\mathcal B}$ are made, 
followed by
a decision (selection of an item). Then, the batch
${\mathcal B}^*$ with the best value estimate is chosen. Once 
${\mathcal B}^*$ is chosen, there are still several options
(\cite{BilgicGetoor.voila} discusses a similar approach to discrete
feature acquisition):
\begin{enumerate}
\item Actually do all the measurements in ${\mathcal B}^*$.
\item Attempt to optimize ${\mathcal B}^*$ into some form of
  conditional plan of measurements.
\item Perform (for now) only the best measurement in ${\mathcal B}^*$.
\end{enumerate}

In all cases, after measurements are made, the selection of measurements is repeated,
until no batch has a positive value, at which point the algorithm {\em
  terminates} and selects an item. Option 1, a combination of online
and offline selection, misses the possibility to choose better
measurements while executing a batch, and therefore is inferior to
options 2 and 3. While limited efficient implementation for option 2 is
possible, optimizing a conditional plan is intractable in general,
recreating the optimal policy problem on a smaller scale.  
Option 3, allows better use of information from
the outcome of previous measurements, and remains tractable.

% In the semi-myopic version, the measurement selection part
% (lines~\ref{alg:greedy-select-start}--\ref{alg:greedy-select-end})
% of the algorithm in Figure~\ref{alg:greedy-algorithm} is
% replaced with the algorithm in Figure~\ref{alg:semi-myopic-scheme}.

The semi-myopic version of the greedy algorithm is presented in
Figure~\ref{alg:semi-myopic-scheme}. 
\begin{figure}
\begin{algorithmic}[1]
\STATE $budget \leftarrow C$
\STATE Initialize joint distribution $P$ to $P_0$ \label{alg:semi-myopic-initialize-beliefs}
\LOOP                        \label{alg:semi-myopic-main-loop-start}
  \STATE {\bf for all} items $s_i$ {\bf do} Compute $\IE_P(U_i)$ \label{alg:semi-myopic-compute-eu}
  \FORALL {batches $b_j$ satisfying constraint $\mathcal{C}$}  \label{alg:semi-myopic-select-start}\label{alg:semi-myopic-compute-batch-start}
    \IF {$cost(b_j) \ge budget$}
      \STATE Compute $V_j^b$
    \ELSE
      \STATE $V_j^b \leftarrow 0$ \label{alg:semi-myopic-batch}
    \ENDIF
  \ENDFOR                         \label{alg:semi-myopic-compute-batch-end}
  \STATE $j_{max} \leftarrow \arg \max\limits_j V_j^b$
  \IF {$V_{j_{max}}^b>0$}
    \STATE {\bf for all} {measurements $m_k \in b_{j_{max}}$} {\bf do} Compute $V_k$  \label{alg:semi-myopic-voi}
    \STATE $k_{max} \leftarrow \arg \max\limits_k V_k$
    \STATE Perform measurement $m_{k_{max}}$ \label{alg:semi-myopic-measure}
    \STATE Update joint distribution $P$ \label{alg:semi-myopic-update-beliefs}
    \STATE $budget \leftarrow budget-c_{k_{max}}$  \label{alg:semi-myopic-select-end}
  \ENDIF
  \STATE {\bf else break} \label{alg:semi-myopic-break}
\ENDLOOP                \label{alg:semi-myopic-main-loop-end}
\STATE $\alpha \leftarrow \arg \max \IE_P(U_i)$ \label{alg:semi-myopic-return-alpha}
\RETURN $s_\alpha$
\end{algorithmic}
\caption{Semi-myopic Scheme}
\label{alg:semi-myopic-scheme}
\end{figure}
The value of information is computed twice: first, value of
information $V_j^b$ of every batch $b_j$ satisfying the constraint
$\mathcal{C}$ is computed (line~\ref{alg:semi-myopic-batch});
then, if the maximum value of information of a batch is positive,
value of information $V_k$ of every single measurement $m_k$ from
the batch $b_{j_{max}}$ is computed
(line~\ref{alg:semi-myopic-voi}), and a measurement with the highest
net value of information (even if negative) is performed.

The meta-reasoning approach (Section \ref{sec:bg-limited}) requires
that computation time be small compared to the measurement
time. Assuming that the value of information of a set of measurements 
and the expected utility of an item can be computed in constant times
$T_V$ and $T_{EU}$ respectively, the computation time  $T_{comp}$ of a single
step of the algorithm is the sum of:

\begin{itemize}
\item the time $T_{eu}$ to compute utilities of all items
  (line~\ref{alg:semi-myopic-compute-eu}),
  $T_{eu}=O(T_UN_s)$;
\item the time $T_{batch}$ to compute values of information of all batches
  according the constraint $\mathcal{C}$
  (lines~\ref{alg:semi-myopic-select-start}--\ref{alg:semi-myopic-select-end}),
  $O(T_V|\mathcal{C}(N_s, N_m)|)$;
\item the time $T_{voi}$ to compute values of information of every single
  measurement in the best batch
  (line~\ref{alg:semi-myopic-voi}),
  $O(T_VN_sN_M)$ --- a single batch may contain all types of
  measurements for each of the items;
\item Assuming the time $T_{bel}$ to update the joint distribution
  (line~\ref{alg:semi-myopic-update-beliefs}) is constant and
  sufficiently small (using approximate inference
  algorithms), denote $T_{bel}$ by $T_B$.
\end{itemize}
\begin{equation}
  \label{eq:thr-computation-time}
  \hspace{-8em}T_{comp}=T_{eu}+T_{batch}+T_{voi}+T_{bel}
\end{equation}
\vspace{-2em}
\begin{equation*}
\hspace{2em}=O(T_UN_s)+O(T_V|\mathcal{C}(N_s, N_m)|)+O(T_VN_sN_M)+T_B\nonumber
\end{equation*}

$T_{batch}$ is the greatest term in $T_{comp}$ and determines the
computational time complexity of the algorithm according to a
semimyopic {\em scheme} defined by the constraint $\mathcal{C}$.  For
the empty constraint --- the {\em exhaustive} scheme --- all possible
measurement sets are considered; this scheme has an exponential
computation time, while {\em still} not guaranteeing optimality (finding an
\emph{optimal} solution requires examination of all \emph{conditional plans}).
Nevertheless, this scheme is a good yardstick against which
to compare other semi-myopic schemes, as done in Section \ref{sec:emp},
as it is unreasonable to expect {\em any} semi-myopic scheme to
achieve a result better than the exhaustive scheme in terms of the objective
function (costs of measurements and utility of selected item).

At the other extreme is the constraint where only singleton
sets are allowed. This extreme results in the greedy single-step
assumption --- the original myopic scheme, which is the
current state of the art for the general selection problem.
\cite{BilgicGetoor.voila} proposes a constraint based 
on the {\em value of information lattice},
a data structure introduced in that paper. The induced number of
subsets is still exponential in a general case, but for certain kinds
of dependency structure the constraint gives significant
improvement. Yet another---{\em omni-myopic}---constraint can be
constructed along the lines of \cite{Heckerman.nonmyopic}: the
measurements are ordered according to their myopic VOI estimates, and
subsets of measurements with greatest VOI estimates are considered.

This paper suggests the constraint that restricts batches to
repeated identical measurements---the {\em blinkered} scheme. This
scheme is tractable, corresponds to a common approach in which noisy
experiments are repeated to increase confidence, and demonstrates
improved efficiency in empirical evaluations.

\subsection{Blinkered Estimate}
\label{sec:thr-blinkered-estimate}

As stated above, the blinkered scheme considers sets of independent identical
measurements; this constitutes unlimited lookahead, but along a single
``direction'', as if one ``had one's blinkers on''.  Although this
scheme has a computational overhead over the myopic one, the factor is
only linear in the budget. \footnote{Assuming either
  normal distributions, or some other distribution 
  where measurement and belief updating can be done, or at least
  approximated, efficiently.
  For general distributions, sets of observations may
  provide information beyond the mean and variance, and the resources
  required to compute value of information may even be exponential in
  the number of measurements.}  The ``blinkered'' value of information
is defined as:
\begin{eqnarray}
\label{eq:thr-bvi}
BVI_j\hspace{-0.8em}&=&\hspace{-0.8em}\max_n MVI_j^n\\
\mathrm{s.t.:\;}c_j\hspace{-0.8em}&=&\hspace{-0.8em}d(s_{last}, s_{i_j})+d(s_{i_j}, s_{i_j})(n-1)+c^m_{k_j}\leq C\nonumber
\end{eqnarray} 
where, as before, the $j$th measurement is of the $k_j$th type and
measures the $i_j$th item, $c_j$ is the net measurement cost, and
$s_{last}$ is the last measured item. 

Driven by this estimate, the blinkered scheme selects a single
measurement of the item where {\em some} number of measurements gains
the greatest value of information.  A single step is expected to be
just the first one in the right direction, rather than to actually
achieve the value. Thus, the estimate relaxes the {\it single-step}
assumption, while still underestimating the value of information.

For the blinkered scheme, $T_{batch}$ depends on the time
$T_{BVI}$ to compute the blinkered estimate for measuring an item:
\begin{equation}
\label{eq:thr-time-batch-blinkered}
T_{batch}=O(N_sN_mT_{BVI})
\end{equation}
For budget $C$ and a single measurement cost bounded from below
 by $c$, the time to compute the estimate $T_{BVI}$ is:
$T_{BVI}=O\left(T_{MVI}\frac C c\right)$.  This time bound (linear in the budget)
%still
%exponential in the representation 
is prohibitive for large budgets; but can be decreased in one of
the following ways:

\begin{itemize}
\item If $MVI^n$ is a unimodal function of $n$, which can be shown for some
  forms of distributions and utility functions,
  the time is logarithmic in the budget $C$. Indeed, using the
  bisection method to find $n$ for which $MVI^{n+1}-MVI^n$ changes sign
  yields the logarithmic time. $MVI^n$ is unimodal when, for example,
  the intrinsic value of information is represented by a translated S-curve
  shaped function of $n$, such as the logistic function or the sigmoid
  function, and the measurement cost is linearly proportional to
  $n$. S-curves are frequently met in learning \cite{Russell.aima},
  mathematical modeling \cite{Gershenfeld.modeling} and economics
  \cite{Johansson.s-curve}. $\Lambda(n)$ in the example in
  Section~\ref{sec:greedy-mvi-shortcomings} has the shape of an S-curve, and $MVI^n$
   is unimodal. 
\item $BVI$ can be computed for a bounded horizon $H$, such that
  $T_{BVI}=O\left(T_{MVI} \frac {\min (C, H)} c\right)$, and the
  choice of $H$ determines tradeoff between the measurement selection
  quality and the computation time.
\item $BVI$ can be estimated as the maximum for powers of
  2, $n=2^{n'}$: instead of selecting the maximum VOI for batches of
  1, 2, 3,~\ldots measurements, batches of 1, 2, 4, 8, 16,~\ldots
  measurements are considered.  This approach has been taken  in the implementation of
  the blinkered scheme used for the empirical evaluation (Section
  \ref{sec:emp}). Although $BVI$ is underestimated, the results of
  the blinkered scheme are still much better than of the myopic
  scheme.
\end{itemize}

In the presence of inexact recurring measurements, the blinkered
scheme plays a role similar to the role of the myopic scheme for exact
measurements. It seems to be the simplest approximation that still
works for a wide range of conditions under realistic assumptions.

% \input{thr-utility-shape}

\subsection{Theoretical Bounds}

Bounds on the blinkered scheme are established for two special cases.
The first result bounds the loss resulting from premature termination
of the blinkered scheme.

\begin{thm}
\label{th:bound-single} 
Let $S=\{ s_1, s_2\} $, where the value of $s_1$ is known exactly. Let
$C_b$ be the remaining measurement budget when the blinkered
scheme terminates. Then the value of information of an optimal
policy from this point onwards is at most $C_b$.
\end{thm}

\begin{IEEEproof}
The intrinsic value of information over the remaining budget when the
blinkered scheme terminates $\Lambda_b$ is at most the remaining
budget $C_b$, since otherwise the scheme would not have terminated. There is
only one kind of measurement, thus the intrinsic value of information
$\Lambda_o$ achieved by an optimal policy is at most equal to that of
all measurements, $\Lambda_o \leq \Lambda_b \leq C_b$.  Since
measurement costs are positive, the value of information $V_o$ of an
optimal policy must therefore also be at most $C_b$.
\end {IEEEproof}

Indeed, the remaining budget is an heuristic used to terminate a search that may
take too much time. Since the total cost of measurements is
subtracted from the intrinsic utility of the result to obtain the
reward (Equation \ref{eq:problem-reward}) and does not exceed the budget, the
remaining budget is normally smaller than the anticipated increase in
the intrinsic utility. On the other hand, the regret---the
difference between the utility of the best item and the utility of the
selected item less the measurement costs---of the myopic
scheme can be arbitrarily large, as the example in Section
\ref{sec:greedy-mvi-shortcomings} shows.

The second bound is related to a common case of a finite budget and
zero-cost measurements. The bound provides certain performance guarantees
for the case when dependencies between items are sufficiently weak,
and shows that the blinkered scheme chooses reasonable search
directions under weaker assumptions than those for the myopic scheme.

\begin{dfn}
\label{dfn:mutually-subadditive}
Measurements of items from set $S=\{s_1, s_2, \ldots \}$ are mutually
subadditive if for any two sets of measurements $Q_1,\;Q_2$ of
disjoint subsets $S_1,\;S_2$ of $S$ the intrinsic
value of information of the union of the sets of measurements is not greater than the
sum of the intrinsic values of each of the sets of measurements, i.e.: $\Lambda(Q_1
\cup Q_2) \le \Lambda(Q_1) + \Lambda(Q_2)$
\end{dfn}

Subadditivity, a weaker property than submodularity,
characterizes sets of items with weak dependencies, when
mutual influence of measurements of items on the belief distribution of
feature values of other items is significantly smaller than influence
on the belief distribution of feature values of the measured
item. The lower performance bound established is:

\begin{thm}
\label{th:bound-multiple}
For a set $S$ of $N_s$ items, a single measurement type, and zero
measurement cost, if measurements of items from set $S$ are mutually
subadditive, the value of information of the blinkered scheme is no
more than a factor of $N_s$ worse than the value of information of an
optimal algorithm.
\end {thm}

\begin{IEEEproof}
Since the measurement cost is $0$, the net value of information
is the same as the intrinsic value, $V=\Lambda$.  Value of information cannot
decrease due to additional measurements, therefore the value of any
sequence of measurements containing all of the measurements in an optimal
sequence is at least as high as the value $V_o$ of an optimal
policy. Consider an exhaustive sequence containing $N_q$ measurements of each
of the $N_s$ items, $N_s\cdot N_q$ measurements total, with value $V_e$. The
exhaustive sequence contains all measurements made according to an optimal
policy within the budget, thus $V_o\le V_e$.

Let $s_i$ be the item with the highest blinkered value for $N_q$
measurements, denote its value by 
$V_{b\,i_{max}}=\max_i V_{b\,i}$. Since measurements of different items are mutually
subadditive, $V_{e} \le N_sV_{b\,i_{max}}$, 
and thus $V_{b\,i_{max}}\ge \frac {V_o} {N_s}$.

The blinkered scheme selects at every step a measurement from a sequence
with value of information which is at least as high as the
value of measurements of $s_i$ for the rest of the
budget. Thus, its value of information $V_b \ge V_{b\,i_{max}}
\ge \frac {V_o} {N_s}$.
\end{IEEEproof}

The bounds in Theorems ~\ref{th:bound-single} and ~\ref{th:bound-multiple} are tight: there exist cases where the bound is reached (see Appendix~\ref{app:tightness}).
%The bounds in these theorems are tight, which can be shown by constructing appropriate problem instances.
