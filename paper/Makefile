all: paper.pdf review-with-responses.pdf

.SUFFIXES: .tex .pdf

%.pdf: %.dot
	dot -Tpdf $< > $@

%.pdf: %.tex
	pdflatex $<

paper.tex: defs.tex \
  abstract.tex \
  bg-depmod.tex bg-limited.tex bg-opt.tex \
  bg-depmod-mrf.tex \
  conclusion.tex \
  emp-case-mms.tex emp-case-svm.tex \
  emp-functions.tex emp-toolkit.tex \
  greedy.tex greedy-sensor-metaphor.tex \
  intro.tex  \
  problem.tex \
  thr-blinkered-estimate.tex thr-computation-reuse.tex \
  thr-markov-chain.tex
	touch paper.tex  

paper.bbl: paper.tex refs.bib paper.aux
	bibtex paper

paper.aux: paper.tex
	pdflatex paper

paper.pdf: paper.tex paper.bbl
	pdflatex paper
	pdflatex paper

review-with-responses.bbl: review-with-responses.tex refs.bib review-with-responses.aux
	bibtex review-with-responses

review-with-responses.aux: review-with-responses.tex
	pdflatex review-with-responses

review-with-responses.pdf: review-with-responses.tex # review-with-responses.bbl
	pdflatex review-with-responses
	pdflatex review-with-responses

clean:
	rm -f *.bbl *.log *.aux *~
