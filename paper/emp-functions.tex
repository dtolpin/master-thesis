% $Id: emp-functions.tex,v 1.21 2011/09/07 12:15:45 dvd Exp $


\subsection {Optimization Benchmarks}

A popular way to compare optimization algorithms is to apply them to
real-valued functions with multiple minima and maxima. Such functions
are difficult to optimize and reflect the challenges which
optimization algorithms meet in real world problems. This study uses
the Ackley function \cite{Ackley.function}
and a modified version of the Himmelblau function
\cite{Wikipedia.Himmelblau_function}. The functions are scaled into a
cube with coordinate range $[-1, 1]$ along each edge. Results for the
Ackley function are presented below, results for the Himmelblau
function are similar.

The two-argument form of the Ackley function is used. The function is
defined by the expression (\ref{eq:emp-ackley}):
\begin{equation}
\label{eq:emp-ackley}
A(x,y)=20\cdot e^{-0.2\sqrt { \frac {x^2+y^2} 2}}+e^{\frac {\cos(2\pi x)+\cos(2\pi y)} 2}
\end{equation}


\subsubsection{Optimization Problems}

Two optimization problems are considered, with a sigmoid utility
function $\tanh(2z)$. In the former problem the movements are free; in
the latter one the movement cost is proportional to the Manhattan
distance. In both problems, the measurements are noisy with
$\sigma_m^2=0.5$, and there are uniform dependencies with
$\sigma_w^2=0.5$ between neighbor nodes in both directions of the
coordinate grid with a step of $0.2$ along each axis.

\subsubsection {Experiment and Representation of Results}
\label{sec:emp-functions-experiment}

For each problem, the myopic and the blinkered
scheme were run 64 times. The chosen locations were recorded, as shown
graphically (see
Figures~\ref{fig:emp-obmark-free-ackley}, \ref{fig:emp-obmark-manhattan-ackley}). In
each figure, the plots in the upper and the lower
rows depict the selected locations\footnote{The coordinates of selection
  locations are slightly dithered in the plots to show that a
  particular location was selected multiple times.} and the histogram
of rewards (the sum of the utility and the budget surplus) for the
myopic and the blinkered scheme, respectively. The extent to
which the locations are grouped around the maxima, and the
utility distribution histograms provide for easy visual comparison of
the results. The textual description below supplies
quantitative performance measures.

\subsubsection {Free Movements}

The case of free movements is relatively easy for the
information-based optimization, and both schemes behave comparably
(Figure~\ref{fig:emp-obmark-free-ackley}). In particular, both schemes
select locations close to the global maximum $(0,0)$ 
but fail to identify the drops in the utility in the saddles
$(\pm0.2, 0)$, $(0, \pm0.2)$.
\begin{figure}[h]
\centering
\includegraphics[scale=0.48,trim=200pt 0pt 0pt 0pt,clip]{obmark-free-ackley.pdf}
\caption{The Ackley function, free movements.}
\label{fig:emp-obmark-free-ackley}
\end{figure}
Still, the myopic scheme produced two outliers: $(0.6, 0.6)$ and $(-1, -1)$.
The mean reward is $0.92$ for the blinkered, $0.81$ for the myopic scheme.
Thus, while on average in the case of free movements both schemes
behave comparably for the given parameters, the myopic scheme is
more likely to select a location with a low utility.

\subsubsection {Manhattan Distance Movement Costs}

When the movement cost is non-zero, the myopic scheme, which cannot
take into account amortization of the movement cost over multiple
measurements, is essentially inferior to the blinkered scheme. Indeed,
the experiment shows better performance of the blinkered scheme
(Figure~\ref{fig:emp-obmark-manhattan-ackley}).  The mean reward is
$0.59$ for the blinkered, $0.40$ for the myopic scheme. The locations
selected by the myopic scheme are spread over a wide area and are less
focused around the true maximum $(0,0)$; $9$ locations among selected
by the myopic scheme have a negative utility, compared to $3$ for the
blinkered scheme.

\begin{figure}[h]
\centering
\includegraphics[scale=0.48,trim=200pt 0pt 0pt 0pt,clip]{obmark-manhattan-ackley.pdf}
\caption{The Ackley function, Manhattan movement costs.}
\label{fig:emp-obmark-manhattan-ackley}
\end{figure}

\subsubsection {Computational overhead}

Measurement selection times and numbers of VOI evaluations are provided in
Table~\ref{tbl:emp-functions}. Both for free movements and for
Manhattan movement costs, the number of evaluations for the blinkered
scheme is 5 times greater than for the myopic scheme. This is a small
factor in real-world applications: if the measurement selection time
of the myopic scheme is negligible compared to the total running
time, then good chances are that the measurement selection time of the
blinkered scheme will remain negligible. 

\begin{table}[h] 
\caption{The Ackley function: runtimes} 
\label{tbl:emp-functions}
\centering
\small
\begin{tabular}{l|c c|c c}
                      & \multicolumn{2}{|c|}{free movements} &  \multicolumn{2}{|c}{Manhattan costs} \\ 
                      & time, seconds& \hspace{-0.75em}evaluations & time, seconds& \hspace{-0.75em}evaluations \\ \hline
myopic                & 1.9 & 3119  & 0.9 & 1752 \\
blinkered             & 9.1 & 14397 & 5.9 & 9422 \\

\end{tabular} 
\end{table} 
