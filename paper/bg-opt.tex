% $Id: bg-opt.tex,v 1.15 2010/04/08 17:47:41 dvd Exp $

\subsection{Optimization under Uncertainty}
\label{sec:bg-opt}

Problems of optimization under uncertainty are characterized by the
necessity of making a decision without being able to predict its exact
effect. Such problems appear in many different areas and are both
conceptually and computationally challenging. Optimization under
uncertainty shares many basic notions with other branches of
optimization, but still differs from them in several formulation,
modeling, and solution issues.

The notion of optimization examined in this paper is
that of making a single, "best" choice from a {\em feasible set},
according to a given {\em objective function}.
Optimization problems can be either {\em discrete} or {\em continuous}.
%In discrete problems, the feasible
%set is countable; in the continuous ones, the feasible set is a subset
%of ${\mathcal R}^n$.

The {\bf Uncertainty} considered here is the lack of exact knowledge
about the values the objective function takes for all or some members
of the feasible set. The ``best'' choice thus depends on the {\em
  beliefs} about the the objective function values, which are
represented by a multi-variate probability distribution.  The agent
can obtain information about the objective function through {\bf
  observations}, which can can be either {\em exact} or {\em
  inexact}. An inexact observation deviates from the true value, and
the observation distribution, conditional on the true value, may also
be unknown.

Optimization under uncertainty is used to solve problems in diverse
domains. A few examples are design of experiments, industrial design,
medical diagnostics, and environmental control. Problems in these
domains can be formulated as optimization of a function under
constraints, when some of the data is unavailable or inexact.

Over the years, various uncertainty modeling philosophies have been
developed \cite{Sahinidis2004}. The range of approaches to
optimization under uncertainty includes, among others, stochastic and
fuzzy programming. The Bayesian framework
\cite{CaramanisMannor.bayesian}, which is assumed in this paper,
is an extension of stochastic
programming in which the uncertainty model incorporates prior beliefs
and is then updated based on observed data using a Bayesian step.

Consider an optimization problem with uncertain coefficients:
\begin{eqnarray}
  \label{eq:bg-opt}
  \mbox{min:}&&f(x,\omega) \nonumber\\
  \mbox{s.t.:}&&x \in X
\end{eqnarray}
where a value $x$ from feasible set $X$  must be found, and the
parameter $\omega$ is uncertain. A typical stochastic optimization
approach is to assume a distribution, conditional on $x$, for the
uncertain parameter $\omega$ and optimize the expected value of the function:
\begin{eqnarray}
  \label{eq:bg-opt-stochastic}
  \mbox{min:}&&\IE_\omega\left[f(x,\omega)\right]=\int\limits_\Omega f(x, \omega)
  p(\omega|x) d\omega \nonumber\\
  \mbox{s.t.:}&&x \in X
\end{eqnarray}
where $p(\omega|x)$ is the conditional probability density function of
the uncertain parameter $\omega$. In the Bayesian framework, the
distribution for $\omega$ is not known in advance, but is gradually
refined by combining prior beliefs with observed data into a posterior
distribution for $\omega$. Realizations of the Bayesian framework
differ in the way in which data about $\omega$ are obtained.
