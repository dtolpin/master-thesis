\documentclass[11pt]{article}
\usepackage{amsfonts,amsmath,amsthm,amssymb}
\usepackage{geometry}
\usepackage{graphicx}

\geometry{a4paper,margin=1in}

\newenvironment{response}[0]%
{\begin{list}{}%
         {\setlength{\leftmargin}{1.5em}}%
         \item[]%
}
{\end{list}}
\setlength{\parindent}{0pt}
\setlength{\parskip}{4pt}


\title {Reviews with Responses}
\author {David~Tolpin and Solomon~Eyal~Shimony}

\begin{document}

\maketitle

\section*{Referee 3}

{\it I have read the authors' responses to my feedback. My feedback was
that (1) the paper had a good experimental coverage, and (2) I was
concerned with the notations and representation of the writings.
However, in this version the notations and the content representations
remain a concern. This has obstructed me from judging the correctness
of the theorem proofs in the appendix.}

\begin{response}
The notation has been changed and carefully checked for improved
clarity.
\end{response}

{\it  And some answers to a few  questions are not up to point. 
I have question regarding the definition of the research problem. In
Page 7, in the objective function, Equation (1), there should be a
policy (say $pi$) as a subscript of max. Also, the policy pi can be
associated with the expectation operator, i.e., the definition should
look like: $\max_{\pi} \mathbb{E}_{P_0, \pi}=...$. The notation $[W]$ is unnecessary
since it is useless except indicating that the goal is to maximize a
net ``reward'' and it is not a given condition like $P_0$ nor a
 parameter.}

\begin{response}
We have split Equation (1) into two equations, (1) and (2). Equation (1)
provides the definiton for reward $W$ of a sequence of measurements $Q$, and Equation (2) states 
formally the goal of finding a policy that maximizes the expected reward given the initial
belief distribution and the measurement budget. Since measurement outcomes are random,
the measurement sequence $Q$ given a policy, as well as the outcomes of the measurements, are 
randomly distributed; therefore, the expected reward $\mathbb{E}[W|\pi]$ given
the policy must be maximized.
\end{response}

{\it Measurement types $M={(c^m,P^m)_k|k=1..N_m}$. What is the relationship
between the items and the observed features?}

\begin{response}
We have changed the explanation so as to clarify the fact that we have
$N_m$ feature types, and each item has one feature )and a corresponding feature value)
for each of these types.
\end{response}

{\it $P^m$ denotes the set of probabilities of $p(observation features = feature_j | item = item_i)$?}


\begin{response}
No. We have changed the text so that this part is clear.
\end{response}

{\it However, the text says observation probability distribution $P^m$ of the
   observed feature values is conditional on the true feature values for
   each measurement type. That says the probability is conditional on the
   features rather than the items. Which one is right?}

\begin{response}
Correct, it is conditional on the feature values (of the measured item).
Clarified in the revised text.
\end{response}

{\it How the model components like the measurement type $M$ and the utility
$u(Z)$ depend on time? They are stationary, i.e, time invariant?}

\begin{response}
Indeed, we assume throughout that they are stationary, i.e. time invariant.  
We never stated anywhere in the paper that they change over time, but we added
an explicit comment that they are time-invariant in the revised version, just
to make sure.
\end{response}

{\it Note that in objective function Equation (1), only the last time point
accumulates utility $u(z(s_\alpha))$, while other time steps accumulate
the cost ($c^m_{k_i}$) and the movement cost $d(s_{i-1},s_i)$. Is this what
you intended?}

\begin{response}
Indeed, that was exactly what we intended.
\end{response}

{\it The question is that at the non-last step, why the model
cannot accumulate utilities and the cost at the same step?}


\begin{response}
You might have a slightly more general model that can accumulate rewards
(utilities) in other steps as well. However, this was not needed for any of
the application problems. This would further complicate a model that is already
quite complicated, needlessly in our opinion, and one of the main differences
of our problem definition from a more general POMDP is exactly that the only
positive reward is at the end, we rely on that in the solution methods.
(For example, this difference is the major difference between our ``basic case''
setting and the well-known "multi-armed bandit" problem, as stated
in Section II, ``Related Work'', page 6 and Section III-A, ``Typical
Problem Settings'', page 9).
\end{response}

{it I have tried to map the model components in the definition to the
``base case'' example in Page 14. After reading the paragraph
   starting from ``Base case'' under the heading ``B. Problem
   Examples'', I had no idea what are the items, features, measurement
   types, utility, move cost and the cost budget. I assume that this
   paragraph should give readers the physical meanings of the model
   components.}

\begin{response}
Moved this earlier and clarified these issues in Section III.A,
``Typical Problem Settings'', pages 8--9.
\end{response}

{\it After reading the authors' response, my questions remain in the POMDP
   formulation and the definition of the measurement types $M={c^m,P^m}$.}


\begin{response}
POMDP formulation repeated here, further clarified
(although not all that useful, as we do not use standard POMDP
techniques to solve the problem, as the resulting POMDP is too large to handle directly
by these methods):

Indeed the entire selection problem can easily be cast as a special case
POMDP, and an earlier version of the paper did so --- removed due to
reviewer comments that stated that it is redundant in the paper, but
restated as an answer below for clarity. 
Viewed as a POMDP, the selection problem
has a factored state space, consisting of 3 types of state variables:

\begin{itemize}
\item Sensor "location". This part of the state is fully deterministic:
   it is known, and the transitions are fully deterministic.
\item Item feature values. This part of the state is constant, though
   initially unknown. That is, items do not evolve. Only our belief about
   them evolves.  The distribution $P_0$ is over the feature values,
   which is exactly the initial ``belief distribution'' $b_0$ in POMDP
   terms.
\item Either the ``remaining budget'' or the ``budget used'',
   for the case of the finite budget. The change in this state
   variable is deterministic.
\end{itemize}

We did not specifically state an action/transition model as it
is rather trivial (i.e. no state change, except for deterministic
change in sensor location, and thus next state is of course unique
given the (unknown) previous state).
The selection problem is closely related
to ``deterministic POMDP'' [Bonet, 2009], and in fact would have been a
special case of the latter except that measurements in our selection problem
are noisy. As in deterministic POMDP, the only uncertainty is due
to uncertainty in the initial state.

The other POMDP elements are obvious: observation distributions
are stated in factored form in terms of measurement noise, rewards
in terms of measurement costs (and are additive, non-discounted).
Finally, this is an ``indefinite horizon POMDP'' as the number
of actions may not be fixed in advance, but a terminal state is
reached as soon as an item is selected (with a reward based on the
true item features).
\end{response}
\end{document}







