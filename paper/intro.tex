% $Id: intro.tex,v 1.19 2011/09/07 12:15:46 dvd Exp $

\IEEEPARstart{O}{ptimization} under uncertainty  is a domain with
numerous important applications \cite{Rish.adaptive,Krause.water}.
Problems of optimization under uncertainty are intractable in general,
and thus special cases are of interest. In this paper, the following selection
problem is examined: given a set of items of unknown utility (but a
distribution of which is known), an item with as high a utility as
possible must be selected.  Measurements (possibly noisy) of item
features are allowed prior to selection, at known costs. The objective
is to optimize the overall decision process of measurement and
selection, which is still an intractable problem \cite{RadovilskyOSS}.

We follow the standard general framework for selecting measurements
based on value of information (VOI) \cite{Russell.right}. 
Since computing VOI is intractable in general, both researchers
and practitioners often
use various forms of myopic VOI estimates \cite{Russell.right} coupled
with greedy search (see Section \ref{sec:greedy}). Even when not based 
on solid theoretical guarantees, such estimates lead to practically
efficient solutions in many cases.
However, in a selection problem involving items with real-valued
utilities, coupled with the capability of the system to perform more 
than one measurement for each item, the
myopic VOI estimate may badly underestimate the value of
information (Section \ref{sec:greedy-mvi-shortcomings}),
resulting in inferior measurement policies. 

The main contribution of this paper is to estimate VOI of a measurement
by evaluating VOI of ``batches'' of measurements. In particular,
batching similar measurements of the same item,
 called the ``blinkered'' scheme, results in better
sensing policies for the pathological cases mentioned above,
at an affordable expense in terms of computation cost.
The proposed blinkered scheme is cast within
a larger ``semi-myopic'' framework, which allows for a
flexible tradeoff between the quality of the sensing policies and
the computation time, another contribution of this paper.

The semi-myopic framework in general, and the blinkered scheme in particular,
are useful in several settings. One such application is
meta-reasoning  \cite{Russell.right}, an aspect of which is considering
which time-consuming deliberation steps to perform
before selecting an action. A concrete setting where
meta-reasoning is used in this paper is to speed up parameter selection
for SVM classifiers. Another application is
locating a point of high temperature using a limited number of
measurements (with dependencies between locations as in
\cite{Guestrin.graphical}). Yet another is the problem of finding a good set
of parameters for setting up an industrial imaging system.  The latter
problem is actually the original motivation for this research, and is
discussed in Section~\ref{sec:emp-case-mms}.

We begin (Section \ref{sec:related})
with a review of related research work that is at the basis of
the methods used in this paper. Some space is devoted to
mentioning papers on closely related problems, differentiating
them from the optimization problem addressed in this paper.
This is followed (Section \ref{sec:problem})
by a formal statement of the selection problem
in general terms, and an explanation of how this general statement
maps to specific applications. Section \ref{sec:greedy} revisits the
standard greedy-myopic scheme and focuses on the cases where it fails.

Our goal is an efficiently
computable scheme that overcomes the limitations of myopic
VOI.
%Since our goal is to find a scheme that, while still
%efficiently computable, can overcome these limitations of myopic
%VOI, 
We thus propose the framework of semi-myopic VOI
(Section \ref{sec:thr}), which includes the myopic VOI as the simplest
special case.
%, but also much more general schemes, including exhaustive
%subset selection at the other extreme.  
Within this framework, we introduce the ``blinkered'' VOI estimate, 
and show that it meets the above goal for many problem instances.
Although the general selection problem
is difficult to analyze, we still get theoretical
bounds that show the benefit of the blinkered estimate
(Section \ref{sec:thr-blinkered-estimate}) for some simple indicative
cases, which nevertheless shed further light on the tradeoffs involved.
Empirical results on artificial and real-world data 
further support using the blinkered scheme (Section \ref{sec:emp}).
