\documentclass[11pt]{article}
\usepackage{geometry}
\usepackage{graphicx}

\geometry{a4paper,margin=1in}

\newenvironment{response}[0]%
{\begin{list}{}%
         {\setlength{\leftmargin}{1.5em}}%
         \item[]%
}
{\end{list}}
\setlength{\parindent}{0pt}
\setlength{\parskip}{4pt}


\title {Reviews with Responses}
\author {David~Tolpin and Solomon~Eyal~Shimony}

\begin{document}

\maketitle

We thank the reviewers for the detailed comments which helped us
further improve the paper in its revised version. We have carefully
revised the paper, ensuring that the definitions and explanations
are given at all appropriate places, and the notation is clear
and consistent.

\section*{Referee: 1}

{\it 1. ``We have rewritten and extended Related Work (Section II); we
  explained that the paper by Liao et al [17] discusses offline
  observation selection. whereas we address the much harder problem of
  online observation selection. The paper by Guestrin et al [5]
  discusses optimal algorithms for both offline and online observation
  selection, but for a restricted case of chains of discrete random
  variables, and where the utility function is additive over features
  of the node distributions, whereas in our case we have a `max'
  operator, which is not additive.''

The method by Liao et al is not limited by offline selection.}


\begin{response}
The method by Liao indeed is not limited by offline selection. 
Still it does focus mainly on a problem that w.r.t. our framework
can be seen as offline selection.
According to [5], there are two related but
different problems. The first problem is called ``subset selection'',
see Section 4 in [5]. ``In the \emph{subset
selection} problem, we want to find a most informative subset of the
variables to observe \emph{in advance}, i.e., before any observations
are made.''  The second problem is called ``conditional plan'', see
Section 5 in [5]. ``In the \emph{conditional plan}
problem, we want to compute an optimal querying policy: We
sequentially observe a variable, pay the penalty, and depending on the
observed values, select the next query as long as our budget
suffices. The objective is to find the plan with the highest expected
reward.''

The paper by Liao et al [17] considers ``subset
selection'' problems. The paper describes the considered problems as
follows:
\begin{quotation} By purposely choosing an optimal subset from
multiple sensing sources, it can save computational time and physical
cost, avoid unnecessary or unproductive sensor actions, reduce
redundancy, and increase the chance of making correct and timely
decisions. Due to these benefits, sensor selection plays an especially
important role for time-critical and resource-limited applications,
including computer vision, control systems, sensor networks, diagnosis
systems, and many military applications.

Basically, sensor selection problems can be divided into two
types. The first type, called a budget-limit case, involves choosing a
sensor set with maximum information gain given a budget limit. The
other type, called an optimal tradeoff case, involves deciding a
sensor set that achieves an optimal tradeoff between the information
gain and the cost. Unfortunately, both of them are NP-hard, because
the number of sensor subsets grows exponentially with the total number
of sensors.
\end{quotation}

We consider a different, ``conditional plan'' problem, in particular,
we consider a conditional plan single {\bf item selection} (as
opposite to sensor subset selection) problem. In this problem, value of
information of a conditional plan is generally not a submodular
function: considering a measurement at a later step may have greater
expected utility than at an earlier step. The introduction to paper by
Liao et al. [17] mentions ``an active sensing
approach'', which is a ``conditional plan'' approach to sensor
selection:

\begin{quotation}
An active sensing approach, based on an entropy measure called the
R\'enyi divergence, is proposed by Kreucher et al. to schedule sensors
for multiple-target tracking applications. At each time step, only one
sensor action is chosen to provide measurement and thus update the
probability density of the target states. Kullback-Leibler’s (KL)
cross-entropy is used for optimal multisensor allocation and sensor
management. Ertin et al [10] employ expected posterior entropy to
choose the next measurement node (sensor) in a distributed Bayesian
sequential estimation framework. They show that minimizing the
expected posterior uncertainty is equivalent to maximizing the mutual
information between the sensor output and the target state.
\end{quotation}

However, the active sensing approach is equivalent to sensor subset
selection in the special case when the function to minimize is the
\textbf{expected posterior uncertainty}, which (under certain
conditions) does not depend on the outcomes of the measurements. In the
general case, while a conditional plan problem can be solved as a subset
selection problem, the information obtained from the measurement
outcomes, which can improve the expected utility of the solution,
would not be used. The example given in the answer to the next
question illustrates the difference between the ``subset selection''
and ``conditional plan'' approaches, when  ``conditional plan''
results in a greater expected utility than ``subset selection''.
\end{response}

{\it Compared to the paper by Guestrin et al, what's the advantage of using "max"
operator instead of using "additive" operator?}

\begin{response}
In our selection problem, the `max' operator is inherent to the problem
formulation (resulting from the applications),
and a difficulty to overcome, rather than a perceived advantage.
That is due to the fact that a single item with the greatest utility must be
chosen, resulting in an inherently non-additive utility function.
Sometimes, the `max' operator can be approximated by an `additive'
operator: instead of selecting an item with the greatest expected
utility (conditional plan), one selects a set of measurements which
maximize the expected utility of the finally selected item (subset
selection). However, in the general case, this reduction is not
possible: an optimal conditional plan often has  a greater expected
utility than an optimal static plan (subset selection) ([32]).

Consider the following example: there are three items $s_1$, $s_2$,
$s_3$, each with a single feature $z$ that can take values 0 or 1 with
equal probability: $p(z=0)=p(z=1)=\frac 1 2$. The measurement
returns the true value of $z$ and has cost $c$. A policy that selects
an item with the maximum value of $z$ must be chosen. Here, the
`max' criterion corresponds to performing a single
measurement at each step and then making a decision whether to select
an item with the highest expected utility or to make another
measurement. The `additive' criterion
corresponds to selecting a set of measurements, performing all of the
measurements, and then returning the item with the highest expected
utility.

The optimal conditional plan (`max') commits to a measurement if the
measurement cost $c<0.25$, otherwise selects an item randomly. If the
first measurement (of item $s_1$ WLOG) returns $z(s_1)=1$,  $s_1$ is
returned. Otherwise, $s_2$ is measured, and if $z(s_2)=1$, $s_2$ is
returned, otherwise, $s_3$ is returned. The net expected utility of the
optimal conditional plan is 

\[EU_{\max}(c)=\max(0.5, 0.875-1.5c)\]

The optimal static plan (`additive') performs either 0, 1, or 2
measurements, depending on the measurement cost, and selects an item
with the highest expected utility. The net expected utility of the
optimal static plan is

\[EU_{\mathrm{additive}}(c)=\max(0.5, 0.75-c, 0.875-2c)\]

It is easy to see that $EU_{\max}(c) \ge EU_{\mathrm{additive}}(c)$
for all values of c, and the inequality is strict for $0<c<0.25$.
Figure~\ref{fig:max-vs-add} shows expected
utilities of the static (`additive') and the conditional
(`max') plans.

\begin{figure}[h]
\centering
\includegraphics[]{fig-max-vs-add.pdf}
\caption{`max' vs `additive' measurement selection}
\label{fig:max-vs-add}
\end{figure}

\end{response}

{\it 2. "We extended the description of the algorithm with the
theoretical analysis of the runtime, and provided data on
computational overhead in Empirical Evaluation (Section VII). We have
shown that while both the myopic and the blinkered schemes are
sufficiently efficient for the computational overhead to be small
compared to the measurement time, the omnimyopic and the exhaustive
method require a much longer time to compute, while still not
guaranteeing optimality. Subsequent work by the authors, Rational
Value of Information Estimation for Measurement Selection, URPDM 2010;
proposes further improvements to performance of the greedy scheme in
cases when VOI recomputation is expensive. "

The new table (Table III) only lists the cases for "2 items" and "4
items", which only shows for small scale, myopic and blinkered are
faster; but how about for larger scale, will both of them be slow,
even relatively faster than the exhaustive approach?}

\begin{response}
In addition to Table III, Tables IV, V, and VI had been added for
the larger problems. In these tables, measurements time, deliberation
times, and total search times are provided both for synthetic
experiments (Table III, IV) and for case studies (Table V,VI). The
times are discussed in the corresponding empirical evaluation sections
as follows:

\begin{quotation}
\subsection*{The Ackley function}

Measurement selection times and numbers of VOI evaluations are provided in
Table~IV. Both for free movements and for
Manhattan movement costs, the number of evaluations for the blinkered
scheme is 5 times greater than for the myopic scheme. This is a small
factor in real-world applications: if the measurement selection time
of the myopic scheme is negligible compared to the total running
time, then good chances are that the measurement selection time of the
blinkered scheme will remain negligible. 

\subsection*{Optimization of SVM parameters}

Note that in the SVM parameter optimization
selection problem, ``measurements'' are performed
internally, and thus cost only in terms of runtime.
A comparison of the runtimes is provided in
Table~V; the runtime includes
training and cross-validation of the SVM classifier for the evaluated
combination of parameters (measurement ``cost''), 
while the measurement selection time is the time
spent by the algorithm to choose the combinations to evaluate
(``meta-reasoning'' time). As
one can see from the table, the runtime of the blinkered
scheme is 40\% longer than that of the myopic scheme. However, this is due
to the blinkered scheme making more ``measurements'' and eventually
selecting a better parameter combination. 
The measurement selection time for the blinkered
scheme is still about 6\% of the runtime and contributes only 18\% to
the difference in the runtimes; therefore, the blinkered scheme
can be considered consistent with the rational metareasoning approach.

\subsection*{Metrology Machine Setup}

A comparison of the runtimes is provided in
Table~VI. The total runtime includes the
actual time to perform measurements, while the measurement selection
time is the time spent by the algorithm to choose the measurements
to perform. The blinkered scheme takes about 18\% more time, due to
both additional measurements and more intensive computations of the
blinkered scheme. For the myopic scheme, the computation time is
1.5\% of the total runtime, for the blinkered scheme the measurement
selection time increases to 11\% of the total runtime, but is still
relatively small.
\end{quotation}
\end{response}

\section*{Referee: 2}

{\it My concerns have been adequately addressed, and I recommend the
paper for acceptance.}

\section*{Referee: 3}

{\it The paper has been improved significantly from its original
  submission. The rewritten or reorganized sections include
  "Introduction", "Related Work", "Problem Statement', and "Greedy
  Myopic Optimization". I am very satisfied with the experiment
  sections that I think are pretty comprehensive and useful
  comparison.

I have a few concerns in the 7-tuple definition of a selection
problem.  (1) It seems lack of a transition model defining how items
evolve. It should take the form of a mapping from $s_i$ to $\{s_j\}$
with certain probability distributions, as an action model in a POMDP
setting or a state transition model in an HMM setting.  Given a state
$s_i$, its possible next state is not unique; the reachable states are
governed by a probability distribution. Such a model is essential in
calculating the belief states. I am not sure if there is one
transition model but not presented, or there is not such model at
all. I believe that $P_0$ is with respect to the first or zero time
step from Equation (1) since it is placed for the expectation.}

\begin{response}
Indeed the entire selection problem can easily be cast as a special case
POMDP, and an earlier version of the paper did so - removed due to
reviewer comments that stated that it is redundant in the paper, but
restated as an answer below for clarity. 
Viewed as a POMDP, the selection problem
has a factored state space, consisting of 2 types of state variables:
\begin{enumerate}
\item Sensor ``location''. This part of the state is fully deterministic:
   it is known, and the transitions are fully deterministic.
\item Item feature values. This part of the state is {\em constant}, though
   initially unknown. That is, items do {\em not} evolve. Only our belief about
   them evolves.  The distribution $P_0$ is over the feature values,
   which is exactly the initial ``belief distribution'' $b_0$ in POMDP
   terms.
\end{enumerate}
We did not specifically state an action/transition model as it
is rather trivial (i.e. no state change, except for deterministic
change in sensor location, and thus next state is of course unique
{\em given the (unknown) previous state}).
The selection problem is closely related
to ``deterministic POMDP'' [Bonet, 2009], and in fact would have been a
special case of the latter except that measurements in our selection problem
are noisy. As in deterministic POMDP, the only uncertainty is due
to uncertainty in the initial state.

The other POMDP elements are obvious: observation distributions
are stated in factored form in terms of measurement noise, rewards
in terms of measurement costs (and are additive, non-discounted).
Finally, this is an ``indefinite horizon POMDP'' as the number
of actions may not be fixed in advance, but a terminal state is
reached as soon as an item is selected (with a reward based on the
true item features).
\end{response}

{\it (2) A joint distribution (on top of Page 7) $P_0$ is a
  distribution at the first time step?  It appears that given an item
  $s$, $P_0$ prescribes the probability distribution of observing
  feature $\{z_i(s)|i\in certain I\}$, i.e., over the feature space of
  an item $s$. However, the notation showed that $P_0$ is the
  probability distribution of observing features over the
  cross-product of features over all items? Please clarify. This again
  looks like an observation model as in POMDP or HMM.}

\begin{response}
$P_0$ is indeed a distribution at (just before) the first time step over
the cross product of features. It is assumed that the marginal
distribution $P_{0i}$ for the feature vector is provided in a factored
manner.  One noteworthy example of that is the case of multivariate
normal distributions, with the marginal distribution obtained by simply
removing irrelevant rows and columns from the mean vector and the
covariance matrix. Subsection III.A, {\it Typical problem settings}
mentions that the distribution is assumed to be structured.
\end{response}

{\it (3) The next bullet defines the set of measurement types
  $M=\{c^m,p^m)\}$. Literally, a measurement type is categorical. So
  $N_m$ is the number of possible measurement types? It is unclear
  what domains are defined by measurement cost $C^m$ and observation
  distribution $p^m$. The measurement types are defined regarding a
  single item or feature, or multiple items or features? Since there
  are two different probabilities in the 7-tuple (referring (2)
  above), it is rather important to give them a clear explanation
  upfront.}

\begin{response}
Indeed, $N_m$ is the number of measurement types. The cost $c^m$ is
real-valued, the conditional observation  distribution ($P^m$) is on
the space of the feature vector of a single item $\mathcal{R}^{N_f}$.
We have augmented the problem definition with the domains for the
measurement cost and for the feature vector.
\end{response}

{\it (4) Also, please do not use both upper case $P_0$ and lower case
  $p^m$ to refer to a probability distribution. If you double-check
  all the notations $p$ throughout the paper, they represent many
  different things on the roads. Please check them and make sure they
  are consistent.}

\begin{response}
We have revised notation throughout the paper and changed it so that
the notation is consistent in all sections. In particular, lowercase
$p$ refers to a probability density function. The notation for
measurement types has been changed to $(c^m, P^m)$.
\end{response}

{\it The subsection "Limited Rationality' of Page 10 is devoted to
  "Rational metareasoning". It is good to see that the expected values
  are with probability distributions after the revision. There is no
  problem to use meta-level action and base-level action, but please
  give some hints on what they represent in the context of the studied
  selection problem.  I still do not get what is a meta-level action
  here. In my understanding, a meta-level action is at a broader
  level. For example, a meta-level action may restrict an agent into a
  region of locations, but a base-level action may restrict it into a
  specific location within the region. Another comment is related to
  the notation $S_i$ and $S_j$ representing meta-level actions, but
  lower cases $s_i$ and $s_j$ have been used to represent items. This
  results in a lot confusions to the readers. Moreover, the capital
  $C$ has been used to refer to the budget in the problem definition,
  but here it is the cost function over two parameters $A_i$ and
  $S_j$.}

\begin{response}
We have changed notation from $S_i$ to $M_i$ (for ``{\bf m}etalevel
action'') in section IV-A, Limited rationality. Section IV-B, The
Metareasoning approach to Measurement Selection, establishes the
correspondence as follows: 
\begin{quotation}
In the framework of the rational metareasoning approach (Section
IV-A), a {\em measurement} corresponds to a {\em
  meta-level action}, and the {\em final selection} to a {\em
  base-level action}. Thus, a measurement with the highest net value
of information is performed at each step.  The algorithm terminates
when no measurement appears to have positive net value of information,
at which point the item with the highest expected utility (given the
current information) is selected and returned.  The joint belief
distribution is initialized to a {\em prior belief}, and then new
evidence is incorporated into the belief through Bayesian
inference~[28].
\end{quotation}
\end{response}

{\it In Page 19, the paragraph right below Theorem 1 used the term
  ``regret''. This is frequently used term in probabilistic inferences
  and reinforcement learning. Please explain what it means in your
  context.}

\begin{response}
We have added an explanation of the term ``regret'' (the
difference between the utility of the best item and the utility of the
selected item less the measurement costs).
\end{response}

{\it In Page 20, the notation $\Lambda(Q_1)$ and $\Lambda(S_j)$ in
  Equation (7) of Page 7 are the same thing? I assume that the former
  case means an action of measuring the measurement in the set $Q_1$,
  and the latter means a base-level action, i.e., measuring certain
  measurement types?}

\begin{response}
$\Lambda(S_i)$ ($\Lambda(M_i)$ in the corrected notation) is the
intrinsic value of information of a single measurement $S_i$
($M_i$). $\Lambda(Q_j)$ is the intrinsic value of information of a
sequence of measurements $Q_j$, applied as a batch.
\end{response}

{\it Page 2, last line, futher$\Rightarrow$further?\\ Page 8, line 5,
  encurred$\Rightarrow$incurred?\\ Page 13, last line, "two items ,
  $s_1...$"$\Rightarrow$remove the space in between\\ Page 34,
  Reference \#5, please add the journal title }

\begin{response}
We are sorry for slipping the typos in. We have fixed the typos, and
spellchecked the whole paper.
\end{response}

%\bibliographystyle{plain}
%\bibliography{refs}

\end{document}
