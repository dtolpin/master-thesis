% $Id: problem.tex,v 1.42 2011/09/07 12:15:46 dvd Exp $

The selection problem addressed in this paper is:
given a set of items, an item with as high a utility as
possible must be selected. 
The items have properties (also called features), each item feature having a value from
a known domain, but the actual feature values are initially unknown. 
The utility of selecting an item is a known function
of the (unknown) feature values of the item. 
Measurements (possibly noisy) of item
features are allowed prior to a final selection of an item,
each measurement having a known cost. The objective
is to optimize the overall decision process of measurement and
selection. Formally, a selection problem is a 7-tuple
$(S, Z, P_0, M, u, d, C)$ where:
\begin{itemize}
\item $S=\{s_1, s_2, \ldots\}$ is a set of items.
\item $Z$ is a set of $N_f$ item 
feature types $Z=\{ z_1, z_2, \ldots, z_{N_f}\}$. (Each feature
type $z_i$ has a domain $\mathcal{D}(z_i)\subseteq \mathbb{R}$.)
\item Every item has a (time-invariant) value for each of the feature types from $Z$.  
$P_0$ is a joint distribution over the feature values of the items in $S$
that specifies the initial belief state over these values. That is, $P_0$ is
a joint distribution over the random
variables $\{ z_1(s_1), z_2(s_1),\ldots , z_1(s_2), z_2(s_2),\ldots\} $. 
\item $M=\{(c^m, P^m)_k\:\vline\; k \in
  1..N_m\}$, is a set of measurement types. Each measurement type can be applied
to any item, providing information about some of its features. Each measurement
type has an intrinsic measurement cost $c^m\in \mathbb{R}$.
$P^m$ is the ``sensor model'', of the measurement, i.e. the distribution
over the observed feature values, conditional on the true feature values.
Repeated measurements are assumed independent given feature values.
\item $u(\textbf{z})\colon\mathbb{R}^{N_f}\to
  \mathbb{R}$ is a known utility function of an item over feature values.
%\footnote{In the simplest case, there is just one real-valued
%feature, acting as the item's utility value, and $u$ is thus simply the identity function,
%see below.}
\item  $d(s_i,s_j)\colon S\times S\to\mathbb{R}$ is the cost of
of moving a sensor from item $s_i$ to item $s_j$.
\item $C$ is a measurement budget.
\end{itemize}

A policy of measurement and selection for a selection problem is a mapping
$\pi $ (either explicit or implicit) from belief states (distributions over 
item features, or alternately histories of observations) to actions, which are
either measurements, or selection of a final item. A policy applied
to the initial distribution results in a (stochastically generated)
sequence of measurements and final selection 
$Q=\{q_i=(k_i,s_i)\:\vline\;i \in 1..N_q\}.s_\alpha$,
where $k_i$ is the type and $s_i$ is the
location of measurement $q_i$, and $s_\alpha$ is the final selected
item. The reward $W$ of the sequence $Q$ is  the utility of the final
selected item $s_\alpha$ less the total cost of the measurements:
\begin{equation}
\label{eq:problem-reward}
W\triangleq u(\textbf{z}(s_\alpha))-\sum_{i=1}^{N_q}\left(c^m_{k_i}+d\left(s_{i-1},s_i\right)\right)
\end{equation}

The goal is to find a policy that 
obeys the measurement budget limit $C$
and maximizes the expectation of the reward $W$ over all possible measurement
outcomes, with a distribution based on the initial belief
$P_0$ and information received from measurements
according to the observation distribution model:
\begin{eqnarray}
\arg\max\limits_\pi:&&\IE_{P_0}[W\,|\,\pi]\nonumber\\
\mathrm{s.t.:}&&\sum_{i=1}^{N_q}\left(c^m_{k_i}
                +d\left(s_{i-1},s_i\right)\right)\le C
\end{eqnarray}

Specific settings of the problem depend on the
way information obtained from measurements is treated.
This paper focuses on the {\em online measurement selection}
\cite{Rish.efficient} problem: a measurement is selected after the
outcomes of all preceding measurements are known. An alternative
problem is {\em offline measurement subsequence
selection}\cite{Liao.sensor}\cite{Krause.water},
in which the policy is just a pre-determined sequence of measurements,
without taking outcomes of previous measurements into account.

\subsection{Typical Problem Settings}

The above problem statement is too general to handle directly.
In typical settings of the selection problem, additional structure is
usually imposed as follows:

{\bf Parameters:} The set of items $S$ can be indexed according to a set
  of parameters $I$.  The parameter values of an item
  $s\in S$  are denoted by $I(s)$.  In the simplest case $I$ is just an integer
  index. Alternatively, $I$ can be a set of coordinates, and the
  items $S$ are indexed by points on a multi-dimensional grid.  The
  latter case provides an approach to approximate optimization of an
  unknown continuous function.

{\bf Distance metric:} When $I$ is a set of coordinates,
  one common set of cost functions is where $d$ is a function only of
  a distance metric over the item coordinates, i.e. $d(s_1, s_2) =
  f(||I(s_1), I(s_2)||)$ for some function $f$ and some metric
  $||\cdot||$. This would be the type of cost incurred by having to physically
  move sensing equipment from location to location.

{\bf Distribution:} The joint distribution over the feature space is
  also structured in many cases, with variables assumed
  independent except where indicated by a structured probability model
  such as a Bayes network, a Markov network, or a Markov random
  field. For example, when $I$ is a multi-dimensional grid, the
  dependency between features of the items is typically modeled as a
  Markov random field with a neighborhood determined according to the
  grid---only (features of) items $s^\prime$ that are neighbors on the grid of
  $s$ are in the Markov neighborhood of (features of) $s$.

The selection problem can be further specified to fit numerous
application settings. For illustrative purposes, we
begin with the most basic case, before moving on to the more complicated
real applications.

{\bf Basic case:} The (finite) set of
items is ${s_1, s_2, ... s_n}$, and each item has a single real-valued feature
$z$. Thus, the distribution $P_0$ is over the random variables
$(z(s_1), z(s_2),... z(s_n))$.  The utility function $u$ is a simple
monotonically increasing function of the feature values, such as the
identity function, i.e.  $u(z(s_i)) = z(s_i)$.  The features $z(s_i)$
of the different items are mutually independent, the movement cost
$d(s_i, s_j)$ is zero, and the budget $C$ is infinite. There is only
one measurement type, which provides a noisy observed value of
$z(s_i)$, and the measurement costs are a uniform value $c^m$. To fully
determine the problem, one must still define the initial belief
distribution $P_0$ of the feature values $z(s_i)$, and the measurement
distribution $P^m(x|z(s_i) = y)$.  These distributions are obviously
application-dependent, but we will examine
in this paper the simple case of a multi-variate normal distribution
$P_0$. The measurement distribution $P^m$ (the sensor model) consists
of additive zero-mean Gaussian noise.

Even for the basic case, obtaining the optimal policy
seems intractable. In fact, this setting resembles
the multi-armed bandit problem (see Section \ref{sec:related}).
Closed-form solutions exist for simple sub-cases,
such as the example in Section \ref{sec:greedy-mvi-shortcomings}.
Section \ref{sec:emp} uses the basic case as one of the benchmarks.

\subsection{Application Examples}

Although there are applications where the basic case can be used,
our interest in defining the selection problem model originated
from more general settings. Of the three examples below, the first is
just a potential application, while the latter two were
raised in actual industrial settings.

{\bf Water reservoir monitoring:} A water reservoir is monitored for
contamination sources. Water probes can be taken in a number of
predefined spots. The goal is to quickly discover the location and
intensity of a contamination source based on analysis of water
quality. 
%The contamination must be identified quickly, before it
%distributes too far or affects the consumers. 
Here, the {\em features}
are concentrations of possible contaminants, the {\em utility
function} is a function of the concentrations; the {\em measurement
cost} is determined by the time required to perform the analysis of
a probe, and, in case of a large natural reservoir, the time and cost
to move the probe physically between locations.

{\bf Metrology machine setup:} A focus and a filter color must be
chosen for optimal setup of metrology equipment (see Section
\ref{sec:emp-case-mms} for details). For each combination of the focus and the
color (these are the {\em item coordinates} $I$), a number of {\em features} can be
observed inexactly, and the {\em utility function} depends in a rather
complicated way on the feature values. Both the measurements and the
equipment movements take time and determine the {\em measurement} and
{\em movement costs}, and the setup must be completed within a given {\em
time budget}.

{\bf SVM parameter optimization:} This is a selection problem
in a meta-reasoning setting (see Section
\ref{sec:emp-case-svm}).  Classification quality of a support vector
machine (SVM) depends on one or more parameters (see Section
\ref{sec:emp-case-svm} for a case study). While there are heuristics
for selecting good parameter values for particular kernel types and
data sets, several combinations of parameters must be tried before a
good one can be chosen. The goal is to get a good setting in a limited
amount of computation time. The SVM parameter optimization problem is
formulated as the following special case of the selection problem:

\begin{itemize}
  \item Each combination of optimization parameters (there are two
    parameters, $\zeta$ and $\gamma$) corresponds to an {\bf item} $s_i$, there
    are
    $\left|\mathcal{D}_\zeta\right|\cdot\left|\mathcal{D}_\gamma\right|$
    items, where $\mathcal{D}_\zeta,\;\mathcal{D}_\gamma$ are domains of
    $\zeta$ and $\gamma$, respectively.
  \item The only {\bf feature}, is the classification
    accuracy $a\in[0, 1]$.
  \item A joint distribution of accuracies for each combination of the
    parameters is represented by a two-dimensional Markov random
    field.
  \item There are two types of measurements, differing in the cost and
    the observation distribution around the true value:
    \begin{itemize}
      \item measurement $m_1$ uses the whole data set to assess
        accuracy (via cross-validation); the observation distribution 
        has low variance, but cross-validation takes a long time due
        to the large size of the data set, and thus the measurement
        cost $c^m_1$ is high;
      \item measurement $m_2$ uses a fraction of the data set; the
        cross-validation is faster and thus the measurement cost $c^m_2$
        is lower, however the observation distribution is less precise
        (has higher variance).
    \end{itemize}
  \item  The utility function is $\tanh(a-\frac 1 2)$, the function
    changes quickly for mediocre values of accuracy $a$ but levels
    off when $a$ approaches $1$ to compensate for inherent
    randomness of SVM classifiers.
  \item Movement cost $d(s_i, s_j)$ is negligible (assumed to be 0).
  \item Budget $C$ is a time limit specified by process engineers.
\end{itemize}
