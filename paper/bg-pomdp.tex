
\subsection{Selection problems as  POMDPs}

In the selection problems considered in this paper, observations are achieved by
making measurements, each at a known cost. The cost in some cases depends on
the previous measurement, as defined formally in Section \ref{sec:problem}.
The entire process, of selecting which measurements to perform (the selection
possibly depending on observations
obtained due to previous measurements) and then finally settling on a choice of item
is a sequential stochastic decision process. The selection problem
can be stated in terms of the well-known partially observable Markov decision 
process (POMDP) model.

A POMDP \cite{Kaelbling.POMDP} is defined over a state space $S$, a set $A$ of possible actions,
and a set $O$ of possible observations.  In addition, a state transition model
$\tau $ maps from state-action pairs to a distribution over states (that is,
a probability of landing in a state $s^\prime $ given that an action $a$ is
executed in state $s$). An observation distribution $\sigma $ from pre-action states,
actions, and post action states to $O$ is the stochastic observation model.
A reward (or alternately, cost) function R from pre-action states, actions, and post-action
states to real numbers is used to model the utility function. Finally, the initial state may
be unknown, in which case an initial belief $b_0$ is given, as a "prior" distribution over $S$.

A policy in a POMDP is a mapping from belief states (probability distributions over $S$)
to the set of actions, fully specifying what actions are to be done given each possible
belief state. An initial belief state and a policy (together with the transition and observation distributions) define a Markov process, which can
be used to stochastically generate sequences of 
(belief, action, observation) tuples, (and likewise a sequence of
(state, action, reward) tuples). One is typically interested in finding
a policy such that the expected sum of rewards over such sequences is
maximized. There are various formulations of POMDP, the one that fits the selection
problem is called an indefinite horizon POMDP \cite{HansenPOMDP}, or a terminal-state POMDP: actions are
executed until some action moves the system into a terminal state.

Although there are several different variants of the selection problem
(more precisely defined in Section \ref{sec:problem}), they can all be stated as a
special case of POMDP as follows. 
The state space $S$ consists of all possible values of the items, the
identity of the last item measured, and a special additional terminal state $\bot $.
The action space $A$ consists of all possible measurement
actions and all possible selection actions. The transition distribution is actually a
rather degenerate deterministic function, as the values of the items do not change, and the
identity of the last measured item is defined by the last action. The initial belief
is defined by the prior distribution over the item values. The observation distribution
depends only on the (measurement) action and the value of the measured item. Finally,
the reward function is defined as minus the cost of the measurement (for a measurement
action), or the utility of the selected item (for a selection action). A selection action
moves the system into the terminal state, $\bot $.

Defining selection problems as POMDPs as done above sheds light on the type of optimization
problem we are trying to solve --- the fact that we are (ideally) trying to find an optimal
policy. Unfortunately, despite the fact that POMDP is a well-studied model
with numerous (usually approximate) solution algorithms available \cite{Cassandra.POMDP}\cite{Shani.POMDP},
POMDP is a highly intractable problem \cite{Cassandra.POMDP}, with complexity typically
exponential in the number of states. Since in this case the number of states is itself 
exponential in the number of items, traditional POMDP solution methods are highly impractical
(especially as in this paper the focus is on items with utility values taken from the reals).
On the other hand, POMDPs defined by selection problems are a special case, due to
the degenerate transition and observation distributions, so it may be possible to apply
specialized techniques to such problems. Indeed, approximate optimization
techniques developed for meta-reasoning are applicable to the selection problem.

