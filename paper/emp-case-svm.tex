% $Id: emp-case-svm.tex,v 1.26 2011/09/07 12:15:45 dvd Exp $

\subsection{Case Study: Optimization of SVM Parameters}
\label{sec:emp-case-svm}

Support Vector Machine (SVM) is a popular family of supervised
learning methods for classification and regression
\cite{Bishop.learning}. SVM-based classifiers are efficient and
robust. However, most kernel functions are parametrized, and the
parameters must be properly chosen for each particular kind of data in
order to achieve good classification accuracy.

An SVM classifier based on the {\em radial basis function} has two
parameters: $\zeta$ and $\gamma$. While there are heuristics for selecting
an initial approximation for a good parameter combination, an
efficient algorithm for determining their values is not known and
several different combinations must be tried. Dependency of the classification
accuracy on the parameters has a shape similar to shown in Figure~\ref{fig:emp-svm-accuracy}. 
\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.4]{svm.pdf}
\end{center}
\caption{SVM classification accuracy as function of $\log \zeta, \log \gamma$.}
\label{fig:emp-svm-accuracy}
\end{figure}
A point in the upper flat region, preferably away from the edges,
is a good parameter combination. Points lying in the
lower flat region yield the {\bf baseline} accuracy,
no better than a random class label.

A trial for a combination of parameters estimates accuracy
of the classifier through cross-validation. One possibility is the
{\em complete data} approach---the complete training data set is used
for each trial, and the accuracy is determined with high confidence, that
is, a more almost exact measurement is performed. However, the
time required to estimate the accuracy is roughly quadratic in the
size of the data set, and with larger sets the computation time can
become prohibitive.

An alternative, {\em incremental data} approach is to perform the
trials on smaller subsets of the data set.
Estimating the accuracy on each subset is imprecise but
fast, and the trials are repeated multiple times in some of the
locations to increase the confidence, each time with a different
subset. The latter approach is more time-efficient, and is based on
recurring imprecise measurements.  This case study compares the myopic
scheme for the complete data approach with both the myopic and the blinkered
scheme for the incremental data approach. We anticipated the best
results to be achieved by combining the incremental data approach with
the blinkered scheme.

\subsubsection{Data Sets}

Two data sets are used for the case study: {\sc usps}
\cite{Hull.dataset} and {\sc svmguide2} \cite{Hsu.dataset}. While the 
data sets have a different number of features and specimens, the
most essential difference is in the number of classes: 10 and 3
respectively. Due to the difference in the number of classes, the
accuracy for a poor combination of parameters, which can be
estimated as $\frac 1 {number\textit{-}of\textit{-}classes}$, is different, as well as
the steepness of the slope between the good and the bad regions.

\begin{description}[\IEEEsetlabelwidth{\sc svmguide2}]
\item[{\sc usps}] --- 10 classes, 2007 specimens, 256 features;
\item[{\sc svmguide2}] --- 3 classes, 391 specimens, 20 features.
\end{description}

\subsubsection{Optimization Problems}

Two optimization problems are defined, one for each of the approaches. The
problem definitions are the same for both data sets, and the
parameters are estimated from the data. For the incremental data approach,
the data sets are divided into 8 equal parts. It is essential that the
same problem definitions used for both data sets; the same algorithm
should be applicable to different problem instances without much
tuning. The utility function in
both problems is $\tanh(4(x-0.5))$, the $\zeta$ and $\gamma$ axes are
scaled for uniformity to ranges $[1..21]$ and there are uniform
dependencies along both axes with $\sigma_w^2=0.4$. The difference is
in the accuracy ($0.01$ for complete, $0.25$ for incremental trials) and
the measurement cost ($0.025$ for complete, $0.01$ for incremental
trials). The movement cost is zero in both problems.

\subsubsection{Experiment and Representation of Results}

The experiment design is similar to that of the function optimization
in Section~\ref{sec:emp-functions-experiment}. The myopic scheme is run for
the complete approach, and both the myopic and the blinkered schemes
are run on each data set for the incremental data approach, 64 times in
each case.  Results for the {\sc svmguide2} data set, turned out to be
more difficult for optimization, are presented in Figure~\ref{fig:emp-svm-svmguide2}.
In the figure, the first row corresponds to the complete
trials, the second and the third row---to the myopic and the
blinkered scheme for the incremental trials. The plots in the left
column show the combinations of parameters chosen by the algorithm on
each run (again, with a slight random dither in the display,
to show multiply selected combinations), and the plots in the right column
depict distributions of the net utility of the result.



\subsubsection{Results and Conclusions}

\begin{figure}[h]
\centering
\includegraphics[scale=0.48,trim=225pt 0pt 0pt 0pt,clip]{svm-svmguide2.pdf}
\caption{The {\sc svmguide2} data set.}
\label{fig:emp-svm-svmguide2}
\end{figure}

The complete data approach exhausted the budget in most runs, and in
several cases the selected parameter combinations yielded the {\bf
  baseline} accuracy. The myopic scheme for the incremental data
approach selected an optimal combination in most runs, but there still
were quite a few outliers. The blinkered scheme, however, succeeded in
always selecting an optimal combination. The mean reward is $0.88$ for
the complete data approach, $1.10$ for the myopic, $1.12$ for the
blinkered scheme.

Note that in the SVM parameter optimization
selection problem, ``measurements'' are performed
internally, and cost only runtime.
A runtimes comparison appears in
Table~\ref{tbl:emp-case-svm-runtime}; the runtime includes
training and cross-validation of the SVM classifier for the evaluated
combination of parameters (measurement ``cost''), 
while the measurement selection time is the time
spent by the algorithm to choose the combinations to evaluate
(``meta-reasoning'' time). As
one can see from the table, the runtime of the blinkered
scheme is 40\% longer than that of the myopic scheme. However, this is due
to the blinkered scheme making more ``measurements'' and eventually
selecting a better parameter combination. 
The measurement selection time for the blinkered
scheme is still about 6\% of the runtime and contributes only 18\% to
the difference in the runtimes; therefore, the blinkered scheme
can be considered consistent with the rational metareasoning approach
(Section \ref{sec:bg-limited}).

\begin{table}[h] 
\caption{Optimization of SVM parameters: runtimes} 
\label{tbl:emp-case-svm-runtime}
\centering
\small
\begin{tabular}{l|c c c}
                           & {\it complete} & \multicolumn{2}{c}{\it incremental}  \\
                           & myopic  &  myopic & blinkered \\ \hline
total runtime, seconds& 573 & 107 & 142 \\
\parbox{1in}{measurement selection time, seconds}& 2.9 & 1.7 & 8.0 \\
VOI evaluations            & 4700 & 4436 & 20663 \\

\end{tabular} 
\end{table} 


