#!/bin/sh

./runex.sh svm-i svm-ft-21-21-1 myopic &
./runex.sh svm-i svm-featab-21-21-1 myopic &
./runex.sh svm-i svm-psi-21-21-1 myopic &
./runex.sh svm-i svm-ft-21-21-1 blinkered &
./runex.sh svm-i svm-featab-21-21-1 blinkered &
./runex.sh svm-i svm-psi-21-21-1 blinkered &
wait
