### translate libsvm format to space-separated table

## libsvm line: class (index:value)+
## table line: number value+ class

import sys

def l2t(fin, fout):
    instances = []
    nfeatures = 0
    for line in fin.readlines():
        words = line.split()
        klass = int(words.pop(0))
        instance = {}
        for word in words:
            sw = word.split(':')
            i = int(sw[0])
            v = float(sw[1])
            if i>nfeatures:
                nfeatures = i
            instance[i-1] = v
        instances.append((instance,klass))
    i = 1
    for (instance,klass) in instances:
        print >>fout,i,
        for j in range(nfeatures):
            try:
                print >>fout,instance[j],
            except KeyError:
                print >>fout,0,
        print >>fout,klass
        i+=1

if __name__=='__main__':
    l2t(sys.stdin, sys.stdout)
        
        
        
            
        



