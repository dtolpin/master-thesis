#!/usr/bin/env python

# $Id: epilog.py,v 1.2 2009/08/16 06:33:50 dvd Exp $

# postprocess result logs


import sys, re

def usage():
    print >>sys.stderr, "usage: %s <log stem>" % sys.argv[0]


LINE_PATTERN = re.compile(
    "^solution:\s*(\w+)=([\d.+-Ee]+)\s+(\w+)=([\d.+-Ee]+)\s*=>"+
    "\s*(\w+)=([\d.+-Ee]+)\s*/\s*(\w+)=([\d.+-Ee]+)$")

def parseline(line):
    groups = LINE_PATTERN.match(line).groups()
    entry = []
    i = 0
    while i!=len(groups):
        name = groups[i]; i+= 1
        value = float(groups[i]); i+= 1
        entry.append((name,value))
    return entry

def main():
    if len(sys.argv)!=2:
        usage()
        sys.exit(1)

    stem=sys.argv[1]
    
    try:
        log = file(stem+".log", "r")
    except IOError, x:
        print >>sys.stderr, "error: file %s.log does not exist" % stem
        sys.exit(2)
    alp = file(stem+".alp", "w")
    utl = file(stem+".utl", "w")
    entries = []
    for line in log.readlines():
        entries.append(parseline(line))
    if len(entries):
        head = entries[0]
        print >>alp, head[0][0], head[1][0]
        print >>utl, head[2][0], head[3][0]
    for entry in entries:
        print >>alp, entry[0][1], entry[1][1]
        print >>utl, entry[2][1], entry[3][1]
    log.close()
    alp.close()
    utl.close()

if __name__=='__main__':
    main()
