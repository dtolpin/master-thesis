#!/bin/sh

solve=../../uncertima/src/algorithm/solvex
n=3
i=0
prb=$1
obj=$2
alg=$3

log=${prb}.${obj}.${alg}.log
mv $log ${log}.bak
while [ $i -ne $n ]
do 
  $solve -stat -a $alg -i 16 -j 16 -p 0.01 -q 0.01 -s -d -B 0.5 ${prb}.p ${obj}.s \
  | grep '^solution:' >> $log
  i=$((i+1))
done
