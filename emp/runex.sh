#!/bin/sh

solve=../../uncertima/src/algorithm/solvex
n=32
B=1.0
prb=$1
obj=$2
alg=$3

log=${prb}.${obj}.B${B}.${alg}.log
mv $log ${log}.bak
i=0
while [ $i -ne $n ]
do 
  $solve -c 0.03 -a $alg -s -d -B $B ${prb}.p ${obj}.s \
  | grep '^solution:' >> $log
  i=$((i+1))
done
