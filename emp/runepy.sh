#!/bin/sh

for a in results*/*.log; do
  stem=`echo $a | sed 's/\.log$//'`
  echo $stem
  ./epilog.py $stem
done
